function save_analysis_results(directory, campaign_tag1, campaign_tag2, jitter_sets1, jitter_sets2, jitter_min_max, latency_sets1, latency_sets2, latency_min_max, packet_loss_bins, packet_loss_bins_max, packet_loss_timings, packet_loss_timings_max)

    save_prefix = strcat(directory, campaign_tag1);



    for i=1:(length(jitter_sets1))
        figure_handle = figure(i);
        clf();

%        width = 4;
%        height = 3;
%        set(figure_handle, 'PaperUnits', 'inches');
%        set(figure_handle, 'PaperOrientation', 'portrait');
%        set(figure_handle, 'PaperSize', [width, height]);
%        set(figure_handle, 'PaperPosition', [0, 0, width, height]);
%        set(figure_handle, 'defaultaxesposition', [0.15, 0.15, 0.75, 0.75]);
%        set(0, 'defaultaxesfontsize', 14);

        set_temp = jitter_sets1(i).bins;
        set_temp_sum = sum(set_temp);
        for j = 1:length(set_temp)
            set_temp(j) = set_temp(j) / set_temp_sum;
        endfor

        stairs_handle = stairs(linspace(jitter_min_max(1), jitter_min_max(2), length(set_temp)), set_temp, 'color', 'blue');
        set(stairs_handle(1), 'linewidth', 3);
        set(gca(), 'linewidth', 3);
        set(gca(), 'fontsize', 22);

%        set(bar_handle(1), 'facecolor', 'none');

        hold on;

        set_temp = jitter_sets2(i).bins;
        set_temp_sum = sum(set_temp);
        for j = 1:length(set_temp)
            set_temp(j) = set_temp(j) / set_temp_sum;
        endfor

        stairs_handle = stairs(linspace(jitter_min_max(1), jitter_min_max(2), length(set_temp)), set_temp, 'color', 'red');
        set(stairs_handle(1), 'linewidth', 3);

        set(gca(), 'yscale', 'log');

%        pos = find_pos_after(jitter_sets(i).bins, 0.01);
%        line([pos, pos], [0, 10000000], 'color', 'green');
%        pos = find_pos_after(jitter_sets(i).bins, 0.05);
%        line([pos, pos], [0, 10000000], 'color', 'yellow');
%        pos = find_pos_before(jitter_sets(i).bins, 0.05);
%        line([pos, pos], [0, 10000000], 'color', 'yellow');
%        pos = find_pos_before(jitter_sets(i).bins, 0.01);
%        line([pos, pos], [0, 10000000], 'color', 'green');

        axis([jitter_min_max, 10^-5, 1]);

        title_str = strcat(strcat(jitter_sets1(i).name, ' jitter histogram for '), campaign_tag1);
        title(title_str);

        xlabel('ms');

        filename = strcat(save_prefix, '_jitter_', jitter_sets1(i).name, '.png');
        print(filename, '-dpng');
	%figure(figure_handle,'Visible','Off');
    endfor






   %FIXME FIXME FIXME

    for i=1:(length(latency_sets1))
        figure_handle = figure(i);
        clf();




%        width = 4;
%        height = 3;
%        set(figure_handle, 'PaperUnits', 'inches');
%        set(figure_handle, 'PaperOrientation', 'portrait');
%        set(figure_handle, 'PaperSize', [width, height]);
%        set(figure_handle, 'PaperPosition', [0, 0, width, height]);
%        set(figure_handle, 'defaultaxesposition', [0.15, 0.15, 0.75, 0.75]);
%        set(0, 'defaultaxesfontsize', 14);

        set_temp = latency_sets1(i).bins;
        set_temp_sum = sum(set_temp);
        for j = 1:length(set_temp)
            set_temp(j) = set_temp(j) / set_temp_sum;
        endfor

        stairs_handle = stairs(linspace(latency_min_max(1), latency_min_max(2), length(set_temp)), set_temp, 'color', 'blue');
        set(stairs_handle(1), 'linewidth', 3);
        set(gca(), 'linewidth', 3);
        set(gca(), 'fontsize', 22);

%        set(bar_handle(1), 'facecolor', 'none');

        hold on;

        set_temp = latency_sets2(i).bins;
        set_temp_sum = sum(set_temp);
        for j = 1:length(set_temp)
            set_temp(j) = set_temp(j) / set_temp_sum;
        endfor

        stairs_handle = stairs(linspace(latency_min_max(1), latency_min_max(2), length(set_temp)), set_temp, 'color', 'red');
        set(stairs_handle(1), 'linewidth', 3);

        set(gca(), 'yscale', 'log');

%        pos = find_pos_after(latency_sets(i).bins, 0.01);
%        line([pos, pos], [0, 10000000], 'color', 'green');
%        pos = find_pos_after(latency_sets(i).bins, 0.05);
%        line([pos, pos], [0, 10000000], 'color', 'yellow');
%        pos = find_pos_before(latency_sets(i).bins, 0.05);
%        line([pos, pos], [0, 10000000], 'color', 'yellow');
%        pos = find_pos_before(latency_sets(i).bins, 0.01);
%        line([pos, pos], [0, 10000000], 'color', 'green');

        axis([latency_min_max, 10^-5, 1]);

        title_str = strcat(strcat(latency_sets1(i).name, ' latency histogram for '), campaign_tag1);
        title(title_str);

        xlabel('ms');

        filename = strcat(save_prefix, '_latency_', latency_sets1(i).name, '.png');

        print(filename, '-dpng');
	%figure(figure_handle,'Visible','Off');
    endfor


   %FIXME FIXME FIXME 

    % packet loss bins
    figure_handle = figure(length(jitter_sets1) + 1);
    clf;

    bar(1:14, packet_loss_bins(2:end));

%    set(gca(), 'xtick', [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 7.5, 9.5, 11.5, 13.5]);
    set(gca(), 'xtick', 0.5:13.5);
    set(gca(), 'xticklabel', '1|2|4|8|16|32||128||512||2048||8192');
    set(gca(), 'linewidth', 3);
    set(gca(), 'fontsize', 22);

    axis([0, 14, 0, packet_loss_bins_max]);

    title_str = strcat('Packet loss histogram for ', campaign_tag1);
    title(title_str);

    filename = strcat(save_prefix, '_packet_loss_bins.png');
    print(filename, '-dpng');
    %figure(figure_handle,'Visible','Off');

    % packet loss timings
    figure_handle = figure(length(jitter_sets1) + 2);

    clf;

    bar(packet_loss_timings(:,2));

    axis([0, size(packet_loss_timings,1), 0, packet_loss_timings_max]);

    set(gca(), 'linewidth', 3);
    set(gca(), 'fontsize', 22);

    title_str = strcat('Packet loss timings for ', campaign_tag1);
    title(title_str);

    xlabel('seconds');

    filename = strcat(save_prefix, '_packet_loss_timings.png');
    print(filename, '-dpng');
    %figure(figure_handle,'Visible','Off');
endfunction

function pos = find_pos_after(bins, before)
    total = sum(bins);
    thusfar = 0;

    for i = 1:length(bins)
        thusfar += bins(i);
        if ((thusfar / total) > before)
            pos = i * 100 / length(bins);
            return ;
        endif
    endfor
endfunction

function pos = find_pos_before(bins, after)
    total = sum(bins);
    thusfar = total;

    for i = 1:length(bins)
        thusfar -= bins(i);
        if ((thusfar / total) < after)
            pos = i * 100 / length(bins);
            return ;
        endif
    endfor
endfunction
