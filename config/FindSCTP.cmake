find_path(
    SCTP_INCLUDE_DIR
    NAMES
    sctp.h
    PATHS
    /usr/include/ /usr/include/sctp/ /usr/include/netinet
    /usr/local/include/ /usr/local/include/sctp/ /usr/local/include/netinet/
    )

find_library(SCTP_LIBRARY NAMES sctp)

if(SCTP_INCLUDE_DIR AND SCTP_LIBRARY)
    set(SCTP_FOUND TRUE)
endif(SCTP_INCLUDE_DIR AND SCTP_LIBRARY)

if(SCTP_FOUND)
    if(NOT SCTP_FIND_QUIETLY)
        message(STATUS "Found SCTP: ${SCTP_LIBRARY}")
    endif(NOT SCTP_FIND_QUIETLY)
else(SCTP_FOUND)
    if(SCTP_FIND_REQUIRED)
        message(FATAL_ERROR "Could not find SCTP")
    endif(SCTP_FIND_REQUIRED)
endif(SCTP_FOUND)
