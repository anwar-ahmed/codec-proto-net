
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <time.h>

#include "codec.h"
#include "feeder_config.h"
#include "timing.h"

static void print_usage(char *binary_name)
{
    printf("Usage: %s [-c <num>] [-i <iterations>] [-t <threads>] template_file\n", binary_name);
}

static int parse_arguments(int argc, char **argv, size_t *codec_idx,
        char **template_file, size_t *iterations, size_t *threads)
{
    int c;

    while (1)
    {
        static struct option long_options[] =
        {
            {"help", no_argument, NULL, 'h'},
            {"codec", required_argument, NULL, 'c'},
            {"iterations", required_argument, NULL, 'i'},
            {"threads", required_argument, NULL, 't'},
            {0, 0, 0, 0}
        };

        int option_index = 0;
        c = getopt_long(argc, argv, "hc:i:t:", long_options, &option_index);

        if (c == -1)
        {
            // it was the end of options
            break ;
        }

        switch (c)
        {
            case 'h':
                print_usage(argv[0]);
                return -1;
            case 'c':
                *codec_idx = atoi(optarg);
                break ;
            case 'i':
                *iterations = atoi(optarg);
                if (*iterations < 1)
                {
                    printf("Error: iterations value has to be at least 1.\n");
                    return -1;
                }
                break ;
            case 't':
                *threads = atoi(optarg);
                if (*threads < 1)
                {
                    printf("Error: threads value has to be at least 1.\n");
                    return -1;
                }
                break ;
            default:
                print_usage(argv[0]);
                return -1;
        }
    }

    int _curr_arg = optind;
    int _arg_i = 1;
    while (_arg_i < 2)
    {
        if (argc <= _curr_arg)
        {
            printf("ERROR: Too few arguments!\n");
            printf("\n");
            print_usage(argv[0]);
            return -1;
        }

        switch (_arg_i)
        {
            case 1:
                *template_file = argv[_curr_arg];
                break ;
            default:
                break;
        }

        ++_arg_i;
        ++_curr_arg;
    }

    return 0;
}

size_t filesize_get(char *filename)
{
    int fd = open(filename, O_RDONLY);
    if (fd == -1)
    {
        printf("open() failed.\n");
        return 0;
    }

    struct stat fd_stat;
    if (fstat(fd, &fd_stat))
    {
        printf("fstat() failed.\n");
        return 0;
    }
    return fd_stat.st_size;
}

static struct timespec *time_earlier(struct timespec *time_a,
        struct timespec *time_b)
{
    if (time_a->tv_sec < time_b->tv_sec)
    {
        return time_a;
    }
    if (time_a->tv_sec > time_b->tv_sec)
    {
        return time_b;
    }
    return time_a->tv_nsec <= time_b->tv_nsec ? time_a : time_b;
}
static struct timespec *time_later(struct timespec *time_a,
        struct timespec *time_b)
{
    return time_a == time_earlier(time_a, time_b) ? time_b : time_a;
}

static void time_accuracy(struct codec_thread *codec_threads, size_t threads,
        struct timespec *time_accuracy_start,
        struct timespec *time_accuracy_end)
{
    struct timespec *time_start_earliest =
            &codec_threads[0].time_benchmark_start;
    struct timespec *time_start_latest =
            &codec_threads[0].time_benchmark_start;
    struct timespec *time_end_earliest =
            &codec_threads[0].time_benchmark_end;
    struct timespec *time_end_latest = 
            &codec_threads[0].time_benchmark_end;

    for (size_t i = 1; i < threads; ++i)
    {
        time_start_earliest = time_earlier(time_start_earliest,
                &codec_threads[i].time_benchmark_start);
        time_start_latest = time_later(time_start_latest,
                &codec_threads[i].time_benchmark_start);
        time_end_earliest = time_earlier(time_end_earliest,
                &codec_threads[i].time_benchmark_end);
        time_end_latest = time_later(time_start_latest,
                &codec_threads[i].time_benchmark_end);
    }

    time_difference(time_start_earliest, time_start_latest,
            time_accuracy_start);
    time_difference(time_end_earliest, time_end_latest, time_accuracy_end);
}

double time_divide(uint64_t total_ms, struct timespec *divisor)
{
    uint64_t total_ns = total_ms * 1000 * 1000;
    uint64_t divisor_ns = divisor->tv_nsec;
    divisor_ns = divisor_ns + divisor->tv_sec * 1000 * 1000 * 1000;

    return (double)total_ns / (double)divisor_ns;
}

int main(int argc, char **argv)
{
    size_t codec_idx = 0;
    char *template_file;
    size_t iterations = 1;
    size_t threads = 1;

    if (parse_arguments(argc, argv, &codec_idx, &template_file, &iterations,
            &threads))
    {
        return -1;
    }

    struct codec_config **codec_config_list;

    if (load_config_file(template_file, &codec_config_list))
    {
        printf("Error in load_config_file()\n");
        return -1;
    }

    size_t codec_config_list_length = 0;
    struct codec_config *codec_config;
    while ((codec_config = codec_config_list[codec_config_list_length]) &&
            (codec_config_list_length < codec_idx))
    {
        codec_config_list_length++;
    }
    if (codec_config == NULL)
    {
        printf("Template file contains only %zu templates, but requested to "
                "benchmark number %zu.\n",
                codec_config_list_length,
                codec_idx);
        free(codec_config_list);
        return -1;
    }

    struct codec_thread *codec_threads =
            malloc(sizeof(struct codec_thread) * threads);

    for (size_t i = 0; i < threads; ++i)
    {
        codec_threads[i].state = CODEC_STATE_NOT_STARTED;
        codec_threads[i].input_file = codec_config->in_filename;
        codec_threads[i].iterations = iterations;
        codec_threads[i].timing = codec_config->timing;
        codec_threads[i].codec_name = codec_config->codec_name;
        codec_threads[i].framesize = codec_config->framesize;
        codec_threads[i].parameters = codec_config->parameters;
    }

    printf("\n");

    printf("Starting benchmark\n");
    printf(" - codec: %s\n", codec_config->codec_name);
    printf(" - iterations: %zu\n", iterations);
    printf(" - threads: %zu\n", threads);

    for (size_t i = 0; i < threads; ++i)
    {
        if (codec_thread_create(&codec_threads[i]))
        {
            printf("Thread creation failed\n");
            return -1;
        }
    }

    for (size_t i = 0; i < threads; ++i)
    {
        int *thread_result;
        pthread_join(codec_threads[i].thread, (void **) &thread_result);
        if (thread_result)
        {
            printf("thread %zu failed (error: 0x%.08x)\n", i, *thread_result);
        }
    }

    struct timespec time_accuracy_start;
    struct timespec time_accuracy_end;
    time_accuracy(codec_threads, threads, &time_accuracy_start,
            &time_accuracy_end);

    size_t input_file_size = filesize_get(codec_config->in_filename);
    uint64_t frames_per_call = input_file_size / codec_config->framesize;
    uint64_t call_time_ms = codec_config->timing * frames_per_call;
    uint64_t call_time_total_ms = call_time_ms * iterations;

    struct timespec time_diff;
    time_difference(&codec_threads[0].time_benchmark_start,
            &codec_threads[0].time_benchmark_end, &time_diff);

    double capacity_per_thread = time_divide(call_time_total_ms, &time_diff);
    double capacity_total = capacity_per_thread * threads;

    printf("\n");

    printf("Benchmarking completed!\n");
    printf(" - total benchmark time: %lld.%06ld seconds\n",
            (long long) time_diff.tv_sec, time_diff.tv_nsec / 1000);
    if (threads > 1)
    {
        printf(" - thread start accuracy: %lld.%06ld seconds\n",
                (long long) time_accuracy_start.tv_sec,
                time_accuracy_start.tv_nsec / 1000);
        printf(" - thread end accuracy: %lld.%06ld seconds\n",
                (long long) time_accuracy_end.tv_sec,
                time_accuracy_end.tv_nsec / 1000);
    }

    printf("\n");

    printf("Results:\n");
    printf(" - per thread capacity: %f simultaneous calls\n",
            capacity_per_thread);
    printf(" - total capacity: %f simultaneous calls\n",
            capacity_total);

    printf("\n");

    free(codec_threads);
    free(codec_config_list);

    return 0;
}

