
#include "linked_list.h"

#include <stdlib.h>

linked_list_t linked_list_create(void)
{
    linked_list_t result = (linked_list_t)
            malloc(sizeof(struct linked_list));

    result->first = NULL;

    return result;
}

int linked_list_destroy(linked_list_t list)
{
    if (list == NULL)
    {
        return -1;
    }

    struct linked_list_node *curr = list->first;

    while (curr)
    {
        struct linked_list_node *freeable = curr;
        curr = curr->next;
        free(freeable);
    }

    free(list);

    return 0;
}

int linked_list_insert(linked_list_t list, uint16_t key, void *value)
{
    if (list == NULL)
    {
        return -1;
    }

    struct linked_list_node *curr = list->first;
    struct linked_list_node **insertable;

    if (!curr)
    {
        // special case: list is empty
        insertable = &list->first;
        goto _insert;
    }

    while (curr->next)
    {
        curr = curr->next;
    }

    insertable = &curr->next;

_insert:
    (*insertable) = (struct linked_list_node *)
            malloc(sizeof(struct linked_list_node));

    (*insertable)->next = NULL;
    (*insertable)->key = key;
    (*insertable)->value = value;

    return 0;
}

int linked_list_remove(linked_list_t list, uint16_t key, void **value)
{
    if (list == NULL)
    {
        return -1;
    }

    struct linked_list_node *curr = list->first;
    struct linked_list_node *prev = NULL;

    if (!curr)
    {
        return -1;
    }

    while (curr->key != key)
    {
        if (!curr->next)
        {
            return -1;
        }

        prev = curr;
        curr = curr->next;
    }

    if (prev == NULL)
    {
        // special case: we're removing the first one
        list->first = curr->next;
    }
    else
    {
        prev->next = curr->next;
    }

    if (value)
    {
        *value = curr->value;
    }

    free(curr);
    return 0;
}

int linked_list_length(linked_list_t list)
{
    if (!list)
    {
        return -1;
    }

    if (!list->first)
    {
        return -1;
    }

    int _amount = 0;
    struct linked_list_node *_curr = list->first;
    while (_curr)
    {
        ++_amount;
        _curr = _curr->next;
    }

    return _amount;
}

int linked_list_search(linked_list_t list, uint16_t key, void **value)
{
    if (list == NULL)
    {
        return -1;
    }

    struct linked_list_node *curr = list->first;

    if (!curr)
    {
        return -1;
    }

    while (curr->key != key)
    {
        if (!curr->next)
        {
            return -1;
        }

        curr = curr->next;
    }

    *value = curr->value;

    return 0;
}

int linked_list_get(linked_list_t list, int num, uint16_t *key, void **value)
{
    if (list == NULL)
    {
        return -1;
    }

    struct linked_list_node *curr = list->first;

    if (!curr)
    {
        return -1;
    }

    int count = 0;

    while (count < num)
    {
        if (!curr)
        {
            return -1;
        }

        curr = curr->next;
        ++count;
    }

    *key = curr->key;
    *value = curr->value;

    return 0;
}

int linked_list_get_first(
        linked_list_t list, linked_list_node_t **first)
{
    if (list == NULL)
    {
        return -1;
    }

    if (first == NULL)
    {
        return -1;
    }

    if (!list->first)
    {
        return -1;
    }

    *first = list->first;

    return 0;
}

int linked_list_get_next(
        linked_list_node_t *node, linked_list_node_t **next, uint16_t *key, void **value)
{
    if (node == NULL)
    {
        return -1;
    }

    if (!node->next)
    {
        return -1;
    }

    *next = node->next;
    *key = (*next)->key;
    *value = (*next)->value;

    return 0;
}

int linked_list_get_node(
        linked_list_t list, uint16_t key, linked_list_node_t **node, void **value)
{
    if (list == NULL)
    {
        return -1;
    }

    struct linked_list_node *curr = list->first;

    if (!curr)
    {
        return -1;
    }

    while (curr->key != key)
    {
        if (!curr->next)
        {
            return -1;
        }

        curr = curr->next;
    }

    *node = curr;
    *value = curr->value;

    return 0;
}

int linked_list_get_and_move_next(
        linked_list_node_t **node, uint16_t *key, void **value)
{
    if ((!node) || (!(*node)))
    {
        return -1;
    }

    if (key)
    {
        *key = (*node)->key;
    }
    *value = (*node)->value;
    *node = (*node)->next;

    return 0;
}

int linked_list_move_next(linked_list_node_t **node)
{
    if ((!node) || (!(*node)) || (!((*node)->next)))
    {
        return -1;
    }

    (*node) = (*node)->next;

    return 0;
}

int linked_list_pop(linked_list_t list, uint16_t *key, void **value)
{
    if (list == NULL)
    {
        return -1;
    }

    if (!list->first)
    {
        return -1;
    }

    struct linked_list_node *poppable = list->first;

    *key = poppable->key;
    *value = poppable->value;

    list->first = poppable->next;

    free(poppable);

    return 0;
}
