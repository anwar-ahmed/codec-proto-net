#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source $DIR/setbuild.sh
source $DIR/ulimits.sh


#pgrep -lf ksoftirqd | awk '{print $1}' | xargs -n1 renice -n 10 -p 
# set real-time scheduling for ksoftirqd with priority 10
pgrep -lf ksoftirqd | awk '{print $1}' | xargs -n1 chrt -f -p 90

# Please note that, you can't set socket receive buffer to maximum value defined in kernel which you can see on /proc/sys/net/core/rmem_max. You have to change this value to use big socket receive buffer size in application:
sudo sysctl -w net.core.rmem_max=33554432

#netdev_max_backlog controls the number of packets allowed to queue for network cards in kernel side.
sudo sysctl -w net.core.netdev_max_backlog=2000

