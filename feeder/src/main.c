
#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


#include "ctrl_msg.h"
#include "feeder_config.h"

#include "control.h"
#include "ui.h"

#define PROGRAM_MAJOR_VERSION 0
#define PROGRAM_MINOR_VERSION 1

#ifndef SCTP
#define UDP_BUFFER_SIZE 1024
#endif

char *interface_to_bind;


// Printable info:
// Verbose 0:
//      Program start
//      Campaign start/end
//      Connection to server
//      Fatal errors
// Verbose 1:
//      Call start/end
//      Errors
// Verbose 2:
//      Thread start/end info
//      Warnings
// Verbose 3:
//      Extended thread info
// Verbose 4:
//      Control request debug info
//      Some SCTP debug info
// Verbose 5:
//      All SCTP debug info

static void print_usage(char *argv0)
{
    printf("Feeder %d.%d\n", PROGRAM_MAJOR_VERSION, PROGRAM_MINOR_VERSION);
    printf("Usage: %s [OPTIONS] <server address> <campaign file>\n", argv0);
    printf("\n");
    printf("Feeder program for Codec Runner.\n");
    printf("\n");
    printf("Options:\n");
    printf("  -h  Print this help and exit.\n");
    printf("  -v  Raise verbosity level\n");
    printf("  -d  Print some debug information.\n");
    printf("  -i  interface to use for outgoing connections");
    printf("  --silent\n");
    printf("      Print only minimal information.\n");
    printf("  --info\n");
    printf("      Print all available info.\n");
    printf("  --control\n");
    printf("      Print all control message infos.\n");
    printf("  --sctp\n");
    printf("      Print all SCTP infos.\n");
    printf("  --thread\n");
    printf("      Print all thread infos.\n");
}

static void raise_verbosity(int *target, int verbosity)
{
    *target = *target <= verbosity ? verbosity : *target;
}

static void lower_verbosity(int *target, int verbosity)
{
    *target = *target >= verbosity ? verbosity : *target;
}

static int parse_command_line(
        int argc,
        char **argv,
        int *display_thread_id,
        struct verbosity_levels *verbosity_levels,
        char **server_address,
        char **campaign_file)
{
    while (1)
    {
        static struct option long_options[] =
        {
            {"help", no_argument, NULL, 'h'},
            {"verbose", no_argument, NULL, 'v'},
            {"debug", no_argument, NULL, 'd'},
            {"thread", optional_argument, NULL, 't'},
            {"interface", optional_argument, NULL, 'i'},
            {"silent", no_argument, NULL, 0x0BEEF000},
            {"info", no_argument, NULL, 0x0DEAD000},
            {"control", no_argument, NULL, 0x0DEAD001},
            {"sctp", no_argument, NULL, 0x0DEAD003},
            {0, 0, 0, 0}
        };

        int option_index = 0;
        int c = getopt_long(argc, argv, "hvdt:i:", long_options, &option_index);

        if (c == -1)
        {
            // it was the end of options
            break ;
        }

        switch (c)
        {
            case 'h':
                print_usage(argv[0]);
                return -1;

            case 'v':
                raise_verbosity((int *) &verbosity_levels->report, 3);
                raise_verbosity((int *) &verbosity_levels->info, 2);
                raise_verbosity((int *) &verbosity_levels->thread, 1);
                raise_verbosity((int *) &verbosity_levels->sctp, 0);
                break ;

            case 'd':
                raise_verbosity((int *) &verbosity_levels->control, 1);
                raise_verbosity((int *) &verbosity_levels->thread, 2);
                break ;

            case 't':
                if (optarg)
                {
                    if ((strcmp(optarg, "yes") == 0) ||
                            (strcmp(optarg, "y") == 0))
                    {
                        *display_thread_id = 1;
                    }
                    else if ((strcmp(optarg, "no") == 0) ||
                            (strcmp(optarg, "n") == 0))
                    {
                        *display_thread_id = 0;
                    }
                    else
                    {
                        printf("ERROR: Unknown value for thread: '%s'\n", optarg);
                        printf("\n");
                        print_usage(argv[0]);
                        return -1;
                    }
                }
                else
                {
                    *display_thread_id = 1;
                }
                break ;


            case 'i':
			   if (optarg == NULL ){
				  fprintf(stderr, "missing arg\n");
				  exit(EXIT_FAILURE);
			   }
            	interface_to_bind = (char *)strdup(optarg);
            	break;

            case 0x0BEEF000:
                lower_verbosity((int *) &verbosity_levels->report, 1);
                lower_verbosity((int *) &verbosity_levels->info, 1);
                lower_verbosity((int *) &verbosity_levels->control, 0);
                lower_verbosity((int *) &verbosity_levels->thread, 0);
                lower_verbosity((int *) &verbosity_levels->sctp, 0);
                break ;

            case 0x0DEAD000:
                raise_verbosity((int *) &verbosity_levels->info, 3);
                break ;

            case 0x0DEAD001:
                raise_verbosity((int *) &verbosity_levels->control, 1);
                break ;

            case 0x0DEAD002:
                // TODO: No way to get here
                raise_verbosity((int *) &verbosity_levels->thread, 2);
                break ;

            case 0x0DEAD003:
                raise_verbosity((int *) &verbosity_levels->sctp, 2);
                break ;

            default:
                printf("\n");
                print_usage(argv[0]);
                return -1;
        }
    }

    int _curr_arg = optind;
    int _arg_i = 1;
    while (_arg_i < 3)
    {
        if (argc <= _curr_arg)
        {
            printf("ERROR: Too few arguments!\n");
            printf("\n");
            print_usage(argv[0]);
            return -1;
        }

        switch (_arg_i)
        {
            case 1:
                *server_address = argv[_curr_arg];
                break ;

            case 2:
                *campaign_file = argv[_curr_arg];
                break ;
            default:
                break;
        }

        ++_arg_i;
        ++_curr_arg;
    }

    return 0;
}

static int open_connection_to_server(socket_handle_t *socket_handle,
        char *address)
{
    int fd_sctp = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);

    if (fd_sctp == -1)
    {
        printf("SCTP socket() error\n");
        return -1;
    }

    // Initialize the SCTP socket (number of streams etc)
    struct sctp_initmsg initmsg;
    memset(&initmsg, 0, sizeof(initmsg));
    initmsg.sinit_num_ostreams = 65535;
    initmsg.sinit_max_instreams = 65535;
    initmsg.sinit_max_attempts = 1;
    if (setsockopt(fd_sctp, IPPROTO_SCTP, SCTP_INITMSG, &initmsg,
            sizeof(struct sctp_initmsg)) == -1)
    {
        int the_error = errno;
        printf("SCTP setsockopt() initmsg error (errno: %d, %s)\n", the_error,
                strerror(the_error));
        return -1;
    }


    struct sctp_event_subscribe events;
    memset((void *) &events, 0, sizeof(events));
    events.sctp_data_io_event = 1;
    if (setsockopt(fd_sctp, SOL_SCTP, SCTP_EVENTS, (const void *) &events,
            sizeof(events)) == -1)
    {
        int the_error = errno;
        printf("SCTP setsockopt() events error (errno: %d, %s)\n", the_error,
                strerror(the_error));
        return -1;
    }

    // Set interface for outgoing connections
    if (interface_to_bind){
    	//printf("Interface: %s\n", interface_to_bind);
    	if(setsockopt(fd_sctp, SOL_SOCKET, SO_BINDTODEVICE, interface_to_bind, strlen(interface_to_bind)))
		{
			int the_error = errno;
			printf("SCTP setsockopt() set interface error (errno: %d, %s)\n", the_error,
					strerror(the_error));
			return -1;
		}
    }


    // Set the size of the sending buffer to 1 (send immediately, no buffering)
    int nodelay = 1;
    if (setsockopt(fd_sctp, SOL_SCTP, SCTP_NODELAY, &nodelay, sizeof(int)))
    {
        int the_error = errno;
        printf("SCTP setsockopt() nodelay error (errno: %d, %s)\n", the_error,
                strerror(the_error));
        return -1;
    }

    // Set the timeout of a call to receive packets
    struct timeval timeout = {.tv_sec = 0, .tv_usec = 500000};
    if (setsockopt(fd_sctp, SOL_SOCKET, SO_RCVTIMEO, &timeout,
            sizeof(timeout)))
    {
        int the_error = errno;
        printf("SCTP setsockopt() timeout error (errno: %d, %s)\n", the_error,
                strerror(the_error));
        return -1;
    }

    struct sockaddr_in addr;

    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SERVER_LISTEN_PORT);
    inet_pton(AF_INET, address, &addr.sin_addr);
    socklen_t addr_len = sizeof(struct sockaddr_in);

    feeder_ui_log(1, 2, "Connecting to the server %s...\n", address);

    if (connect(fd_sctp, (struct sockaddr *) &addr, addr_len) == -1)
    {
        printf("SCTP connect() error\n");
        return -1;
    }

    *socket_handle = malloc(sizeof(struct socket_handle_struct));
    (*socket_handle)->fd_sctp_listen = -1;
    (*socket_handle)->fd_sctp_connection = fd_sctp;
    (*socket_handle)->fd_udp = -2;
    (*socket_handle)->addr = NULL;
    (*socket_handle)->addr_len = 0;

    enum CTRL_MSG_TYPE ctrl_msg_type;
    while (1)
    {
        int the_error;
        if ((the_error =
                ctrl_msg_recv(*socket_handle, &ctrl_msg_type, NULL, NULL,
                NULL)))
        {
            if (the_error == -1)
            {
                if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
                    continue ;

                printf("ctrl_msg_recv() error: %d (%s).\n", errno,
                        strerror(errno));
            }
            else
            {
                printf("ctrl_msg_recv() error: %d.\n", the_error);
            }

            return -1;
        }

        if (ctrl_msg_type == CTRL_MSG_TYPE_CONNECTION_UDP_OPENED_INF)
            break ;

        printf("ERROR: Received unexpected ctrl_msg: %d\n", ctrl_msg_type);
    }

    int fd_udp = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd_udp == -1)
    {
        printf("UDP socket() error: %d (%s).\n", errno, strerror(errno));
        return -1;
    }

    struct sockaddr_in addr_udp;
    memset(&addr_udp, 0, sizeof(struct sockaddr_in));
    addr_udp.sin_family = AF_INET;
    addr_udp.sin_port = htons(SERVER_LISTEN_PORT);
    if (inet_pton(AF_INET, address, &addr_udp.sin_addr) <= 0)
    {
        printf("UDP inet_pton() error: %d (%s).\n", errno, strerror(errno));
        return -1;
    }
    if (connect(fd_udp, (struct sockaddr *) &addr_udp,
            sizeof(struct sockaddr_in)))
    {
        printf("UDP connect() error: %d (%s).\n", errno, strerror(errno));
        return -1;
    }

    if (ctrl_msg_send_connection_udp_opened_ack(*socket_handle))
    {
        printf("connect_udp_opened_ack() failed.\n");
        return -1;
    }

    (*socket_handle)->fd_udp = fd_udp;
    (*socket_handle)->addr = malloc(addr_len);
    memcpy((*socket_handle)->addr, &addr_udp, addr_len);
    (*socket_handle)->addr_len = addr_len;

    feeder_ui_log(1, 2, "Connected!\n");


    return 0;
}

static void close_connection_to_server(socket_handle_t socket_handle)
{
    if (socket_handle->fd_sctp_connection > 0)
        close(socket_handle->fd_sctp_connection);
    if (socket_handle->fd_udp > 0)
        close(socket_handle->fd_udp);
}

static int read_campaign_file(struct codec_config ***config_list, char *filename)
{
    feeder_ui_log(1, 2, "Reading the campaign file...\n");

    int result;
    if ((result = load_config_file(filename, config_list)))
    {
        printf("Reading of the campaing file failed with the error: %d\n", result);
        return -1;
    }

    feeder_ui_log(1, 2, "Campaign file reading completed!\n");

    return 0;
}

int main(int argc, char **argv)
{
    int display_thread_id = 0;
    struct verbosity_levels verbosity_levels = VERBOSITY_LEVELS_DEFAULT_VALUES;
    char *server_address;
    char *campaign_file;
    if (parse_command_line(
            argc,
            argv,
            &display_thread_id,
            &verbosity_levels,
            &server_address,
            &campaign_file))
    {
        return -1;
    }

    int _end_ui_painter = 0;
    struct feeder_ui_thread_start thread_ui_start_struct =
            {
                    .end = &_end_ui_painter,
                    .display_thread_id = display_thread_id,
                    .verbosity_levels = verbosity_levels
            };
    pthread_t thread_ui;
    if (pthread_create(
            &thread_ui,
            NULL,
            &feeder_ui_thread,
            &thread_ui_start_struct))
    {
        printf("could not create UI thread\n");
        return -1;
    }

    feeder_ui_wait_init_completion();

    feeder_ui_log(1, 2, "Running Feeder %d.%d\n",
            PROGRAM_MAJOR_VERSION,
            PROGRAM_MINOR_VERSION);

    feeder_ui_log(1, 2, "Connecting to server %s...\n", server_address);

    socket_handle_t socket_handle;
    if (open_connection_to_server(&socket_handle, server_address))
    {
        sleep(10);
        _end_ui_painter = 1;
        pthread_join(thread_ui, NULL);
        printf("could not connect to server\n");
        return -1;
    }

    feeder_ui_log(1, 2, "Connected to server!\n");

    feeder_ui_log(1, 2, "Loading campaign file %s...\n", campaign_file);

    struct codec_config **config_list;
    int error_code;
    if ((error_code = read_campaign_file(&config_list, campaign_file)))
    {
        close_connection_to_server(socket_handle);
        _end_ui_painter = 1;
        pthread_join(thread_ui, NULL);
        printf("could not load the campaign file (err: %d)\n", error_code);
        return -1;
    }

    feeder_ui_log(1, 1, "Starting campaign...\n");

    feeder_control(socket_handle, config_list);

    feeder_ui_log(1, 1, "Campaign completed!\n");

    feeder_ui_log(1, 2, "Will now disconnect...\n");

    close_connection_to_server(socket_handle);

    free(config_list);

    _end_ui_painter = 1;
    pthread_join(thread_ui, NULL);

    return 0;
}

