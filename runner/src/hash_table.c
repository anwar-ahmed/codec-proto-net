
#include <stdlib.h>
#include <string.h>

#include "hash_table.h"

#define HASHFUNC(x) ((x) % HASH_TABLE_SIZE)

int hash_function(int key)
{
    return key % HASH_TABLE_SIZE;
};

hash_table_t hash_table_create(void)
{
    hash_table_t result = (hash_table_t) malloc(sizeof(struct hash_table));

    hash_table_init(result);

    return result;
}

int hash_table_init(hash_table_t table)
{
    if (table == NULL)
    {
        return -1;
    }

    size_t table_size = sizeof(struct hash_table_entry *) * HASH_TABLE_SIZE;

    table->table = (struct hash_table_entry **) malloc(table_size);

    memset(table->table, 0, table_size);

    return 0;
}

int hash_table_destroy(hash_table_t table)
{
    if (table == NULL)
    {
        return -1;
    }

    for (size_t i = 0; i < HASH_TABLE_SIZE; ++i)
    {
        struct hash_table_entry *entry = table->table[i];

        if (entry)
        {
            while (entry->next)
            {
                entry = entry->next;
                free(entry->prev);
            }

            free(entry);
        }
    }

    free(table->table);

    free(table);

    return 0;
}

int hash_table_search(const hash_table_t table, int key, void **value)
{
    if (table == NULL)
    {
        return -1;
    }

    struct hash_table_entry *entry = table->table[HASHFUNC(key)];

    if (entry == NULL)
    {
        return -1;
    }

    while (entry->key != key)
    {
        if (entry->next == NULL)
        {
            break ;
        }

        entry = entry->next;
    }

    if (entry->key == key)
    {
        *value = entry->value;
        return 0;
    }

    return -1;
}

int hash_table_insert(hash_table_t table, int key, void *value)
{
    if (table == NULL)
    {
        return -1;
    }

    struct hash_table_entry *entry = table->table[HASHFUNC(key)];

    if (entry == NULL)
    {
        entry = (struct hash_table_entry *) malloc(sizeof(struct hash_table_entry));

        entry->key = key;
        entry->value = value;
        entry->next = NULL;
        entry->prev = NULL;

        table->table[HASHFUNC(key)] = entry;

        return 0;
    }

    while (entry->key != key)
    {
        if (entry->next == NULL)
        {
            entry->next = (struct hash_table_entry *) malloc(sizeof(struct hash_table_entry));

            entry->next->key = key;
            entry->next->value = value;
            entry->next->next = NULL;
            entry->next->prev = entry;

            return 0;
        }

        entry = entry->next;
    }

    // entry already exists, won't overwrite

    return -1;
}

int hash_table_remove(hash_table_t table, int key)
{
    if (table == NULL)
    {
        return -1;
    }

    struct hash_table_entry *entry = table->table[HASHFUNC(key)];

    if (entry == NULL)
    {
        return -1;
    }

    while (entry->key != key)
    {
        if (entry->next == NULL)
        {
            return -1;
        }

        entry = entry->next;
    }

    if (entry->prev)
    {
        entry->prev->next = entry->next;
    }
    else
    {
        // it was the first
        table->table[HASHFUNC(key)] = entry->next;
    }

    if (entry->next)
    {
        entry->next->prev = entry->prev;
    }

    free(entry);

    return 0;
}
