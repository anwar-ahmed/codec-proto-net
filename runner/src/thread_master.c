#define _GNU_SOURCE

#include "wool.h"
#include "codec.h"
#include "ctrl_msg.h"
#include "hash_table.h"
#include "sched_headers.h"
#include "thread_master.h"
#include "macros.h"

#include <errno.h>
#include <netinet/sctp.h>
#include <pthread.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <inttypes.h>
#include <unistd.h>
#include <netinet/ip.h>
#include <sys/socket.h>

/* second nanosecond format */
#define TIMESNS "%" PRIu64 ".%.9" PRIu64 " "

#define handle_error(en, msg) \
               do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

/*
Unlike TCP, UDP protocol does not have built-in flow-control capabilities,
so if you can't process all of the received packets fast enough, kernel will
start to drop new incoming packets because of the socket receive buffer is full.
When you don't make any tuning on udp stack, default udp receive buffer size is
between 32-128 kilobytes per socket. You can set it to much higher value with
setsockopt like below:

*/

#define UDP_BUFFER_SIZE 1024
#define VLEN 64


struct timespec campaign_start_time;

struct socket_reading_thread_arg
{
    socket_handle_t socket_handle;
    hash_table_t codec_buffers;
    int *thread_stop;
};

/* WOOL FOR loop Body definition */
LOOP_BODY_1(processFrames,
        LARGE_BODY,
        int, j,
        struct codec_struct **,
        codecPtrArray)
{
    clock_gettime(CLOCK_REALTIME,
                    &codecPtrArray[j]->record.enc_alogrithm_timestamp);
    if (codecPtrArray[j]->ippFuncsPtr->Encode(codecPtrArray[j]->encoder,
			&codecPtrArray[j]->inStream,
			&codecPtrArray[j]->outStream) == USC_NoError)
	{
	    clock_gettime(CLOCK_REALTIME,
	            &codecPtrArray[j]->record.task_done_timestamp);
	    write(codecPtrArray[j]->timeStamps_fd,
	            &codecPtrArray[j]->record, sizeof(codecPtrArray[j]->record));
	}
}

static pthread_cond_t thread_exit_cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t thread_exit_mutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_mutex_t codec_state_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t codec_state_cond = PTHREAD_COND_INITIALIZER;

static int sched_setattr(pid_t pid, const struct sched_attr *attr,
        unsigned int flags) {
    return syscall(SYSCALL_SETATTR, pid, attr, flags);
}

static void set_thread_stop(int *thread_stop)
{
    pthread_mutex_lock(&thread_exit_mutex);
    *thread_stop = 1;
    pthread_cond_signal(&thread_exit_cond);
    pthread_mutex_unlock(&thread_exit_mutex);
}



/* The timestamp control message is sent with level
 SOL_SOCKET and the cmsg_data field is a struct timeval/timespec
 indicating the reception time of the last packet. Further info:
 http://man7.org/linux/man-pages/man3/cmsg.3.html
 */


void save_timestamp_from_datagram(struct msghdr *msgPtr,
        struct codec_struct *codecPtr)
{
    struct cmsghdr *cmsgPtr;

    for (cmsgPtr = CMSG_FIRSTHDR(msgPtr); cmsgPtr != NULL;
            cmsgPtr = CMSG_NXTHDR(msgPtr, cmsgPtr))
    {
        if (cmsgPtr->cmsg_level == SOL_SOCKET)
        {
            memcpy(&codecPtr->record.kernel_timestamp, CMSG_DATA(cmsgPtr),
                    sizeof(codecPtr->record.kernel_timestamp));
        }
        break;
    }
}

static void *udp_socket_reading_thread(void *arg)
{
	struct socket_reading_thread_arg *_arg =
			(struct socket_reading_thread_arg *) arg;
	socket_handle_t socket_handle = _arg->socket_handle;
	hash_table_t codec_buffers = _arg->codec_buffers;
	int *thread_stop = _arg->thread_stop;

	/* initializing wool run-time environment */
	wool_init(0, NULL);
	#if 0

	struct sched_attr attr;
	memset(&attr, 0, sizeof(struct sched_attr));
	attr.size = sizeof(struct sched_attr);
	unsigned int flags = 0;

	// Here we need to set the FIFO sched_priority right in order not to starve
	// network I/O process

	attr.sched_policy = SCHED_FIFO;
	attr.sched_priority = 11;

	int ret = sched_setattr(0, &attr, flags);

	if (ret < 0)
	{
		printf("ERROR: Failed to set scheduling priority for "
				"the UDP socket reading thread (%d - %s)\n",
		errno, strerror(errno));
		perror("sched_setattr");
		exit(-1);
	}
	#endif

	/* error handling in case of Receive failure */
	int the_error = 0xDEADBEEF;
	char *error_str = NULL;

	/* common array for currently running streams to be processed in parallel */
	struct codec_struct **codecPtrArray = malloc(VLEN * sizeof(uintptr_t));

	/* structures for Sockets and received Datagrams */
	struct sockaddr_in host_address;
	struct mmsghdr datagramS[VLEN];
	struct iovec iovecS[VLEN];
	char datagramBuff[VLEN][UDP_BUFFER_SIZE];
	char control[1024];

	host_address.sin_family = AF_INET;
	host_address.sin_port = htons(43762);
	host_address.sin_addr.s_addr = INADDR_ANY;

	/* initialization of Socket structures */
	memset(datagramS, 0, sizeof(datagramS));
	for (int i = 0; i < VLEN; ++i)
	{
		iovecS[i].iov_base = datagramBuff[i];
		iovecS[i].iov_len = UDP_BUFFER_SIZE;
		datagramS[i].msg_hdr.msg_iov = &iovecS[i];
		datagramS[i].msg_hdr.msg_iovlen = 1;
		datagramS[i].msg_hdr.msg_control = control;
		datagramS[i].msg_hdr.msg_controllen = 1024;
		datagramS[i].msg_hdr.msg_namelen = socket_handle->addr_len;
		datagramS[i].msg_hdr.msg_name = &host_address;
	}

	/* keep running until the thread is signaled to stop */
	while (!(*thread_stop))
	{
		int nr_datagrams = recvmmsg(socket_handle->fd_udp, datagramS, VLEN, MSG_WAITFORONE,
				NULL);
		if (nr_datagrams == -1)
		{
			the_error = errno;
			error_str = "recvmmsg() error";
			goto error_location;
		}

		struct codec_struct *codecPtr;

		for (int i = 0; i < nr_datagrams; ++i)
		{
			uint16_t streamId = *((uint16_t *) datagramBuff[i]);
			uint16_t dgramSeqNum = *((uint16_t *) (datagramBuff[i]
					+ sizeof(uint16_t)));

			if (hash_table_search(codec_buffers, streamId, (void **) &codecPtr))
			{
                // The stream might have been terminated and scratched by
			    // SCTP thread and we still have packets in kernel buffers
			    // The packets should not be processed further
			    printf("Dropping the packet no:%d from streamId:%d\n",
                        dgramSeqNum, streamId);
			    nr_datagrams = nr_datagrams - 1;
                continue;
			}
			/* store current sequence number in stream structure */
			codecPtr->record.seq_num = dgramSeqNum;

			/* save kernel's timestamp in stream structure */
            struct msghdr *msgPtr = &datagramS[i].msg_hdr;
			save_timestamp_from_datagram(msgPtr, codecPtr);

			/* store the incoming frame in Frame Buffer without Sequence No.
			and Stream ID */
			memcpy(codecPtr->inStream.pBuffer,
					2 * sizeof(uint16_t) + datagramBuff[i],
					codecPtr->frameLength);
			codecPtrArray[i] = codecPtr;
		}

		/* Parallel FOR loop to spawn N tasks where N is # of Datagrams */
		FOR(processFrames, 0, nr_datagrams, codecPtrArray);

		/* Now signal SCTP thread if a stream has to be terminated. */
        pthread_mutex_lock(&codec_state_mutex);
        /* mutex is now locked - wait on the condition variable.  */
        /* During the execution of pthread_cond_wait, the mutex is unlocked. */
        pthread_cond_signal(&codec_state_cond);
        pthread_mutex_unlock(&codec_state_mutex);
	}

	wool_fini( );
	return NULL;

	error_location: printf("UDP reading thread: ERROR %d (%s)\n", the_error,
			error_str);
	printf("UDP reading thread: Thread will end\n");
	set_thread_stop(thread_stop);
	int *result = malloc(sizeof(int));
	*result = the_error;
	wool_fini( );
	return result;
}

static int handle_ctrl_msg(socket_handle_t handle,
        enum CTRL_MSG_TYPE ctrl_msg_type,
        uint16_t stream_id,
        char *ctrl_msg_data,
        hash_table_t codec_buffers,
        int *thread_stop)
{
  switch (ctrl_msg_type)
	{
	case CTRL_MSG_TYPE_CODEC_START_REQ:
	{
		struct ctrl_msg_codec_start_req *start_req =
				(struct ctrl_msg_codec_start_req *) ctrl_msg_data;

		struct codec_struct *codecPtr = (struct codec_struct *) malloc(
				sizeof(struct codec_struct));

		if (hash_table_insert(codec_buffers, stream_id, codecPtr))
		{
			// id already exists
			ctrl_msg_send_codec_start_dec(handle, stream_id);
			return -1;
		}

		size_t name_len = strlen(start_req->codec_name);
		codecPtr->codec_name = malloc(name_len + 1);
		strcpy(codecPtr->codec_name, start_req->codec_name);

		codecPtr->socket_handle = handle;
		codecPtr->state = CODEC_STATE_NOT_STARTED;
		codecPtr->timing = start_req->timing;
		codecPtr->weight = start_req->weight;
		codecPtr->call_id = stream_id;
		codecPtr->frameLength = start_req->framesize;
		codecPtr->max_delay = start_req->max_delay;
		codecPtr->parameters = start_req->parameters;

		int *returnErrCode = initStreamStruct(codecPtr);
	    if (returnErrCode != NULL)
	    {
	        ctrl_msg_send_codec_start_dec(handle, stream_id);
	        hash_table_remove(codec_buffers, stream_id);
	        free(codecPtr->codec_name);
	        free(codecPtr);
	        return -1;
	    }
	    else
	    {
	    	ctrl_msg_send_codec_start_acc(handle, stream_id);
	    }
		break;
	}

	case CTRL_MSG_TYPE_CODEC_END:
	{
		struct codec_struct *codecPtr;

		if (hash_table_search(codec_buffers, stream_id, (void **) &codecPtr) == 0)
            {
                /* lock the mutex to change the codec state to END */
                pthread_mutex_lock(&codec_state_mutex);
                /* mutex is now locked - wait on the condition variable.  */
                /* During the execution of pthread_cond_wait, the mutex is unlocked. */
                codecPtr->state = CODEC_STATE_ENDED;
                hash_table_remove(codec_buffers, stream_id);
                close(codecPtr->timeStamps_fd);
                for (int i = 0; i < codecPtr->numMemBanks; ++i)
                {
                    free(codecPtr->codecMemBanksPtr[i].pMem);
                }
                free(codecPtr->codecMemBanksPtr);
                free(codecPtr->ippCodecinfoPtr);
                free_all(codecPtr->inStream.pBuffer,
                        codecPtr->outStream.pBuffer, codecPtr->codec_name,
                        codecPtr);
                /* finally, unlock the mutex */
                pthread_mutex_unlock(&codec_state_mutex);
            }
		break;
	}

	case CTRL_MSG_TYPE_CAMPAIGN_END:
		set_thread_stop(thread_stop);
		break;

	default:
		printf("ERROR: Unknown control message received ");
		printf("(type: 0x%.4x)\n", ctrl_msg_type);
		break;
	}

  return 0;
}

static void *sctp_socket_reading_thread(void *arg)
{
    struct socket_reading_thread_arg *_arg =
            (struct socket_reading_thread_arg *)arg;
    socket_handle_t socket_handle = _arg->socket_handle;
    hash_table_t codec_buffers = _arg->codec_buffers;
    int *thread_stop = _arg->thread_stop;
	
    #if 0
    struct sched_attr attr;
    memset(&attr, 0, sizeof(struct sched_attr));
    attr.size = sizeof(struct sched_attr);
    unsigned int flags = 0;

    // This should be higher than UDP reading thread
    attr.sched_policy = __SCHED_FIFO;
    attr.sched_priority = 12;


    int ret;

    ret = sched_setattr(0, &attr, flags);
    if (ret < 0) {
        printf("ERROR: Failed to set scheduling priority for "
                "the SCTP socket reading thread (%d - %s)\n",
        errno, strerror(errno));

        perror("sched_setattr");
        exit(-1);
    }
    #endif

    int _thread_result = 0;
    while (!(*thread_stop))
    {
        enum CTRL_MSG_TYPE _ctrl_msg_type;
        uint16_t _stream_id;
        char *_data_ptr;
        uint16_t _data_len;
        int _ctrl_msg_recv_result;
        if ((_ctrl_msg_recv_result = ctrl_msg_recv(socket_handle,
                &_ctrl_msg_type, &_stream_id, &_data_ptr, &_data_len)))
        {
            printf("SCTP reading thread: ");
            if (_ctrl_msg_recv_result ==
                    -CTRL_MSG_RECV_ERROR_CLIENT_DISCONNECTED)
            {
                printf("Client disconnected.\n");
            }
            else if (_ctrl_msg_recv_result == -1)
            {
                printf("ERROR %d (%s).\n", errno, strerror(errno));
                _thread_result = -1;
            }
            else
            {
                printf("ERROR %d.\n", _ctrl_msg_recv_result);
                _thread_result = -2;
            }

            set_thread_stop(thread_stop);
            break ;
        }

        // control message
        handle_ctrl_msg(socket_handle, _ctrl_msg_type, _stream_id, _data_ptr,
                codec_buffers, thread_stop);

        if (_data_len)
            free(_data_ptr);
    }

    printf("SCTP reading thread: Thread ended.\n");

    if (_thread_result)
    {
        int *result_ptr = malloc(sizeof(int));
        *result_ptr = _thread_result;
        return result_ptr;
    }
    return NULL;
}

//static void join_thread(pthread_t thread, char *thread_str)
//{
//    int *join_retval;
//    pthread_join(thread, (void **) &join_retval);
//    if (join_retval)
//    {
//        printf("%s exited with code %d.\n", thread_str, *join_retval);
//        free(join_retval);
//    }
//}

int thread_master(socket_handle_t handle)
{
    clock_gettime(CLOCK_MONOTONIC, &campaign_start_time);

    hash_table_t codec_buffers = hash_table_create();

    int thread_stop = 0;
    int status = 0;

    struct socket_reading_thread_arg _socket_reading_thread_arg =
        {
            .socket_handle = handle,
            .codec_buffers = codec_buffers,
            .thread_stop = &thread_stop,
        };

    pthread_t udp_thread;
    status = pthread_create(&udp_thread,
            NULL,
            udp_socket_reading_thread,
            &_socket_reading_thread_arg);
    if (status != 0)
        handle_error(status, "pthread_create_udp");

    pthread_t sctp_thread;
    status = pthread_create(&sctp_thread,
            NULL,
            sctp_socket_reading_thread,
            &_socket_reading_thread_arg);
    if (status != 0)
        handle_error(status, "pthread_create_sctp");


    pthread_mutex_lock(&thread_exit_mutex);
    while (!thread_stop)
    {
        pthread_cond_wait(&thread_exit_cond, &thread_exit_mutex);
    }
    pthread_mutex_unlock(&thread_exit_mutex);


    // FIXME
    //join_thread(udp_thread, "UDP reading thread");
    //join_thread(udp_thread, "SCTP reading thread");

    hash_table_destroy(codec_buffers);

    return 0;
}
