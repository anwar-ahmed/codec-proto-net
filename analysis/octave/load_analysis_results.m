function [jitter_sets, latency_sets, packet_loss_bins, packet_loss_timings] = load_analysis_results(directory, scheduler)
    jitter_sets = load_timing_histograms(directory, scheduler);
    latency_sets = load_latency_histograms(directory, scheduler);
    packet_loss_bins = load_packet_loss_bins(directory, scheduler);
    packet_loss_timings = load_packet_loss_timings(directory, scheduler);
endfunction
