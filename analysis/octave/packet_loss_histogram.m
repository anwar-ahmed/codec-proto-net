function [bins, biggest] = packet_loss_histogram(measurements, bin_amount)
% packet_loss_histogram(measurements, bin_amount)
%
% Calculates maximum amount of consecutive packets lost per call and creates
% the histogram bins out of it
%
% In:   measuremets     the measurements of the campaign
%       bin_amount      amount of bins to use when creating the histogram
% Out:  bins            the bins of the histogram
%       biggest         if there are more consecutive packets lost in a call
%                       than bin_amount, this value will be set
%
% The function executes in O(n*m), where n is the amount of calls and m
% the amount of measurements per call.
    bins = zeros(bin_amount, 1);
    biggest = -1;

    for i = 1:length(measurements)
        max_cons_packet_loss = 0;

        for j = 2:length(measurements(i).seq_nums)
            if (measurements(i).seq_nums(j) < measurements(i).seq_nums(j-1))
                error("Seq numbers in wrong order, re-run the campaign");
            endif

            cons_packet_loss = measurements(i).seq_nums(j) - measurements(i).seq_nums(j-1) - 1;

            if (cons_packet_loss > max_cons_packet_loss)
                max_cons_packet_loss = cons_packet_loss;
            endif
        endfor

        if (max_cons_packet_loss < bin_amount)
            bins(max_cons_packet_loss + 1) += 1;
        else
            if (biggest < max_cons_packet_loss)
                biggest = max_cons_packet_loss;
            endif
        endif
    endfor
endfunction
