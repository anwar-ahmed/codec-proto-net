
#pragma once

#include "ctrl_msg.h"


enum FEEDER_CONTROL_PROGRAM_NOTIFICATION
{
    FEEDER_CONTROL_PROGRAM_NOTIFICATION_SCTP_ERROR_DISCONNECT,
    FEEDER_CONTROL_PROGRAM_NOTIFICATION_SCTP_ERROR_CONNECTION_RESET,
    FEEDER_CONTROL_PROGRAM_NOTIFICATION_CAMPAIGN_ENDED
};

void feeder_control(socket_handle_t socket_handle,
        struct codec_config **codec_configs);

void feeder_control_request_thread_start(struct codec_config *codec_config);
void feeder_control_request_thread_end(int id);
void feeder_control_request_ctrl_msg(enum CTRL_MSG_TYPE ctrl_msg_type,
        uint16_t stream_id, char *data);
void feeder_control_request_key_press(int ch);
void feeder_control_request_program_notification(
        enum FEEDER_CONTROL_PROGRAM_NOTIFICATION notification);
