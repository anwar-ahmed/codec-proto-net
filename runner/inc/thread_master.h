
#pragma once

#include <pthread.h>

#include "codec.h"
#include "ctrl_msg.h"

int thread_master(socket_handle_t handle);

int *initStreamStruct(struct codec_struct *codec_struct_ptr);

