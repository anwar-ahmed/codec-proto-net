#define _GNU_SOURCE
#include "threads.h"
#include "macros.h"

#include <fcntl.h>
#include <netinet/sctp.h>
#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>

#include "ctrl_msg.h"
#include "timing.h"

#include "control.h"
#include "ui.h"

#include <errno.h>

#define LOG_SHORT_BUFFER_SIZE 128
#define LOG_VERBOSE_BUFFER_SIZE 256

extern size_t feeder_threads_stop;

struct feeder_codec
{
    uint16_t id;
    pthread_t thread;
    struct codec_config *codec_config;
};

struct feeder_codec **terminated_threads = NULL;
size_t terminated_threads_count;
size_t reaped_threads_count;

static pthread_mutex_t thread_ended_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t thread_ended_condition = PTHREAD_COND_INITIALIZER;

struct feeder_thread_start
{
    socket_handle_t socket_handle;
    uint16_t id;
    struct codec_config *codec_config;
};

/**
 *
 *  This function is used to create timings output folder from current time
 *
 *  @param function does not take parameters
 */

static char * create_timing_output_folder(void)
{
    // Starting time stamp for the results folder
    time_t orig_format;
    time(&orig_format);
    struct tm *timeinfo;
    timeinfo = gmtime(&orig_format);
    char *start_time;

    //asprintf(&start_time,"%d_%02d_%02d_%02d_%02d", timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday, (timeinfo->tm_hour+3), timeinfo->tm_min);
    asprintf(&start_time,"%d_%02d_%02d", timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday);

    return start_time;
}


/**
 *  End thread
 *
 *  @param id
 *  @param thread
 *  @param codec_config
  *
 */

static void thread_ended(uint16_t id, pthread_t thread, struct codec_config *codec_config)
{
    pthread_mutex_lock(&thread_ended_mutex);

    terminated_threads[terminated_threads_count]->id = id;
    terminated_threads[terminated_threads_count]->thread = thread;
    terminated_threads[terminated_threads_count]->codec_config = codec_config;

    ++terminated_threads_count;

    pthread_cond_signal(&thread_ended_condition);

    pthread_mutex_unlock(&thread_ended_mutex);
}

static int describe_thread(struct codec_config *codec_config, char **dest)
{
    int strlen = asprintf(dest, "%s (%s - %s)",
            codec_config->id,
            codec_config->codec_name,
            codec_config->parameters.direction == 0 ? "ENCODE" : "DECODE");

    if (strlen < 0)
    {
        return -1;
    }

    return 0;
}

static void print_start_of_thread_msg(struct codec_config *codec_config)
{
    char *thread_description;
    if (describe_thread(codec_config, &thread_description))
    {
        feeder_ui_log(1, 3, "A new codec has been started.\n");
    }
    else
    {
        feeder_ui_log(1, 3, "Start new thread: %s\n", thread_description);
    }
    free(thread_description);
}

static void print_end_of_thread_msg(struct codec_config *codec_config, int fail, int error)
{
    char *thread_description;
    if (describe_thread(codec_config, &thread_description))
    {
        if (fail)
        {
            feeder_ui_log(0, 2, "Coding failed with error code: %d.\n", error);
        }
        else
        {
            feeder_ui_log(1, 3, "Coding completed.\n");
        }
    }
    else
    {
        if (fail)
        {
            feeder_ui_log(0, 2, "Coding (%s) failed with error code: 0x%.04x.\n",
                    thread_description, error);
        }
        else
        {
            feeder_ui_log(1, 3, "Coding completed: %s.\n", thread_description);
        }
    }
    free(thread_description);
}

static void *feeder_thread(void *arg)
{
    int error_code = 0;
    struct feeder_thread_start *start_struct = (struct feeder_thread_start *) arg;
    socket_handle_t socket_handle = start_struct->socket_handle;
    int id = start_struct->id;
    struct codec_config *codec_config = start_struct->codec_config;
    free(start_struct);

    int fd = open(codec_config->in_filename, O_RDONLY);
    if (fd == -1)
    {
        error_code = FEEDER_THREAD_ERROR_OPEN_FILE;
        goto error_handler;
    }

    size_t filesize;
    struct stat fd_stat;
    if (fstat(fd, &fd_stat))
    {
        feeder_ui_log(0, 2, "ERROR: Couldn't read the file size for %s, the "
                "progress printing will be inaccurate.\n",
                codec_config->in_filename);
        filesize = 0xFFFFFFFF; // big enough...
    }
    else
    {
        filesize = fd_stat.st_size;
    }

    size_t framesize = codec_config->framesize;
    size_t timing = codec_config->timing;

    // buffer contents:
    //  uint16_t - stream ID
    //  uint16_t - packet sequence number
    //  n bytes  - speech frame
    char *in_buffer = malloc(
            2 * sizeof(uint16_t) +
            framesize
            );
    memcpy(in_buffer, (uint16_t *) &id, sizeof(uint16_t));
    uint16_t _seq_num = 0;



    struct itimerspec timer_timespec = {
            .it_value.tv_sec = 0,
            .it_value.tv_nsec = timing * 1000000,
            .it_interval.tv_sec = 0,
            .it_interval.tv_nsec = timing * 1000000
    };

    // Initialize new timer, CLOCK_MONOTONIC

    int timerfd = timerfd_create(CLOCK_MONOTONIC, 0);
    if (timerfd == -1)
    {
        error_code = FEEDER_THREAD_ERROR_TIMER_CREATE;
        goto error_handler;
    }

    if (timerfd_settime(timerfd, 0, &timer_timespec, NULL) == -1)
    {
        error_code = FEEDER_THREAD_ERROR_TIMER_ARM;
        goto error_handler;
    }

    size_t total_size = filesize - filesize % framesize;

    // Bytes_sent is part of _codec_progress structure that is being displayed in the UI to the user

    size_t *bytes_sent = feeder_ui_codec_new(id, total_size, codec_config->id);
    while ((*bytes_sent < total_size) && (!feeder_threads_stop))
    {
        ssize_t bytes_read = read(fd,
                2 * sizeof(uint16_t) +
                in_buffer,
                framesize);

        uint64_t timer_expirations; // n-1 overflows
        if (read(timerfd, &timer_expirations, sizeof(uint64_t)) != sizeof(uint64_t))
        {
            error_code = FEEDER_THREAD_ERROR_TIMER_READ;
            goto error_handler;
        }

        if (timer_expirations > 1)
        {
            error_code = FEEDER_THREAD_ERROR_TIMER_OVERFLOW;
            goto error_handler;
        }

        if (bytes_read < 0)
        {
            error_code = FEEDER_THREAD_ERROR_READ_FILE;
            goto error_handler;
        }
        else if (bytes_read == 0)
        {
            // EOF
            break ;
        }

        else if ((size_t)bytes_read < framesize)
        {
            // We read less than framesize because end of file or because a signal
            // interrupted the reading. try once to continue and fail if still doesn't
            // work since timing would already probably be so off.
            size_t extra_buffer_size = framesize - bytes_read;
            char *extra_buffer = (char *) malloc(extra_buffer_size);
            ssize_t second_read = read(fd, extra_buffer, extra_buffer_size);
            if (second_read < (ssize_t)extra_buffer_size)
            {
                free(extra_buffer);
                error_code = FEEDER_THREAD_ERROR_READ_FILE2;
                goto error_handler;
            }
            memcpy(in_buffer + 2 * sizeof(uint16_t) + bytes_read, extra_buffer,
                    extra_buffer_size);
            free(extra_buffer);
        }

        // UDP packet seq_num
        ++_seq_num;
        memcpy(in_buffer + sizeof(uint16_t), &_seq_num, sizeof(uint16_t));
//        if (sendto(socket_handle->fd_udp,
//                in_buffer,
//                bytes_read + 2 * sizeof(uint16_t),
//                0,
//                (struct sockaddr *) &(socket_handle->addr),
//                socket_handle->addr_len) == -1)
        if (send(socket_handle->fd_udp,
                in_buffer,
                bytes_read + 2 * sizeof(uint16_t),
                0)
                == -1)
        {
            feeder_ui_log(0, 2, "DEBUG: UDP error is %d - %s\n", errno, strerror(errno));
            error_code = FEEDER_THREAD_ERROR_SEND_UDP;
            goto error_handler;
        }

        *bytes_sent += framesize;
    }

    if (*bytes_sent < total_size)
    {
        error_code = FEEDER_THREAD_ERROR_PREMATURE_EXIT;
        goto error_handler;
    }

    feeder_ui_codec_remove(id);

    close(timerfd);
    close(fd);

    free(in_buffer);

    thread_ended(id, pthread_self(), codec_config);

    return NULL;

error_handler:
    {
        if (error_code & 0x8000)
        {
            feeder_ui_codec_remove(id);
        }
        int *error = (int *) malloc(sizeof(int));
        *error = error_code;
        thread_ended(id, pthread_self(), codec_config);
        free(in_buffer);
        return (void *) error;
    }
}

int feeder_thread_thread_create_request(socket_handle_t socket_handle,
        struct codec_config *codec_config, int *id)
{
    static uint16_t _id = 0;

    // ensure id belongs to [1, 65534]
    // note: 65535 not allowed since max in/out streams in linux SCTP implementation
    // is U16 value and we use 0 for control channel: We can have at most 65534
    // different codec streams.
    _id %= 65534;
    ++_id;

    char *timing_output_folder = create_timing_output_folder();

    char *ptr = strchr(codec_config->campaign_filename,'.');
    *ptr = '\0';

    //char *timing_output_filename = (char *) malloc(TIMING_OUTPUT_FILENAME_MAX_LENGTH);
    /*snprintf(timing_output_filename, TIMING_OUTPUT_FILENAME_MAX_LENGTH - 1,
            "%s_%s",strchr(codec_config->campaign_filename, '.')+1,timing_output_folder);
     */
    char *timing_output_filename = "woolTrial_2016_02_04";
    int rval = ctrl_msg_send_codec_start_req(socket_handle, _id, codec_config,
            timing_output_filename);
    /*free_all(timing_output_filename
            ,timing_output_folder);*/
    free_all(timing_output_folder);

    *id = _id;

    return rval;
}

int feeder_thread_thread_create_create(socket_handle_t socket_handle, int id,
        struct codec_config *codec_config)
{
    struct feeder_thread_start *arg = (struct feeder_thread_start *)
            malloc(sizeof(struct feeder_thread_start));
    arg->socket_handle = socket_handle;
    arg->id = id;
    arg->codec_config = codec_config;

    pthread_t thread;
    if (pthread_create(&thread, NULL, feeder_thread, (void *) arg))
    {
        return -1;
    }

    return 0;
}

static int time_has_passed(
        struct timespec *time_moment,
        struct timespec *current_time)
{
    if (current_time->tv_sec > time_moment->tv_sec)
    {
        return 1;
    }

    if (current_time->tv_sec < time_moment->tv_sec)
    {
        return 0;
    }

    if (current_time->tv_nsec > time_moment->tv_nsec)
    {
        return 1;
    }

    return 0;
}

static void time_add(
        struct timespec *time_base,
        struct timespec *time_add,
        struct timespec *destination)
{
    destination->tv_sec = time_base->tv_sec + time_add->tv_sec;
    destination->tv_nsec = time_base->tv_nsec + time_add->tv_nsec;

    if (destination->tv_nsec >= 1000000000)
    {
        destination->tv_sec++;
        destination->tv_nsec -= 1000000000;
    }
}

static void *control_threads(void *arg)
{
    struct codec_config **codec_configs = (struct codec_config **) arg;
    size_t amount_of_threads_total = 0;
    struct codec_config **curr_codec_thread = codec_configs;
    while (*curr_codec_thread)
    {
        ++amount_of_threads_total;
        ++curr_codec_thread;
    }

    terminated_threads = (struct feeder_codec **)
            malloc(sizeof(struct feeder_codec *) * (amount_of_threads_total));
    for (size_t i = 0; i < amount_of_threads_total; ++i)
    {
        terminated_threads[i] = (struct feeder_codec *)
                malloc(sizeof(struct feeder_codec));
    }

    terminated_threads_count = 0;
    reaped_threads_count = 0;

    curr_codec_thread = codec_configs;

    struct timespec start_time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);

    struct timespec start_time_epoch;
    clock_gettime(CLOCK_REALTIME, &start_time_epoch);

    feeder_ui_log(1, 3, "Starting campaign with %zu registered codecs.\n",
            amount_of_threads_total);

    while (amount_of_threads_total > reaped_threads_count)
    {
        // create new threads at appropriate times, but in-between wait
        // if previously created thread would terminate

        struct timespec current_time;
        clock_gettime(CLOCK_MONOTONIC, &current_time);

        struct timespec time_since_start;
        time_difference(&start_time, &current_time, &time_since_start);

        if (!feeder_threads_stop)
        {
            while (*curr_codec_thread)
            {
                if (!time_has_passed(&(**curr_codec_thread).start_time, &time_since_start))
                {
                    break ;
                }

                print_start_of_thread_msg(*curr_codec_thread);

                feeder_control_request_thread_start(*curr_codec_thread);

                ++curr_codec_thread;
            }
        }
        else
        {
            size_t started_threads_count = curr_codec_thread - codec_configs;
            if (reaped_threads_count == started_threads_count)
            {
                break ;
            }
        }

        pthread_mutex_lock(&thread_ended_mutex);

        if (*curr_codec_thread)
        {
            struct timespec sleep_until;
            time_add(&start_time_epoch, &(**curr_codec_thread).start_time,
                    &sleep_until);

            // note: cond_wait will unlock the mutex, but lock it again before
            // returning
            pthread_cond_timedwait(&thread_ended_condition, &thread_ended_mutex,
                    &sleep_until);
        }
        else
        {
            pthread_cond_wait(&thread_ended_condition, &thread_ended_mutex);
        }

        while (terminated_threads_count > reaped_threads_count)
        {
            struct feeder_codec *reapable = terminated_threads[reaped_threads_count++];

            int *retval;

            pthread_join(reapable->thread, (void **) &retval);

            if (retval)
            {
                print_end_of_thread_msg(reapable->codec_config, 1, *retval);

                free(retval);
            }
            else
            {
                print_end_of_thread_msg(reapable->codec_config, 0, 0);
            }

            feeder_control_request_thread_end(reapable->id);

            free_all(reapable->codec_config->id
                    ,reapable->codec_config->codec_name
                    ,reapable->codec_config->in_filename
                    ,reapable->codec_config->out_filename
                    ,reapable->codec_config)
        }

        pthread_mutex_unlock(&thread_ended_mutex);
    }

    feeder_control_request_program_notification(
            FEEDER_CONTROL_PROGRAM_NOTIFICATION_CAMPAIGN_ENDED);

    for (size_t i = 0; i < amount_of_threads_total; ++i)
    {
        free(terminated_threads[i]);
    }
    free(terminated_threads);

    return NULL;
}

int feeder_thread_control_thread_create(pthread_t *thread, struct codec_config **codec_configs)
{
    if (pthread_create(thread, NULL, control_threads, codec_configs))
    {
        return -1;
    }

    return 0;
}

