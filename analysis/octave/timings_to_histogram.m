function [bins, outliers] = timings_to_histogram(measurements, bin_amount, min_value, max_value)
% timings_to_histogram(measurements, bin_amount, min_value, max_value)
%
% Creates the histogram bins for the jitter deviation of the measurements
%
% In:   measurements    the measurements of the campaign
%       bin_amount      amount of bins to plot the jitter with
%       min_value       minimum timing value to show
%       max_value       maximum timing value to show
% Out:  bins            the timing bins
%       outliers        amount of measurements outside of the range
%                               [min_value, max_value]
%
% The function executes in O(n*m), where n is the amount of calls and m
% the amount of measurements per call.
    bins = zeros(bin_amount,1);
    outliers = 0;

    for i=1:length(measurements)
        for j=2:length(measurements(i).timings)
            delay = measurements(i).timings(j) - measurements(i).timings(j-1);

            bin_number = get_bin(delay, bin_amount, min_value, max_value);
            if (bin_number > 0)
                bins(bin_number) += 1;
            else
                outliers += 1;
            endif
        endfor
    endfor
endfunction

function bin_number = get_bin(delay, bin_amount, min_value, max_value)
    if ((delay < min_value) || (delay > max_value))
        bin_number = -1;
        return ;
    endif

    % note! this function executes in linear time

    delta = (max_value - min_value) / bin_amount;

    bin_number = floor((delay - min_value) / delta) + 1;

    if (bin_number > bin_amount)
        % this happens if delay == max_value
        bin_number = bin_amount
    endif
endfunction
