/*
 * processing_scheduling_latency_histogram.c
 *
 *  Created on: Aug 26, 2015
 *      Author: eheihol
 */


/*
 * scheduling_latency_histogram.c
 *
 *  Created on: Aug 24, 2015
 *      Author: eheihol
 */

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <inttypes.h>

#include "scheduling_latency_histogram.h"

#define FILENAME_MAX_LEN 256

struct bins_and_outliers
{
    size_t *bins;
    size_t outliers;
};

struct scheduling_latency_histogram
{
    char *codec_name;
    size_t timing;
    struct bins_and_outliers bao_untouched;
//    struct bins_and_outliers bao_compensate_loss;
//    struct bins_and_outliers bao_remove_loss;
    struct scheduling_latency_histogram *next;
};


static int write_file(char *directory, char *scheduler, char *codec_name,
        char *strategy, size_t *bins, size_t bin_amount)
{
    char _filename[FILENAME_MAX_LEN];
    snprintf(_filename, FILENAME_MAX_LEN, "%s%s_scheduling_latency_%s_%s.txt", directory,
            scheduler, codec_name, strategy);

    FILE *_file = fopen(_filename, "w");
    if (!_file)
    {
        printf("ERROR: Could not open %s for writing of scheduling_latency "
                "histogram (%d - %s)\n", _filename, errno,
                strerror(errno));
        return -1;
    }

    for (size_t i = 0; i < bin_amount; ++i)
        fprintf(_file, "%zu\n", bins[i]);

    fclose(_file);
    return 0;
}

static int get_bin(size_t scheduling_latency_ns, size_t bin_amount, size_t scheduling_latency_min_ns,
        size_t scheduling_latency_max_ns, size_t *bin)
{
    if (!bin || (scheduling_latency_ns < scheduling_latency_min_ns) || (scheduling_latency_ns > scheduling_latency_max_ns)){
        return -1;
    }


    size_t delta_ns = (scheduling_latency_max_ns - scheduling_latency_min_ns) / bin_amount;

    *bin = (scheduling_latency_ns - scheduling_latency_min_ns) / delta_ns;

    return 0;
}

static struct scheduling_latency_histogram *find_histogram(
        struct scheduling_latency_histogram *list_head, char *codec_name)
{
    if (!list_head || !list_head->codec_name)
        return NULL;

    struct scheduling_latency_histogram *_curr = list_head;

    while (strcmp(_curr->codec_name, codec_name))
    {
        if (!_curr->next)
            return NULL;

        _curr = _curr->next;
    }

    return _curr;
}


//FIXME implement
int scheduling_latency_histogram(char *directory, char *scheduler,
        struct measurement_set **set_list, size_t scheduling_latency_min_ms,
        size_t scheduling_latency_max_ms, size_t bin_amount){

    if (!set_list)
          return -1;

    size_t _scheduling_latency_min_ns = scheduling_latency_min_ms * 1000000;
    size_t _scheduling_latency_max_ns = scheduling_latency_max_ms * 1000000;


    size_t _histogram_amount = 0;
    struct scheduling_latency_histogram *_list_head =
            malloc(sizeof(struct scheduling_latency_histogram));
    struct scheduling_latency_histogram *_list_last = _list_head;
    _list_head->codec_name = NULL;
    _list_head->timing = 0;
    _list_head->next = NULL;
    struct measurement_set **_set_curr = set_list;




    while (*_set_curr)
    {
        struct scheduling_latency_histogram *_curr;

        _curr = find_histogram(_list_head, (*_set_curr)->codec_name);

        if (!_curr)
        {
            _histogram_amount++;
            if (!_list_last->codec_name)
                _curr = _list_last;
            else
            {
                _curr = malloc(sizeof(struct scheduling_latency_histogram));
                _list_last->next = _curr;
            }

            _curr->codec_name = (*_set_curr)->codec_name;
            _curr->timing = (*_set_curr)->timing;

            _curr->bao_untouched.bins = malloc(sizeof(size_t) * bin_amount);
            memset(_curr->bao_untouched.bins, 0, sizeof(size_t) * bin_amount);
            _curr->bao_untouched.outliers = 0;

//            _curr->bao_compensate_loss.bins =
//                    malloc(sizeof(size_t) * bin_amount);
//            memset(_curr->bao_compensate_loss.bins, 0,
//                    sizeof(size_t) * bin_amount);
//            _curr->bao_compensate_loss.outliers = 0;
//
//            _curr->bao_remove_loss.bins = malloc(sizeof(size_t) * bin_amount);
//            memset(_curr->bao_remove_loss.bins, 0,
//                    sizeof(size_t) * bin_amount);
//            _curr->bao_remove_loss.outliers = 0;

            _curr->next = NULL;
            _list_last = _curr;
        }


        // MODIFY FOR scheduling_latency MEASUREMENTS FIXME

        for (size_t i = 1; i < (*_set_curr)->amount; ++i)
        {
//            size_t _packet_loss = ((*_set_curr)->list + i)->seq_num -
//                    ((*_set_curr)->list + i - 1)->seq_num - 1;

            uint64_t _timing_diff_ns = timing_get_diff_ns(
                    &((*_set_curr)->list + i)->timing,
                    &((*_set_curr)->list + i)->udp_rx_hw_timestamp
                    );

            //DEBUG PRINTOUTS
//            printf("NIC_HW %lld.%.9ld ", (long long)((*_set_curr)->list + i)->udp_rx_hw_timestamp.tv_sec, ((*_set_curr)->list + i)->udp_rx_hw_timestamp.tv_nsec);
//            printf("BEFORE %lld.%.9ld ", (long long)((*_set_curr)->list + i)->timing.tv_sec, ((*_set_curr)->list + i)->timing.tv_nsec);
//            printf("AFTER %lld.%.9ld\n", (long long)((*_set_curr)->list + i)->timing_end.tv_sec, ((*_set_curr)->list + i)->timing_end.tv_nsec);

            size_t _bin;

            // untouched: disregard information about packet loss
            if (get_bin(_timing_diff_ns, bin_amount, _scheduling_latency_min_ns,
                    _scheduling_latency_max_ns, &_bin))
                _curr->bao_untouched.outliers++;
            else
                _curr->bao_untouched.bins[_bin]++;

//            // compensate loss: divide the scheduling_latency by (packet loss + 1)
//            if (get_bin(_timing_diff_ns / (_packet_loss + 1), bin_amount,
//                    _scheduling_latency_min_ns, _scheduling_latency_max_ns, &_bin))
//                _curr->bao_compensate_loss.outliers++;
//            else
//                _curr->bao_compensate_loss.bins[_bin]++;
//
//            // remove loss: do not include the scheduling_latency measurements where
//            // packets have been lost
//            if (!_packet_loss)
//            {
//                if (get_bin(_timing_diff_ns, bin_amount, _scheduling_latency_min_ns,
//                        _scheduling_latency_max_ns, &_bin))
//                    _curr->bao_remove_loss.outliers++;
//                else
//                    _curr->bao_remove_loss.bins[_bin]++;
//            }
        }

        _set_curr++;
    }

    char _filename_outliers[FILENAME_MAX_LEN];
    snprintf(_filename_outliers, FILENAME_MAX_LEN, "%s%s_outliers.txt",
            directory, scheduler);
    FILE *_file_outliers = fopen(_filename_outliers, "w");
    if (!_file_outliers)
    {
        printf("ERROR: Could not open %s for writing of scheduling_latency "
                "outliers (%d - %s)\n", _filename_outliers, errno,
                strerror(errno));
        // TODO: free memory
        return -1;
    }

    struct scheduling_latency_histogram *_curr = _list_head;
    while (_curr)
    {
        fprintf(_file_outliers, "%s_untouched: %zu\n", _curr->codec_name,
                _curr->bao_untouched.outliers);
//        fprintf(_file_outliers, "%s_compensate_loss: %zu\n", _curr->codec_name,
//                _curr->bao_compensate_loss.outliers);
//        fprintf(_file_outliers, "%s_remove_loss: %zu\n", _curr->codec_name,
//                _curr->bao_remove_loss.outliers);

        if (write_file(directory, scheduler, _curr->codec_name, "untouched",
                _curr->bao_untouched.bins, bin_amount))
            return -1;
//        if (write_file(directory, scheduler, _curr->codec_name,
//                "compensate_loss", _curr->bao_compensate_loss.bins,
//                bin_amount))
//            return -1;
//        if (write_file(directory, scheduler, _curr->codec_name, "remove_loss",
//                _curr->bao_remove_loss.bins, bin_amount))
//            return -1;

        _curr = _curr->next;
    }






    fclose(_file_outliers);

    return 0;}




