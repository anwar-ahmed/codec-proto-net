function timings = load_packet_loss_timings(directory, scheduler)
    dir_contents = dir(directory);

    str_cmp_to = strcat(scheduler, "_packet_loss_timings.txt");

    for i = 1:length(dir_contents)
        if (strcmp(dir_contents(i).name, str_cmp_to) == 0)
            continue;
        endif

        timings = load(strcat(directory, dir_contents(i).name));

        return;
    endfor

    error('packet loss timings: no file %s_packet_loss_timings.txt found!\n', scheduler);
endfunction
