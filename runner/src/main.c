#include <ctype.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "codec.h"
#include "server.h"
#include "thread_master.h"

size_t scheduling_policy = SCHEDULING_POLICY_DEFAULT;
size_t use_sched_yield = 0;
char* server_listen_address;
char* timing_file_directory;
char* interface;


static void print_usage(char *binary_name)
{
    printf("usage: %s [-scheduling <policy>] "
            "[-use_sched_yield] "
            "[-l listen_to_address] "
            "[-i interface]\n", binary_name);
    printf("\n");
    printf("Scheduling policies: FIFO, CFS, BATCH\n");
}

static int parse_command_line(int argc, char **argv)
{

    while (1)
    {
        static struct option long_options[] =
        {
            {"help", no_argument, NULL, 'h'},
            {"scheduling", required_argument, NULL, 's'},
            {"use_sched_yield", no_argument, NULL, 'y'},
            {"listen_address", optional_argument, NULL, 'l'},
            {"timings_file_directory", optional_argument, NULL, 'd'},
            {"interface", optional_argument, NULL, 'i'},
            {0, 0, 0, 0}
        };

        int option_index = 0;
        int c = getopt_long(argc, argv, "hs:yl:d:i:", long_options, &option_index);

        if (c == -1)
        {
            // it was the end of options
            break ;
        }

        switch (c)
        {
        case 'h':
            print_usage(argv[0]);
            return -1;
        case 's':
        {
            char *optarg_lowercase = malloc(strlen(optarg) + 1);
            for (size_t i = 0; i < strlen(optarg); ++i)
            {
                optarg_lowercase[i] = tolower(optarg[i]);
            }
            optarg_lowercase[strlen(optarg)] = '\0';
            if (!strcmp(optarg_lowercase, "fifo"))
            {
                printf("Using scheduling policy: FIFO\n");
                scheduling_policy = SCHEDULING_POLICY_FIFO;
            }
            else if (!strcmp(optarg_lowercase, "cfs"))
            {
                printf("Using scheduling policy: CFS\n");
                scheduling_policy = SCHEDULING_POLICY_CFS;
            }
            else if (!strcmp(optarg_lowercase, "batch")) {
                printf("Using scheduling policy: BATCH\n");
                scheduling_policy = SCHEDULING_POLICY_BATCH;
            }
            else
            {
                printf("unknown scheduling policy: %s\n", optarg);
                printf("\n");
                print_usage(argv[0]);
                free(optarg_lowercase);
                return -1;
            }
            free(optarg_lowercase);
            break ;
        }
        case 'y':
            use_sched_yield = 1;
            break ;

        case 'l':
        {
        	server_listen_address = malloc(strlen(optarg) + 1);
        	strcpy(server_listen_address, optarg);
        	server_listen_address[strlen(optarg)] = '\0';
        	break;
        }
        case 'd': {
            timing_file_directory = malloc(strlen(optarg) + 1);
            strcpy(timing_file_directory, optarg);
            timing_file_directory[strlen(optarg)] = '\0';
            break;
        }
        case 'i': {
            interface = malloc(strlen(optarg) + 1);
            strcpy(interface, optarg);
            interface[strlen(optarg)] = '\0';
            break;
        }

        default:
            printf("unknown parameter\n");
            printf("\n");
            print_usage(argv[0]);
            return -1;
        }
    }

    if (optind < argc)
    {
        printf("too many parameters\n");
        printf("\n");
        print_usage(argv[0]);
        return -1;
    }

    return 0;
}

int main(int argc, char **argv)
{

	if (parse_command_line(argc, argv))
    {
        return -1;
    }

    int errno_code;
    char *error_str;

    printf("Opening server socket...\n");
    socket_handle_t socket_handle;
    if (server_open(SERVER_LISTEN_PORT,
            server_listen_address,
            &socket_handle,
            &errno_code,
            &error_str))
    {
        printf("Server socket opening failed: %s (errno: %d, %s)\n",
                error_str, errno_code, strerror(errno_code));
        free(error_str);
        return 1;
    }

    printf("Server socket opened!\n");

    printf("Accepting client connection...\n");
    if (server_accept(socket_handle,
            &errno_code,
            &error_str,
            interface,
            server_listen_address))
    {
        printf("Accepting client connection failed: %s (errno: %d, %s)\n",
                error_str, errno_code, strerror(errno_code));
        free(error_str);
        server_close(socket_handle);
        return 2;
    }
    printf("Client connected!\n");

    thread_master(socket_handle);

    printf("Client disconnected!\n");

    server_close(socket_handle);

    printf("Campaign completed...exiting...");

    exit(0);
}

