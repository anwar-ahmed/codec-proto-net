
#pragma once

#include "measurements.h"

int packet_loss_bins(char *directory, char *scheduler,
        struct measurement_set **set_list,
        struct scheduler_stats *scheduler_stats);

