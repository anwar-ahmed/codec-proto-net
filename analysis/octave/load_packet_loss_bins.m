function bins = load_packet_loss_bins(directory, scheduler)
    dir_contents = dir(directory);

    str_cmp_to = strcat(scheduler, "_packet_loss_bins.txt");

    for i = 1:length(dir_contents)
        if (strcmp(dir_contents(i).name, str_cmp_to) == 0)
            continue;
        endif

        bins = load(strcat(directory, dir_contents(i).name));

        return;
    endfor

    error('packet loss bins: no file %s_packet_loss_bins.txt found!\n', scheduler);
endfunction
