
#pragma once

#include <stdint.h>

struct linked_list_node
{
    struct linked_list_node *next;
    uint16_t key;
    void *value;
};

struct linked_list
{
    struct linked_list_node *first;
};

typedef struct linked_list *linked_list_t;
typedef struct linked_list_node linked_list_node_t;

linked_list_t linked_list_create();
int linked_list_destroy(linked_list_t list);
int linked_list_insert(linked_list_t list, uint16_t key, void *value);
int linked_list_remove(linked_list_t list, uint16_t key, void **value);
int linked_list_length(linked_list_t list);
int linked_list_search(linked_list_t list, uint16_t key, void **value);
int linked_list_get(linked_list_t list, int num, uint16_t *key, void **value);
int linked_list_get_first(linked_list_t list, linked_list_node_t **first);
int linked_list_get_next(
        linked_list_node_t *node, linked_list_node_t **next, uint16_t *key, void **value);
int linked_list_get_node(
        linked_list_t list, uint16_t key, linked_list_node_t **node, void **value);
int linked_list_get_and_move_next(
        linked_list_node_t **node, uint16_t *key, void **value);
int linked_list_move_next(linked_list_node_t **node);
int linked_list_pop(linked_list_t list, uint16_t *key, void **value);

