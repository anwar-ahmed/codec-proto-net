#!/bin/bash

echo "Trying to execute script as root..."

if [ "$(id -u)" != "0" ]; then
   echo "...fail you have to run this script as root" 1>&2
   exit 1
fi

echo "Setting ulimits for protonet"

ulimit -H -n 65536
ulimit -n 65536

echo "-------------------------------------------------------------"
echo "Soft limits: "
echo "-------------------------------------------------------------"

ulimit -a $1

echo "-------------------------------------------------------------"
echo "Hard limits: "
echo "-------------------------------------------------------------"

ulimit -H -a $1

echo "-------------------------------------------------------------"

