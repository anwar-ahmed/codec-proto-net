/*
   This file is part of Wool, a library for fine-grained independent
   task parallelism

   Copyright 2009-2016 Karl-Filip Faxén, kff@sics.se

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   This is Wool version @WOOL_VERSION@
*/

#ifndef WOOL_H
#define WOOL_H

#include "wool-common.h"

#define LOOP_BODY_0(NAME, COST, IXTY, IXNAME)                         \
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME);          \
                                                                      \
VOID_TASK_2(NAME##_TREE, IXTY, __from, IXTY, __to)                    \
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i );                                     \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to );                                \
    CALL( NAME##_TREE, __from, __mid );                               \
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME)           \

// Task definition for arity 1

#define TASK_FORW_1(RTYPE, NAME, ATYPE_1 )                            \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1);\

#define VOID_TASK_FORW_1(NAME, ATYPE_1 )                              \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1);\

#define TASK_DECL_1(RTYPE, NAME, ATYPE_1)                             \
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1)                        \
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1 );                                      \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1 );                                      \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1);                 \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1 );                          \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1 );                          \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_1(RTYPE, NAME, ATYPE_1, ARG_1 )                     \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1);                 \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1);       \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1)                  \
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1 );                          \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1)  \

#define TASK_1(RTYPE, NAME, ATYPE_1, ARG_1 )                          \
TASK_DECL_1(RTYPE, NAME, ATYPE_1)                                     \
TASK_IMPL_1(RTYPE, NAME, ATYPE_1, ARG_1)                              \
                                                                      \

#define VOID_TASK_DECL_1(NAME, ATYPE_1)                               \
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1)                        \
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1 );                                      \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1 );                                      \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1);                  \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1 );                          \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1 );                          \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_1(NAME, ATYPE_1, ARG_1 )                       \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1);                  \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_1), 1 );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1);        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1)                   \
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1 );                          \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1)   \

#define VOID_TASK_1(NAME, ATYPE_1, ARG_1 )                            \
VOID_TASK_DECL_1(NAME, ATYPE_1)                                       \
VOID_TASK_IMPL_1(NAME, ATYPE_1, ARG_1)                                \
                                                                      \

#define LOOP_BODY_1(NAME, COST, IXTY, IXNAME, ATYPE_1, ARG_1)         \
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1);\
                                                                      \
VOID_TASK_3(NAME##_TREE, IXTY, __from, IXTY, __to, ATYPE_1, a1)       \
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i, a1 );                                 \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to, a1 );                            \
    CALL( NAME##_TREE, __from, __mid, a1 );                           \
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1)\

// Task definition for arity 2

#define TASK_FORW_2(RTYPE, NAME, ATYPE_1, ATYPE_2 )                   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2);\

#define VOID_TASK_FORW_2(NAME, ATYPE_1, ATYPE_2 )                     \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2);\

#define TASK_DECL_2(RTYPE, NAME, ATYPE_1, ATYPE_2)                    \
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2)            \
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2 );                                  \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2 );                                  \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2);     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2 );                      \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2 );                      \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_2(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2 )     \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2);     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2)      \
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2 );                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2)\

#define TASK_2(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2 )          \
TASK_DECL_2(RTYPE, NAME, ATYPE_1, ATYPE_2)                            \
TASK_IMPL_2(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2)              \
                                                                      \

#define VOID_TASK_DECL_2(NAME, ATYPE_1, ATYPE_2)                      \
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2)            \
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2 );                                  \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2 );                                  \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2);      \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2 );                      \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2 );                      \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_2(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2 )       \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2);      \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2)       \
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2 );                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2)\

#define VOID_TASK_2(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2 )            \
VOID_TASK_DECL_2(NAME, ATYPE_1, ATYPE_2)                              \
VOID_TASK_IMPL_2(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2)                \
                                                                      \

#define LOOP_BODY_2(NAME, COST, IXTY, IXNAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2)\
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2);\
                                                                      \
VOID_TASK_4(NAME##_TREE, IXTY, __from, IXTY, __to, ATYPE_1, a1, ATYPE_2, a2)\
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i, a1, a2 );                             \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to, a1, a2 );                        \
    CALL( NAME##_TREE, __from, __mid, a1, a2 );                       \
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2)\

// Task definition for arity 3

#define TASK_FORW_3(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3 )          \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3);\

#define VOID_TASK_FORW_3(NAME, ATYPE_1, ATYPE_2, ATYPE_3 )            \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3);\

#define TASK_DECL_3(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3)           \
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3 );                              \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3 );                              \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3 );                  \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3 );                  \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_3(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3 )\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3 );                  \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3)\

#define TASK_3(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3 )\
TASK_DECL_3(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3)                   \
TASK_IMPL_3(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3)\
                                                                      \

#define VOID_TASK_DECL_3(NAME, ATYPE_1, ATYPE_2, ATYPE_3)             \
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3 );                              \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3 );                              \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3 );                  \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3 );                  \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_3(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3 )\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3 );                  \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3)\

#define VOID_TASK_3(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3 )\
VOID_TASK_DECL_3(NAME, ATYPE_1, ATYPE_2, ATYPE_3)                     \
VOID_TASK_IMPL_3(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3)\
                                                                      \

#define LOOP_BODY_3(NAME, COST, IXTY, IXNAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3)\
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3);\
                                                                      \
VOID_TASK_5(NAME##_TREE, IXTY, __from, IXTY, __to, ATYPE_1, a1, ATYPE_2, a2, ATYPE_3, a3)\
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i, a1, a2, a3 );                         \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to, a1, a2, a3 );                    \
    CALL( NAME##_TREE, __from, __mid, a1, a2, a3 );                   \
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3)\

// Task definition for arity 4

#define TASK_FORW_4(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4 ) \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4);\

#define VOID_TASK_FORW_4(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4 )   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4);\

#define TASK_DECL_4(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4)  \
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4 );                          \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4 );                          \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4 );              \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4 );              \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_4(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4 )\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4 );              \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4)\

#define TASK_4(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4 )\
TASK_DECL_4(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4)          \
TASK_IMPL_4(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4)\
                                                                      \

#define VOID_TASK_DECL_4(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4)    \
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4 );                          \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4 );                          \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4 );              \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4 );              \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_4(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4 )\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4 );              \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4)\

#define VOID_TASK_4(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4 )\
VOID_TASK_DECL_4(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4)            \
VOID_TASK_IMPL_4(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4)\
                                                                      \

#define LOOP_BODY_4(NAME, COST, IXTY, IXNAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4)\
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4);\
                                                                      \
VOID_TASK_6(NAME##_TREE, IXTY, __from, IXTY, __to, ATYPE_1, a1, ATYPE_2, a2, ATYPE_3, a3, ATYPE_4, a4)\
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i, a1, a2, a3, a4 );                     \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to, a1, a2, a3, a4 );                \
    CALL( NAME##_TREE, __from, __mid, a1, a2, a3, a4 );               \
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4)\

// Task definition for arity 5

#define TASK_FORW_5(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5);\

#define VOID_TASK_FORW_5(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5);\

#define TASK_DECL_5(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5 );                      \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5 );                      \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5 );          \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5 );          \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_5(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5 )\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5 );          \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5)\

#define TASK_5(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5 )\
TASK_DECL_5(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5) \
TASK_IMPL_5(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5)\
                                                                      \

#define VOID_TASK_DECL_5(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5 );                      \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5 );                      \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5 );          \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5 );          \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_5(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5 )\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5 );          \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5)\

#define VOID_TASK_5(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5 )\
VOID_TASK_DECL_5(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5)   \
VOID_TASK_IMPL_5(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5)\
                                                                      \

#define LOOP_BODY_5(NAME, COST, IXTY, IXNAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5)\
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5);\
                                                                      \
VOID_TASK_7(NAME##_TREE, IXTY, __from, IXTY, __to, ATYPE_1, a1, ATYPE_2, a2, ATYPE_3, a3, ATYPE_4, a4, ATYPE_5, a5)\
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i, a1, a2, a3, a4, a5 );                 \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to, a1, a2, a3, a4, a5 );            \
    CALL( NAME##_TREE, __from, __mid, a1, a2, a3, a4, a5 );           \
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5)\

// Task definition for arity 6

#define TASK_FORW_6(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6);\

#define VOID_TASK_FORW_6(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6);\

#define TASK_DECL_6(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6 );                  \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6 );                  \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6 );      \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6 );      \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_6(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6 )\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6 );      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6)\

#define TASK_6(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6 )\
TASK_DECL_6(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6)\
TASK_IMPL_6(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6)\
                                                                      \

#define VOID_TASK_DECL_6(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6 );                  \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6 );                  \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6 );      \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6 );      \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_6(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6 )\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6 );      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6)\

#define VOID_TASK_6(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6 )\
VOID_TASK_DECL_6(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6)\
VOID_TASK_IMPL_6(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6)\
                                                                      \

#define LOOP_BODY_6(NAME, COST, IXTY, IXNAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6)\
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6);\
                                                                      \
VOID_TASK_8(NAME##_TREE, IXTY, __from, IXTY, __to, ATYPE_1, a1, ATYPE_2, a2, ATYPE_3, a3, ATYPE_4, a4, ATYPE_5, a5, ATYPE_6, a6)\
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i, a1, a2, a3, a4, a5, a6 );             \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to, a1, a2, a3, a4, a5, a6 );        \
    CALL( NAME##_TREE, __from, __mid, a1, a2, a3, a4, a5, a6 );       \
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6)\

// Task definition for arity 7

#define TASK_FORW_7(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7);\

#define VOID_TASK_FORW_7(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7);\

#define TASK_DECL_7(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
  *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) = a7;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7 );              \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7 );              \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7 );  \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7 );  \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_7(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7 )\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6, a7 );  \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7)\

#define TASK_7(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7 )\
TASK_DECL_7(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7)\
TASK_IMPL_7(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7)\
                                                                      \

#define VOID_TASK_DECL_7(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
  *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) = a7;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7 );              \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7 );              \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7 );  \
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7 );  \
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_7(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7 )\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6, a7 );  \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7)\

#define VOID_TASK_7(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7 )\
VOID_TASK_DECL_7(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7)\
VOID_TASK_IMPL_7(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7)\
                                                                      \

#define LOOP_BODY_7(NAME, COST, IXTY, IXNAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7)\
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7);\
                                                                      \
VOID_TASK_9(NAME##_TREE, IXTY, __from, IXTY, __to, ATYPE_1, a1, ATYPE_2, a2, ATYPE_3, a3, ATYPE_4, a4, ATYPE_5, a5, ATYPE_6, a6, ATYPE_7, a7)\
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i, a1, a2, a3, a4, a5, a6, a7 );         \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to, a1, a2, a3, a4, a5, a6, a7 );    \
    CALL( NAME##_TREE, __from, __mid, a1, a2, a3, a4, a5, a6, a7 );   \
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7)\

// Task definition for arity 8

#define TASK_FORW_8(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8);\

#define VOID_TASK_FORW_8(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8);\

#define TASK_DECL_8(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
  *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) = a7;\
  *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) = a8;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8 );          \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8 );          \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8 );\
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8 );\
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_8(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8 )\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6, a7, a8 );\
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8)\

#define TASK_8(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8 )\
TASK_DECL_8(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8)\
TASK_IMPL_8(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8)\
                                                                      \

#define VOID_TASK_DECL_8(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
  *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) = a7;\
  *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) = a8;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8 );          \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8 );          \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8 );\
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8 );\
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_8(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8 )\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6, a7, a8 );\
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8)\

#define VOID_TASK_8(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8 )\
VOID_TASK_DECL_8(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8)\
VOID_TASK_IMPL_8(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8)\
                                                                      \

#define LOOP_BODY_8(NAME, COST, IXTY, IXNAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8)\
                                                                      \
static unsigned long const NAME##__min_iters__                        \
   = COST > FINEST_GRAIN ? 1 : FINEST_GRAIN / ( COST ? COST : 20 );   \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8);\
                                                                      \
VOID_TASK_10(NAME##_TREE, IXTY, __from, IXTY, __to, ATYPE_1, a1, ATYPE_2, a2, ATYPE_3, a3, ATYPE_4, a4, ATYPE_5, a5, ATYPE_6, a6, ATYPE_7, a7, ATYPE_8, a8)\
{                                                                     \
  if( __to - __from <= NAME##__min_iters__ ) {                        \
    IXTY __i;                                                         \
    for( __i = __from; __i < __to; __i++ ) {                          \
      NAME##_LOOP( __self, __i, a1, a2, a3, a4, a5, a6, a7, a8 );     \
    }                                                                 \
  } else {                                                            \
    IXTY __mid = (__from + __to) / 2;                                 \
    SPAWN( NAME##_TREE, __mid, __to, a1, a2, a3, a4, a5, a6, a7, a8 );\
    CALL( NAME##_TREE, __from, __mid, a1, a2, a3, a4, a5, a6, a7, a8 );\
    SYNC( NAME##_TREE );                                              \
  }                                                                   \
}                                                                     \
                                                                      \
static inline void NAME##_LOOP(Worker *__self, IXTY IXNAME, ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8)\

// Task definition for arity 9

#define TASK_FORW_9(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9);\

#define VOID_TASK_FORW_9(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9);\

#define TASK_DECL_9(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
  *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) = a7;\
  *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) = a8;\
  *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) = a9;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8, a9 );      \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8, a9 );      \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8, a9 );\
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8, a9 );\
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_9(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9 )\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8, ATYPE_9 ARG_9);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6, a7, a8, a9 );\
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8, ATYPE_9 ARG_9)\

#define TASK_9(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9 )\
TASK_DECL_9(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9)\
TASK_IMPL_9(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9)\
                                                                      \

#define VOID_TASK_DECL_9(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
  *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) = a7;\
  *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) = a8;\
  *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) = a9;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8, a9 );      \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8, a9 );      \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8, a9 );\
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8, a9 );\
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_9(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9 )\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8, ATYPE_9 ARG_9);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6, a7, a8, a9 );\
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8, ATYPE_9 ARG_9)\

#define VOID_TASK_9(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9 )\
VOID_TASK_DECL_9(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9)\
VOID_TASK_IMPL_9(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9)\
                                                                      \


// Task definition for arity 10

#define TASK_FORW_10(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10);\
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );  \
  static inline __attribute__((__always_inline__))                    \
    RTYPE NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10);\

#define VOID_TASK_FORW_10(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10 )\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SPAWN_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10);\
  static inline __attribute__((__always_inline__))                    \
    void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) );   \
  static inline __attribute__((__always_inline__))                    \
    void NAME##_CALL_DSP( Worker *__self, int _WOOL_(fs_in_task), ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10);\

#define TASK_DECL_10(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
    RTYPE res;                                                        \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
  *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) = a7;\
  *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) = a8;\
  *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) = a9;\
  *(ATYPE_10 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ) ) = a10;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 ); \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 ); \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 );\
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 );\
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC(Worker *__self)                                     \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
    RTYPE res;                                                        \
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
    res =  NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ), *(ATYPE_10 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return res;                                                       \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ( (NAME##_TD *) cached_top )->d.res;                       \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )       \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define TASK_IMPL_10(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9, ATYPE_10, ARG_10 )\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  t->d.res = NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ), *(ATYPE_10 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
    t->d.res = NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ), *(ATYPE_10 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8, ATYPE_9 ARG_9, ATYPE_10 ARG_10);\
                                                                      \
RTYPE NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 );\
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
RTYPE NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8, ATYPE_9 ARG_9, ATYPE_10 ARG_10)\

#define TASK_10(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9, ATYPE_10, ARG_10 )\
TASK_DECL_10(RTYPE, NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10)\
TASK_IMPL_10(RTYPE, NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9, ATYPE_10, ARG_10)\
                                                                      \

#define VOID_TASK_DECL_10(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10)\
                                                                      \
typedef struct _##NAME##_TD {                                         \
  TASK_COMMON_FIELDS( struct _##NAME##_TD * )                         \
  union {                                                             \
                                                                      \
  } d;                                                                \
} NAME##_TD;                                                          \
                                                                      \
static inline __attribute__((__always_inline__))                      \
char* NAME##_FREE_SPACE(Task* cached_top)                             \
{                                                                     \
  const size_t _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
  const size_t _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  return _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ), double );\
}                                                                     \
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t);                       \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN(Worker *__self , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10)\
{                                                                     \
  Task* cached_top = __self->pr_top;                                  \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) cached_top) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
  *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ) = a1;       \
  *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ) = a2;\
  *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ) = a3;\
  *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ) = a4;\
  *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ) = a5;\
  *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ) = a6;\
  *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ) = a7;\
  *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ) = a8;\
  *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ) = a9;\
  *(ATYPE_10 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ) ) = a10;\
                                                                      \
                                                                      \
  COMPILER_FENCE;                                                     \
                                                                      \
  _WOOL_(fast_spawn)( __self, cached_top, (wrapper_t) &NAME##_WRAP ); \
                                                                      \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SPAWN_DSP(Worker *__self, int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10)\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 ); \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    NAME##_SPAWN( __self , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 ); \
  }                                                                   \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10);\
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_CALL_DSP( Worker *_WOOL_(self), int _WOOL_(fs_in_task), ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10 )\
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 );\
  } else {                                                            \
    _WOOL_(self) = _WOOL_(slow_get_self)( );                          \
    return NAME##_CALL( _WOOL_(self) , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 );\
  }                                                                   \
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
/* This implementation has the PUB function only in the implementation file, so uses from other\
   compilation units call it.                                         \
*/                                                                    \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp );                \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC(Worker *__self)                                      \
{                                                                     \
  WOOL_WHEN_MSPAN( hrtime_t e_span; )                                 \
  Task *jfp = __self->join_first_private;                             \
  Task *cached_top = __self->pr_top;                                  \
                                                                      \
  if( MAKE_TRACE ||                                                   \
      ( LOG_EVENTS &&                                                 \
      __self->curr_block_fidx + ( cached_top - __self->curr_block_base ) <= __self->n_public ) )\
  {                                                                   \
    logEvent( __self, 6 );                                            \
  }                                                                   \
                                                                      \
  if( __builtin_expect( jfp < cached_top, 1 ) ) {                     \
    Task *t = --cached_top;                                           \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
                                                                      \
    __self->pr_top = cached_top;                                      \
    PR_INC( __self, CTR_inlined );                                    \
                                                                      \
    WOOL_MSPAN_BEFORE_INLINE( e_span, t );                            \
                                                                      \
     NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ), *(ATYPE_10 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ) ) );\
    WOOL_MSPAN_AFTER_INLINE( e_span, t );                             \
    if( MAKE_TRACE ) {                                                \
      logEvent( __self, 8 );                                          \
    }                                                                 \
    return ;                                                          \
  } else {                                                            \
    cached_top = NAME##_PUB( __self, cached_top, jfp );               \
    return ;                                                          \
  }                                                                   \
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_SYNC_DSP( Worker *__self, int _WOOL_(fs_in_task) )        \
{                                                                     \
  if( _WOOL_(fs_in_task) ) {                                          \
    return NAME##_SYNC( __self );                                     \
  } else {                                                            \
    __self = _WOOL_(slow_get_self)( );                                \
    return NAME##_SYNC( __self );                                     \
  }                                                                   \
}                                                                     \

#define VOID_TASK_IMPL_10(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9, ATYPE_10, ARG_10 )\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10);\
                                                                      \
/** SPAWN related functions **/                                       \
                                                                      \
void NAME##_WRAP(Worker *__self, NAME##_TD *t)                        \
{                                                                     \
  const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
  const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
  char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
   NAME##_CALL( __self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ), *(ATYPE_10 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ) ) );\
}                                                                     \
                                                                      \
/** SYNC related functions **/                                        \
                                                                      \
Task *NAME##_PUB(Worker *self, Task *top, Task *jfp )                 \
{                                                                     \
  unsigned long ps = self->public_size;                               \
                                                                      \
  WOOL_WHEN_AS( int us; )                                             \
                                                                      \
  grab_res_t res = WOOL_FAST_EXC ? TF_EXC : TF_OCC;                   \
                                                                      \
  if(                                                                 \
        ( WOOL_WHEN_AS_C( us = self->unstolen_stealable )             \
         __builtin_expect( (unsigned long) jfp - (unsigned long) top < ps, 1 ) )\
         && __builtin_expect( WOOL_LS_TEST(us), 1 )                   \
         && (res = _WOOL_(grab_in_sync)( self, (top)-1 ),             \
             (                                                        \
               WOOL_WHEN_AS_C( self->unstolen_stealable = us-1 )      \
               __builtin_expect( res != TF_OCC, 1 ) ) )               \
   ) {                                                                \
    /* Semi fast case */                                              \
    NAME##_TD *t = (NAME##_TD *) --top;                               \
    const unsigned long _WOOL_max_align = _WOOL_(max)( __alignof__(ATYPE_10), _WOOL_(max)( __alignof__(ATYPE_9), _WOOL_(max)( __alignof__(ATYPE_8), _WOOL_(max)( __alignof__(ATYPE_7), _WOOL_(max)( __alignof__(ATYPE_6), _WOOL_(max)( __alignof__(ATYPE_5), _WOOL_(max)( __alignof__(ATYPE_4), _WOOL_(max)( __alignof__(ATYPE_3), _WOOL_(max)( __alignof__(ATYPE_2), _WOOL_(max)( __alignof__(ATYPE_1), 1 ) ) ) ) ) ) ) ) ) );\
    const unsigned long _WOOL_(sss) = sizeof( __wool_task_common ) + _WOOL_max_align - 1;\
    char *_WOOL_(p) = ((char *) t) + _WOOL_(sss) - _WOOL_(sss) % _WOOL_max_align;\
                                                                      \
    self->pr_top = top;                                               \
    PR_INC( self, CTR_inlined );                                      \
     NAME##_CALL( self , *(ATYPE_1 *)( _WOOL_(p) + _WOOL_ALIGNTO( 0, ATYPE_1 ) ), *(ATYPE_2 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ) ), *(ATYPE_3 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ) ), *(ATYPE_4 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ) ), *(ATYPE_5 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ) ), *(ATYPE_6 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ) ), *(ATYPE_7 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ) ), *(ATYPE_8 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ) ), *(ATYPE_9 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ) ), *(ATYPE_10 *)( _WOOL_(p) + _WOOL_ALIGNTO( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( _WOOL_OFFSET_AFTER( 0, ATYPE_1 ), ATYPE_2 ), ATYPE_3 ), ATYPE_4 ), ATYPE_5 ), ATYPE_6 ), ATYPE_7 ), ATYPE_8 ), ATYPE_9 ), ATYPE_10 ) ) );\
    return top;                                                       \
  } else {                                                            \
      /* An exceptional case */                                       \
      top = _WOOL_(new_slow_sync)( self, top, res );                  \
      return top;                                                     \
  }                                                                   \
                                                                      \
}                                                                     \
                                                                      \
/** CALL related functions **/                                        \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8, ATYPE_9 ARG_9, ATYPE_10 ARG_10);\
                                                                      \
void NAME##_CALL(Worker *_WOOL_(self) , ATYPE_1 a1, ATYPE_2 a2, ATYPE_3 a3, ATYPE_4 a4, ATYPE_5 a5, ATYPE_6 a6, ATYPE_7 a7, ATYPE_8 a8, ATYPE_9 a9, ATYPE_10 a10)\
{                                                                     \
  return NAME##_WRK( _WOOL_(self), 1 , a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 );\
}                                                                     \
                                                                      \
static inline __attribute__((__always_inline__))                      \
void NAME##_WRK(Worker *__self, int _WOOL_(in_task), ATYPE_1 ARG_1, ATYPE_2 ARG_2, ATYPE_3 ARG_3, ATYPE_4 ARG_4, ATYPE_5 ARG_5, ATYPE_6 ARG_6, ATYPE_7 ARG_7, ATYPE_8 ARG_8, ATYPE_9 ARG_9, ATYPE_10 ARG_10)\

#define VOID_TASK_10(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9, ATYPE_10, ARG_10 )\
VOID_TASK_DECL_10(NAME, ATYPE_1, ATYPE_2, ATYPE_3, ATYPE_4, ATYPE_5, ATYPE_6, ATYPE_7, ATYPE_8, ATYPE_9, ATYPE_10)\
VOID_TASK_IMPL_10(NAME, ATYPE_1, ARG_1, ATYPE_2, ARG_2, ATYPE_3, ARG_3, ATYPE_4, ARG_4, ATYPE_5, ARG_5, ATYPE_6, ARG_6, ATYPE_7, ARG_7, ATYPE_8, ARG_8, ATYPE_9, ARG_9, ATYPE_10, ARG_10)\
                                                                      \


#endif
