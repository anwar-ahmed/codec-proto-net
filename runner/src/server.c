
#include "server.h"
#include "ctrl_msg.h"

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <linux/if.h>
#include <linux/net_tstamp.h>
#include <linux/sockios.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <netinet/sctp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// 1 is reserved for syscall error
#define SERVER_SOCKET_NOT_OPENED 0x0002
#define SERVER_SOCKET_CLOSED 0x0003

static void set_error(int error_no, char* error_str,
        char** errstr, int* errno_code) {
    int error_str_len = strlen(error_str) + 1;
    *errstr = (char*) malloc(error_str_len);
    strncpy(*errstr, error_str, error_str_len);
    *errno_code = error_no;
}


int server_open(int port, char *server_listen_address, socket_handle_t *socket_handle,
        int *errno_code, char **errstr)
{

    int socket_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);
    if (socket_fd == -1)
    {
        set_error(errno, "socket() error (SCTP)", errstr, errno_code);
        return -1;
    }

    struct sockaddr_in socket_addr;

    memset(&socket_addr, 0, sizeof(socket_addr));

    socket_addr.sin_family = AF_INET;
    socket_addr.sin_port = htons(port);

    if(server_listen_address){
    	socket_addr.sin_addr.s_addr = inet_addr(server_listen_address);
    }
    else {
    	socket_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    }


    if (bind(socket_fd, (struct sockaddr *) &socket_addr, sizeof(socket_addr)) == -1)
    {
        set_error(errno, "bind() error (SCTP)", errstr, errno_code);
        return -1;
    }

    struct sctp_initmsg sctp_init_msg;
    memset(&sctp_init_msg, 0, sizeof(sctp_init_msg));
    sctp_init_msg.sinit_num_ostreams = 65535;
    sctp_init_msg.sinit_max_instreams = 65535;
    sctp_init_msg.sinit_max_attempts = 1;

    if (setsockopt(socket_fd, IPPROTO_SCTP, SCTP_INITMSG, &sctp_init_msg,
            sizeof(sctp_init_msg)) == -1)
    {
        set_error(errno, "setsockopt() error", errstr, errno_code);
        return -1;
    }

    if (listen(socket_fd, 1) == -1)
    {
        set_error(errno, "listen() error", errstr, errno_code);
        return -1;
    }

    *socket_handle = malloc(sizeof(struct socket_handle_struct));
    (*socket_handle)->fd_sctp_listen = socket_fd;
    (*socket_handle)->fd_sctp_connection = -SERVER_SOCKET_NOT_OPENED;
    (*socket_handle)->fd_udp = -SERVER_SOCKET_NOT_OPENED;
    (*socket_handle)->addr = NULL;
    (*socket_handle)->addr_len = 0;

    return 0;
}

int server_accept(socket_handle_t socket_handle, int *errno_code,
        char **errstr, char* interface, char *server_listen_address)
{
    if (!socket_handle ||
            socket_handle->fd_sctp_listen <= 0 ||
            socket_handle->fd_sctp_connection != -2)
    {
        set_error(EINVAL, "accept_client() error", errstr, errno_code);
        return -1;
    }

    struct sockaddr_in socket_client_addr;
    socklen_t client_addr_len = sizeof(struct sockaddr_in);
    int socket_fd_client = accept(socket_handle->fd_sctp_listen,
            (struct sockaddr *) &socket_client_addr, &client_addr_len);
    if (socket_fd_client == -1)
    {
        set_error(errno, "accept() error", errstr, errno_code);
        return -1;
    }

    struct sctp_event_subscribe sctp_events;
    memset((void *) &sctp_events, 0, sizeof(sctp_events));
    sctp_events.sctp_data_io_event = 1;
    if (setsockopt(socket_fd_client, SOL_SCTP, SCTP_EVENTS, (const void *) &sctp_events,
            sizeof(sctp_events)) == -1)
    {
        set_error(errno, "setsockopt() error", errstr, errno_code);
        return -1;
    }

    int socket_fd_udp = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_fd_udp == -1)
    {
        set_error(errno, "socket() error (UDP)", errstr, errno_code);
        return -1;
    }


    /* if interface is set use socket Timestamping option */

    if (interface)
    {
        ////////////////////////////////////////////////////////////////////////////////////////////
        // SO_TIMESTAMP:
        // Generate time stamp for each incoming packet using the (not necessarily
        // monotonous!) system time. Result is returned via recv_msg() in a
        // control message as timeval (usec resolution).
        //
        //
        // SO_TIMESTAMPNS
        // Same time stamping mechanism as SO_TIMESTAMP, but returns result as
        // timespec (nsec resolution).
        //
        // https://www.kernel.org/doc/Documentation/networking/timestamping.txt
        // http://lxr.free-electrons.com/source/Documentation/networking/timestamping/timestamping.c
        ///////////////////////////////////////////////////////////////////////////////////////////

        int so_timestamping_flags = 0;
        so_timestamping_flags |= SOF_TIMESTAMPING_RX_SOFTWARE;
        if (setsockopt(socket_fd_udp,
                SOL_SOCKET,
                SO_TIMESTAMPNS,
                &so_timestamping_flags,
                sizeof(so_timestamping_flags)) < 0)
        {
            set_error(errno, "UDP setsockopt() error ", errstr, errno_code);
        }
        else
        {
            printf("TimeStamping has been activated on UDP socket\n");
        }
    }

    struct sockaddr_in socket_udp_addr;
    memset(&socket_udp_addr, 0, sizeof(struct sockaddr_in));
    socket_udp_addr.sin_family = AF_INET;
    socket_udp_addr.sin_port = htons(SERVER_LISTEN_PORT);

    //socket_udp_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(server_listen_address){
        socket_udp_addr.sin_addr.s_addr = inet_addr(server_listen_address);
    }
    else {
        socket_udp_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    }

    if (bind(socket_fd_udp, (struct sockaddr *) &socket_udp_addr,
            sizeof(struct sockaddr_in)) == -1)
    {
        set_error(errno, "bind() error (UDP)", errstr, errno_code);
        return -1;
    }

    socket_handle->fd_sctp_connection = socket_fd_client;

    if (ctrl_msg_send_connection_udp_opened_inf(socket_handle))
    {
        set_error(errno, "connection_udp_open() error", errstr, errno_code);
        return -1;
    }

    enum CTRL_MSG_TYPE ctrl_msg_type;
    while (1)
    {
        int error_no = 0xDEADBEEF;

        if ((error_no =
                ctrl_msg_recv(socket_handle, &ctrl_msg_type, NULL, NULL,
                NULL)))
        {
            if (error_no == -1)

            set_error(errno, "ctrl_msg_recv() error", errstr, errno_code);
            return -1;
        }

        if (ctrl_msg_type == CTRL_MSG_TYPE_CONNECTION_UDP_OPENED_ACK)
            break ;

        printf("ERROR: Received unexpected ctrl_msg: %d\n", ctrl_msg_type);
    }

    socket_handle->fd_udp = socket_fd_udp;
    socket_handle->addr = malloc(sizeof(struct sockaddr_in));
    memcpy(socket_handle->addr, &socket_udp_addr, sizeof(struct sockaddr_in));
    socket_handle->addr_len = client_addr_len;

    return 0;
}

static void server_close_fd(int *socket_fd)
{
    if (*socket_fd <= 0)
        return ;

    close(*socket_fd);
    *socket_fd = -SERVER_SOCKET_CLOSED;
}

int server_close(socket_handle_t socket_handle)
{
    if (!socket_handle)
        return -1;

    server_close_fd(&socket_handle->fd_sctp_connection);
    server_close_fd(&socket_handle->fd_sctp_listen);
    server_close_fd(&socket_handle->fd_udp);

    if (socket_handle->addr)
        free(socket_handle->addr);

    free(socket_handle);

    return 0;
}

