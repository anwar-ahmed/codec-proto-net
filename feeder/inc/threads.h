
#pragma once

#include <pthread.h>

#include "ctrl_msg.h"
#include "feeder_config.h"

#define FEEDER_THREAD_ERROR_SET_SCHEDULER 0x0001
#define FEEDER_THREAD_ERROR_OPEN_FILE 0x0011
#define FEEDER_THREAD_ERROR_READ_FILE 0x0012
#define FEEDER_THREAD_ERROR_READ_FILE2 0x8013
#define FEEDER_THREAD_ERROR_TIMER_CREATE 0x0020
#define FEEDER_THREAD_ERROR_TIMER_ARM 0x0021
#define FEEDER_THREAD_ERROR_TIMER_READ 0x8022
#define FEEDER_THREAD_ERROR_TIMER_OVERFLOW 0x8023
#define FEEDER_THREAD_ERROR_SEND_UDP 0x8100
#define FEEDER_THREAD_ERROR_PREMATURE_EXIT 0x8200

int feeder_thread_thread_create_request(socket_handle_t socket_handle,
        struct codec_config *codec_config, int *id);

int feeder_thread_thread_create_create(socket_handle_t socket_handle, int id,
        struct codec_config *codec_config);

int feeder_thread_control_thread_create(
        pthread_t *thread, struct codec_config **codec_configs);
