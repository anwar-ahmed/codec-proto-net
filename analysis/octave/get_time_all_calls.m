function [earliest, latest] = get_time_all_calls(measurements)
    earliest = 0;
    latest = inf;

    for i = 1:length(measurements)
        min_time = measurements(i).timings(1);
        max_time = measurements(i).timings(end);

        if (min_time > earliest)
            earliest = min_time;
        endif
        if (max_time < latest)
            latest = max_time;
        endif
    endfor
endfunction
