
#pragma once

#include <pthread.h>
#include <stddef.h>
#include <time.h>
#include <usc.h>
#include <stdint.h>
#include <sys/time.h>


#include "ctrl_msg.h"

#define CODEC_ERROR_USC_GET_INFO_SIZE 0x0001
#define CODEC_ERROR_USC_GET_INFO 0x0002
#define CODEC_ERROR_USC_NUM_ALLOC 0x0003
#define CODEC_ERROR_USC_MEM_ALLOC 0x0004
#define CODEC_ERROR_USC_INIT 0x0005
#define CODEC_ERROR_USC_GET_INFO2 0x0006
#define CODEC_ERROR_USC_ENCODE 0x0007
#define CODEC_ERROR_USC_DECODE 0x0008
#define CODEC_ERROR_USC_GETOUTSTREAMSIZE 0x0009
#define CODEC_ERROR_SET_SCHEDULER 0x0100
#define CODEC_ERROR_NICE 0x0101
#define CODEC_ERROR_NO_SUCH_CODEC 0x0200
#define CODEC_ERROR_TIMESTAMP_FILE_NAME 0x1000
#define CODEC_ERROR_TIMESTAMP_FILE_OPEN 0x1001
#define CODEC_ERROR_READ_FILE 0x1010
#define CODEC_ERROR_WRITE_FILE_OPEN 0x1020
#define CODEC_ERROR_WRITE_FILE_HEADER 0x1021
#define CODEC_ERROR_WRITE_FILE_SEQ_NUM_AND_TIMING 0x1022

#define CODEC_STATE_NOT_STARTED 0x0001
#define CODEC_STATE_STARTED 0x0002
#define CODEC_STATE_ENDED 0x1001
#define CODEC_STATE_REAPABLE 0x1002
#define CODEC_STATE_REAPED 0x1004
#define CODEC_STATE_NO_MORE_PACKETS 0x1000

#define SCHEDULING_POLICY_DEFAULT 0
#define SCHEDULING_POLICY_FIFO 1
#define SCHEDULING_POLICY_BATCH 66
#define SCHEDULING_POLICY_CFS 2
#define SCHEDULING_POLICY_DEADLINE 3
#define SCHEDULING_POLICY_FIFO_PRIORITY_DEFAULT 5

#ifdef BENCHMARK_CODEC

#define BENCHMARK_CODEC_ERROR_FILE_OPEN 0x8001
#define BENCHMARK_CODEC_ERROR_FILE_READ 0x8002
#define BENCHMARK_CODEC_ERROR_FILE_SEEK 0x8003

#endif // BENCHMARK_CODEC

struct seq_num_and_timings
{
    uint16_t seq_num;
    struct timespec kernel_timestamp;
    struct timespec enc_alogrithm_timestamp;
    struct timespec task_done_timestamp;
};

struct codec_struct
{
    size_t state;  // CODEC_STATE_<SOMETHING>
#ifndef BENCHMARK_CODEC
    size_t max_delay;
    size_t weight;
    size_t call_id;
    int timeStamps_fd;
    socket_handle_t socket_handle;
    struct seq_num_and_timings record;
#else
    char *input_file;
    size_t iterations;
    struct timespec time_benchmark_start;
    struct timespec time_benchmark_end;
#endif // BENCHMARK_CODEC
    size_t timing;
    char *codec_name;
    size_t frameLength;
    int numMemBanks;
    USC_Handle encoder;
    USC_Handle decoder;
    USC_PCMStream inStream;
    USC_Bitstream outStream;
    USC_Option parameters;
    USC_Fxns *ippFuncsPtr;
    USC_MemBank *codecMemBanksPtr;
    USC_CodecInfo *ippCodecinfoPtr;
};
