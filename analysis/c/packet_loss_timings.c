
#include "packet_loss_timings.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME_MAX_LEN 256

int packet_loss_timings(char *directory, char *scheduler,
        struct measurement_set **set_list, struct timespec *start,
        struct timespec *end, size_t packet_loss_timing_accuracy_ms)
{
    if (!set_list)
        return -1;

    uint64_t _window_ns = timing_get_diff_ns(end, start);

    uint64_t _timing_accuracy_ns = packet_loss_timing_accuracy_ms * 1000000;

    size_t _bin_amount = _window_ns / _timing_accuracy_ns;
    if (_window_ns % _timing_accuracy_ns)
        _bin_amount++;

    if (!_bin_amount)
    {
        printf("ERROR: packet loss timing bin amount would be 0!"
                "(debug: _window_ns %zu)\n", _window_ns);
        return -1;
    }

    size_t *_bins = malloc(sizeof(size_t) * _bin_amount);
    memset(_bins, 0, sizeof(size_t) * _bin_amount);

    struct measurement_set **_set_curr = set_list;
    while (*_set_curr)
    {
        for (size_t i = 1; i < (*_set_curr)->amount; ++i)
        {
            size_t _loss_amount = ((*_set_curr)->list + i)->seq_num -
                    ((*_set_curr)->list + i - 1)->seq_num - 1;

            if (_loss_amount == 0)
                continue ;

            // idea: guess where the packets would've been if they wouldn't
            // had been lost. increment those bins.

            uint64_t _loss_window_ns = timing_get_diff_ns(
                    &((*_set_curr)->list + i)->timing,
                    &((*_set_curr)->list + i - 1)->timing);

            uint64_t _loss_start = timing_get_diff_ns(
                    &((*_set_curr)->list + i)->timing, start);

            uint64_t _estimate_delay_ns = _loss_window_ns / (_loss_amount + 1);

            for (size_t j = 1; j <= _loss_amount; ++j)
            {
                size_t _bin = (_loss_start + _estimate_delay_ns * j) /
                        _timing_accuracy_ns;

                if (_bin < _bin_amount)
                    _bins[_bin]++;
            }
        }

        _set_curr++;
    }

    char _filename[FILENAME_MAX_LEN];
    snprintf(_filename, FILENAME_MAX_LEN, "%s%s_packet_loss_timings.txt",
            directory, scheduler);

    FILE *_file = fopen(_filename, "w");
    if (!_file)
    {
        printf("ERROR: Could not open %s for writing of packet loss "
                "timings (%d - %s)\n", _filename, errno,
                strerror(errno));
        free(_bins);
        return -1;
    }

    for (size_t i = 0; i < _bin_amount; ++i)
        fprintf(_file, "%zu, %zu\n", i * packet_loss_timing_accuracy_ms,
                _bins[i]);

    fclose(_file);

    free(_bins);

    return 0;
}

