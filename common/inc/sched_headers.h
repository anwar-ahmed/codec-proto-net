
#pragma once

#ifndef BENCHMARK_CODEC
#include <stdint.h>

// note: partly copy-pasted code from the Linux kernel code because no
// GNU Lib C avaible supporting the sched_getattr() syscall
struct sched_attr {
    uint32_t size;

    uint32_t sched_policy;
    uint64_t sched_flags;

    /* SCHED_NORMAL, SCHED_BATCH */
    int32_t sched_nice;

    /* SCHED_FIFO, SCHED_RR */
    uint32_t sched_priority;

    /* SCHED_DEADLINE */
    uint64_t sched_runtime;
    uint64_t sched_deadline;
    uint64_t sched_period;
};

#define SYSCALL_SETATTR 314
#define SYSCALL_GETATTR 315

#define __SCHED_NORMAL 0
#define __SCHED_FIFO 1
#define __SCHED_RR 2
#define __SCHED_BATCH 3
#define __SCHED_DEADLINE 6

#define __SCHED_RESET_ON_FORK     0x40000000
#define __SCHED_FLAG_RESET_ON_FORK    0x01
#define __SCHED_FLAG_SOFT_RSV         0x02
#endif

