
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <ncurses.h>

#include "linked_list.h"
#include "timing.h"

#include "control.h"
#include "ui.h"

#define LOG_MESSAGES_BUFFER_MAX 256
#define LOG_MESSAGES_MAX_LENGTH 80

static struct verbosity_levels verbosity_levels = VERBOSITY_LEVELS_DEFAULT_VALUES;
static int display_thread_id;

// initialization variables
static size_t init_completed = 0;
static pthread_mutex_t init_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t init_cond = PTHREAD_COND_INITIALIZER;

// global (with internal linkage) variables for log output
static struct timespec start_time;
//static char ** log_messages_buffer = NULL;
//static size_t log_messages_ptr;
static linked_list_t log_messages;
static uint16_t log_messages_amount;
static pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;

// global (with internal linkage) variables for codec progression output
struct codec_progress
{
    size_t filesize;
    size_t done;
    char *id;
};
//static struct codec_progress **codec_progresses;
static linked_list_t codec_progresses;
struct codec_progress_printout
{
    char *name;
    size_t amount;
};
static linked_list_t codec_progress_printouts;
static int codec_progress_printout_state_changed;
static pthread_mutex_t codec_progress_mutex = PTHREAD_MUTEX_INITIALIZER;

void feeder_ui_log(int type, int level, char *format, ...)
{
    int *compare_to;
    switch (type)
    {
        case FEEDER_UI_LOG_TYPE_REPORT:
            compare_to = (int *) &verbosity_levels.report;
            break ;

        case FEEDER_UI_LOG_TYPE_INFO:
            compare_to = (int *) &verbosity_levels.info;
            break ;

        case FEEDER_UI_LOG_TYPE_CONTROL:
            compare_to = (int *) &verbosity_levels.control;
            break ;

        case FEEDER_UI_LOG_TYPE_THREAD:
            compare_to = (int *) &verbosity_levels.thread;
            break ;

        case FEEDER_UI_LOG_TYPE_SCTP:
            compare_to = (int *) &verbosity_levels.sctp;
            break ;

        default:
            return ;
    }

    if (level > *compare_to)
    {
        return ;
    }

    pthread_mutex_lock(&log_mutex);

    if (log_messages_amount >= LOG_MESSAGES_BUFFER_MAX)
    {
        char *_old_log;
        if (linked_list_remove(
                log_messages,
                log_messages_amount - LOG_MESSAGES_BUFFER_MAX,
                (void **) &_old_log))
        {
            free(_old_log);
        }
    }

    va_list va_list_first;
    va_list va_list_second;
    va_start(va_list_first, format);
    va_copy(va_list_second, va_list_first);

    struct timespec current_time;
    struct timespec time_since_start;
    clock_gettime(CLOCK_MONOTONIC, &current_time);
    time_difference(&start_time, &current_time, &time_since_start);

    size_t timestamp_len = 1 + 4 + 1 + 3 + 1; // [xxxx:xxx]
    if (display_thread_id)
    {
        timestamp_len += 8 + 1; // thread ID and delimiter
    }
    size_t msg_len_without_timestamp = vsnprintf(NULL, 0, format, va_list_first);
    va_end(va_list_first);
    char *_new_log = (char *)
            malloc(msg_len_without_timestamp + timestamp_len + 2);

    // insert timestamp
    if (display_thread_id)
    {
        snprintf(_new_log,
                timestamp_len + 1,
                "[%.8x|%.4lld.%.3ld]",
                (unsigned int) pthread_self(),
                (long long) time_since_start.tv_sec % 10000,
                time_since_start.tv_nsec / 1000000);
    }
    else
    {
        snprintf(_new_log,
                timestamp_len + 1,
                "[%.4lld.%.3ld]",
                (long long) time_since_start.tv_sec % 10000,
                time_since_start.tv_nsec / 1000000);
    }
    _new_log[timestamp_len] = ' ';

    // insert actual message
    vsnprintf(_new_log + timestamp_len + 1,
            msg_len_without_timestamp + 1, format, va_list_second);

    va_end(va_list_second);

    linked_list_insert(log_messages, log_messages_amount, _new_log);

    ++log_messages_amount;

    pthread_mutex_unlock(&log_mutex);
}

size_t *feeder_ui_codec_new(uint16_t id, size_t filesize, char *call_id)
{
    struct codec_progress *_codec_progress = (struct codec_progress *)
            malloc(sizeof(struct codec_progress));

    pthread_mutex_lock(&codec_progress_mutex);

    linked_list_insert(codec_progresses, id, _codec_progress);

    _codec_progress->filesize = filesize;
    _codec_progress->done = 0;
    _codec_progress->id = call_id;

    linked_list_node_t *_curr_printout_node = NULL;
    static uint16_t _printout_key = 0;
    if (!linked_list_get_first(codec_progress_printouts, &_curr_printout_node))
    {
        while (strcmp(((struct codec_progress_printout *) 
                _curr_printout_node->value)->name, call_id))
        {
            if (linked_list_move_next(&_curr_printout_node))
            {
                _curr_printout_node = NULL;
                break ;
            }
        }
    }
    if (!_curr_printout_node)
    {
        struct codec_progress_printout *_curr_printout_value =
                malloc(sizeof(struct codec_progress_printout));
        _curr_printout_value->name = (char *) malloc(strlen(call_id) + 1);
        strcpy(_curr_printout_value->name, call_id);
        _curr_printout_value->amount = 1;
        linked_list_insert(codec_progress_printouts, _printout_key++,
                _curr_printout_value);
        codec_progress_printout_state_changed = 1;
    }
    else
    {
        ++((struct codec_progress_printout *)
                _curr_printout_node->value)->amount;
    }

    pthread_mutex_unlock(&codec_progress_mutex);

    return &_codec_progress->done;
}

void feeder_ui_codec_remove(uint16_t id)
{
    struct codec_progress *_codec_progress;

    pthread_mutex_lock(&codec_progress_mutex);

    char *_call_id;
    if (linked_list_remove(codec_progresses, id, (void **) &_codec_progress))
    {
        pthread_mutex_unlock(&codec_progress_mutex);
        return ;
    }

    _call_id = _codec_progress->id;
    free(_codec_progress);

    linked_list_node_t *_curr_printout_node = NULL;
    if (!linked_list_get_first(codec_progress_printouts, &_curr_printout_node))
    {
        while (strcmp(((struct codec_progress_printout *) 
                _curr_printout_node->value)->name, _call_id))
        {
            if (linked_list_move_next(&_curr_printout_node))
            {
                _curr_printout_node = NULL;
                break ;
            }
        }
    }
    if (_curr_printout_node)
    {
        size_t *_amount_ptr = &((struct codec_progress_printout *)
                _curr_printout_node->value)->amount;

        --(*_amount_ptr);
        if (!(*_amount_ptr))
        {
            free(((struct codec_progress_printout *)
                    _curr_printout_node->value)->name);
            free(_curr_printout_node->value);
            linked_list_remove(codec_progress_printouts,
                    _curr_printout_node->key, NULL);
            codec_progress_printout_state_changed = 1;
        }
    }

    pthread_mutex_unlock(&codec_progress_mutex);
}


/**
 * Ncurses printout of the log.
 */

static void paint_log(int screen_width, int screen_height)
{
    move(0, 0);
    printw("LOG:");

    struct timespec current_time;
    struct timespec time_since_start;
    clock_gettime(CLOCK_MONOTONIC, &current_time);
    time_difference(&start_time, &current_time, &time_since_start);

    mvprintw(0,
            screen_width - (1 + 4 + 1 + 3 + 1) - 1,
            "[%.4lld.%.3ld]",
            (long long) time_since_start.tv_sec % 10000,
            time_since_start.tv_nsec / 1000000);

    pthread_mutex_lock(&log_mutex);

    size_t _height = screen_height / 2 - 1;
    size_t _x = 1;
    uint16_t _curr_key;

    if (log_messages_amount >= _height)
    {
        _curr_key = log_messages_amount - _height;
    }
    else
    {
        _curr_key = 0;
    }

    char *_curr_char;
    linked_list_node_t *_curr_node = NULL;
    linked_list_get_node(log_messages, _curr_key, &_curr_node, (void **) &_curr_char);

    while (_curr_node && (_x <= _height))
    {
        linked_list_get_and_move_next(&_curr_node, &_curr_key, (void **) &_curr_char);
        mvprintw(_x, 0, _curr_char);
        ++_x;
    }

    pthread_mutex_unlock(&log_mutex);
}


/**
 * Ncurses printout of the codec's progress.
 */
static void paint_codecs(int screen_width, int screen_height)
{
    move(screen_height/2, 0);
    printw("CODECS:");

    pthread_mutex_lock(&codec_progress_mutex);

    size_t _x = screen_height / 2 + 1; // above: log msgs (half screen) and codec topic (1 row)
    linked_list_node_t *_curr;
    if (linked_list_get_first(codec_progresses, &_curr))
    {
        goto _paint_codecs_error;
    }

    if (linked_list_length(codec_progresses) < (screen_height - (int) _x))
    {
        while ((_curr) && (_x < (size_t)screen_height))
        {
            uint16_t _id;
            struct codec_progress *_codec_progress;
            linked_list_get_and_move_next(&_curr, &_id,
                    (void **) &_codec_progress);

            mvprintw(
                    _x,
                    0,
                    "ID: %s, progress %zu %% (bytes: %zu / %zu)\n",
                    _codec_progress->id,
                    (_codec_progress->done * 100) / _codec_progress->filesize,
                    _codec_progress->done,
                    _codec_progress->filesize);
            ++_x;
        }
    }
    else
    {
        static size_t _printout_amount_last = 0;
        static char **_printout_names = NULL;
        static size_t *_printout_bytes_done = NULL;
        static size_t *_printout_bytes_total = NULL;
        static size_t *_printout_calls = NULL;
        size_t _printout_amount = linked_list_length(codec_progress_printouts);
        _printout_amount = _printout_amount < (size_t)(screen_height - (int)_x) ?
                (size_t) _printout_amount : (size_t) (screen_height - (int)_x);

        // We need to update if the amount of codecs has changed or if the
        // screen height has changed. Simply checking if printout amount has
        // changed is not enough since both an add and a remove might have
        // happened.
        if ((codec_progress_printout_state_changed) ||
                _printout_amount != _printout_amount_last)
        {
            for (size_t i = 0; i < _printout_amount_last; ++i)
            {
                free(_printout_names[i]);
            }
            free(_printout_names);
            free(_printout_bytes_done);
            free(_printout_bytes_total);
            free(_printout_calls);

            _printout_amount_last = _printout_amount;
            _printout_names = malloc(sizeof(char *) * _printout_amount);
            linked_list_node_t *_printout_node;
            linked_list_get_first(codec_progress_printouts, &_printout_node);
            struct codec_progress_printout *_printout_struct;
            for (size_t i = 0; i < _printout_amount; ++i)
            {
                linked_list_get_and_move_next(&_printout_node, NULL, (
                        void **) &_printout_struct);
                _printout_names[i] = malloc(strlen(_printout_struct->name) +1);
                strcpy(_printout_names[i], _printout_struct->name);
            }
            _printout_bytes_done = malloc(sizeof(size_t) * _printout_amount);
            _printout_bytes_total = malloc(sizeof(size_t) * _printout_amount);
            _printout_calls = malloc(sizeof(size_t) * _printout_amount);

            codec_progress_printout_state_changed = 0;
        }

        for (size_t i = 0; i < _printout_amount; ++i)
        {
            _printout_bytes_done[i] = 0;
            _printout_bytes_total[i] = 0;
            _printout_calls[i] = 0;
        }

        while (_curr)
        {
            struct codec_progress *_codec_progress;
            linked_list_get_and_move_next(&_curr, NULL, (void **) &_codec_progress);

            for (size_t i = 0; i < _printout_amount; ++i)
            {
                if (!strcmp(_codec_progress->id, _printout_names[i]))
                {
                    _printout_bytes_done[i] += _codec_progress->done;
                    _printout_bytes_total[i] += _codec_progress->filesize;    
                    _printout_calls[i] += 1;
                    break ;
                }
            }
        }

        for (size_t i = 0; i < _printout_amount; ++i)
        {
            mvprintw(
                    _x,
                    0,
                    "ID: %s, calls %zu, progress %zu %% (bytes: %zu / %zu)\n",
                    _printout_names[i],
                    _printout_calls[i],
                    (_printout_bytes_done[i] * 100) / _printout_bytes_total[i],
                    _printout_bytes_done[i],
                    _printout_bytes_total[i]);
            ++_x;
        }
    }

_paint_codecs_error:
    pthread_mutex_unlock(&codec_progress_mutex);

    // paint rest of the lines as empty lines since ncurses doesn't update
    // untouched lines -> the amount of lines might this time be less than
    // previously and that would cause old lines to still be displayed
    while (_x < (size_t)screen_height)
    {
        mvprintw(_x, 0, "\n");
        ++_x;
    }
}

void *feeder_ui_thread(void *arg)
{
    struct feeder_ui_thread_start *start_struct =
            (struct feeder_ui_thread_start *) arg;
    int *_end_ui_painter = start_struct->end;
    verbosity_levels = start_struct->verbosity_levels;
    display_thread_id = start_struct->display_thread_id;

    log_messages = linked_list_create();
    log_messages_amount = 0;

    codec_progress_printout_state_changed = 0;
    codec_progresses = linked_list_create();
    codec_progress_printouts = linked_list_create();

    clock_gettime(CLOCK_MONOTONIC, &start_time);

    initscr();

    cbreak();
    noecho();
    timeout(10);  // 10 ms

    init_completed = 1;
    pthread_cond_signal(&init_cond);

    while (!(*_end_ui_painter))
    {
        // screen width/height might change, update on every loop
        int screen_width;
        int screen_height;
        getmaxyx(stdscr, screen_height, screen_width);

        paint_log(screen_width, screen_height);

        paint_codecs(screen_width, screen_height);

        refresh();

        int ch = getch();

        if (ch == ERR)
        {
            continue ;
        }

        feeder_control_request_key_press(ch);
    }

    init_completed = 0;

    endwin();

    linked_list_destroy(log_messages);

    linked_list_destroy(codec_progresses);
    linked_list_destroy(codec_progress_printouts);

    return NULL;
}

void feeder_ui_wait_init_completion(void)
{
    pthread_mutex_lock(&init_mutex);

    while (!init_completed)
    {
        pthread_cond_wait(&init_cond, &init_mutex);
    }

    pthread_mutex_unlock(&init_mutex);
}
