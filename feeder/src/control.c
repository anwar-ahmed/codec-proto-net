
#include "control.h"
#include "macros.h"

#include <errno.h>
#include <netinet/sctp.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

#include "linked_list.h"

#include "threads.h"
#include "ui.h"

#define UDP_BUFFER_SIZE 1024

size_t feeder_threads_stop;
static size_t feeder_control_reading_thread_stop;
static size_t feeder_control_reading_thread_reapable;

enum CONTROL_REQUEST_TYPE
{
    CONTROL_REQUEST_TYPE_THREAD_START,
    CONTROL_REQUEST_TYPE_THREAD_END,
    CONTROL_REQUEST_TYPE_CTRL_MSG,
    CONTROL_REQUEST_TYPE_KEY_PRESS,
    CONTROL_REQUEST_TYPE_PROGRAM_NOTIFICATION
};
struct control_request
{
    enum CONTROL_REQUEST_TYPE type;
    union
    {
        struct codec_config *thread_start;
        int thread_end;
        struct
        {
            enum CTRL_MSG_TYPE ctrl_msg_type;
            uint16_t stream_id;
            char *data;
        } ctrl_msg;
        int key_press;
        enum FEEDER_CONTROL_PROGRAM_NOTIFICATION notification;
    } request;
};
static linked_list_t control_requests;
static pthread_mutex_t control_requests_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t control_requests_cond = PTHREAD_COND_INITIALIZER;

// codec_configs that have been requested to start, but are waiting for acceptance
// from the codec_runner
static linked_list_t startable_codec_configs;

// Possible states:
// INITIALIZING: Starting up required threads. Not yet ready to accept any requests.
// RUNNING: Running, accepting requests and serving one request at a time ASAP.
// STOPPING: Tearing down, no requests are anymore accepted or processed.
// FINISHED: All control logic has finished. Waiting to terminate.
enum FEEDER_CONTROL_STATE
{
    FEEDER_CONTROL_STATE_INITIALIZING,
    FEEDER_CONTROL_STATE_RUNNING,
    FEEDER_CONTROL_STATE_STOPPING,
    FEEDER_CONTROL_STATE_FINISHED
};
static enum FEEDER_CONTROL_STATE feeder_control_state;


void *control_reading_thread(void *arg)
{
    socket_handle_t socket_handle = *(socket_handle_t *) arg;

    feeder_ui_log(3, 1, "Control reading thread started!\n");

    feeder_control_reading_thread_reapable = 0;
    while (!(feeder_control_reading_thread_stop))
    {
        enum CTRL_MSG_TYPE ctrl_msg_type;
        uint16_t stream_id;
        char *data_ptr;
        uint16_t data_len;
        int the_error;
        if ((the_error = ctrl_msg_recv(socket_handle, &ctrl_msg_type,
                &stream_id, &data_ptr, &data_len)))
        {
            if (the_error == -1)
            {
                if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
                {
                    continue ;
                }
                else if (errno == ECONNRESET)
                {
                    feeder_control_request_program_notification(
                            FEEDER_CONTROL_PROGRAM_NOTIFICATION_SCTP_ERROR_CONNECTION_RESET);
                    break ;
                }
                else
                {
                    // unknown error
                    feeder_ui_log(0, 3, "WARNING: Receiving a packet from the "
                            "server failed (errno: %d - %s).\n",
                            errno,
                            strerror(errno));
                    continue ;
                }
            }

            feeder_ui_log(0, 3, "WARNING: Receiving a packet from the "
                    "server failed with code %d.\n", the_error);
            continue ;
        }

        feeder_control_request_ctrl_msg(ctrl_msg_type, stream_id, data_ptr);
    }

    feeder_ui_log(3, 1, "Control reading thread ended.\n");
    exit(0);
    feeder_control_reading_thread_reapable = 1;

    return NULL;
}

static int control_request_handler_thread_start(socket_handle_t socket_handle,
        struct codec_config *codec_config)
{
    int id;

    if (feeder_thread_thread_create_request(socket_handle, codec_config, &id))
    {
        feeder_ui_log(0, 1, "FATAL ERROR: feeder_thread_thread_create_request failed!\n");
        return -1;
    }

    linked_list_insert(startable_codec_configs, id, codec_config);

    return 0;
}

static int control_request_handler_thread_end(socket_handle_t socket_handle, int id)
{
    return ctrl_msg_send_codec_end(socket_handle, id);
}

static int control_request_handler_ctrl_msg(socket_handle_t socket_handle,
        enum CTRL_MSG_TYPE ctrl_msg_type, uint16_t stream_id, char *data)
{
    switch (ctrl_msg_type)
    {
        case CTRL_MSG_TYPE_CODEC_START_ACC:
        {
            struct codec_config *codec_config;
            if (linked_list_remove(startable_codec_configs, stream_id,
                    (void **) &codec_config))
            {
                feeder_ui_log(0, 1, "FATAL ERROR: Startable codec not found"
                        "(id: %d)!\n", stream_id);
                return 0;
            }
            return feeder_thread_thread_create_create(socket_handle, stream_id,
                    codec_config);
        }

        case CTRL_MSG_TYPE_CODEC_START_DEC:
        {
            linked_list_remove(startable_codec_configs, stream_id, NULL);
            feeder_ui_log(0, 1, "FATAL ERROR: Server refused to accept new codec.\n");
            return 0;
        }

        case CTRL_MSG_TYPE_CODEC_ERROR:
        {
            uint16_t error_code = *((uint16_t *) data);
            feeder_ui_log(0, 1, "FATAL ERROR: Server thread %d exited "
                    "with the code 0x%.4x\n", stream_id, error_code);
            free(data);
            return -1;
        }

        default:
            return -1;
    }

    return 0;
}

static int control_request_handler_key_press(int ch)
{
    if ((ch == 'q') || (ch == 'Q'))
    {
        return -1;
    }

    return 0;
}

static int control_request_handler_program_notification(
        enum FEEDER_CONTROL_PROGRAM_NOTIFICATION notification)
{
    switch (notification)
    {
        case FEEDER_CONTROL_PROGRAM_NOTIFICATION_SCTP_ERROR_DISCONNECT:
            feeder_ui_log(0, 1, "FATAL ERROR: Connection to server was lost!\n");
            feeder_ui_log(1, 1, "Campaign will now end.\n");
            return -1;

        case FEEDER_CONTROL_PROGRAM_NOTIFICATION_SCTP_ERROR_CONNECTION_RESET:
            feeder_ui_log(0, 1, "FATAL ERROR: Connection to server was reset!\n");
            feeder_ui_log(1, 1, "Campaign will now end.\n");
            return -1;

        case FEEDER_CONTROL_PROGRAM_NOTIFICATION_CAMPAIGN_ENDED:
            feeder_ui_log(1, 1, "All codecs processed, campaign will now end.\n");
            return -1;

        default:
            return 0;
    }
}


void feeder_control(socket_handle_t socket_handle,
        struct codec_config **codec_configs)
{

    control_requests = linked_list_create();
    startable_codec_configs = linked_list_create();

    feeder_ui_log(1, 3, "Feeder control state is INITIALIZING.\n");
    feeder_control_state = FEEDER_CONTROL_STATE_INITIALIZING;


    pthread_t sctp_thread;
    feeder_control_reading_thread_stop = 0;
    pthread_create(&sctp_thread, NULL, control_reading_thread, &socket_handle);


    pthread_t control_thread;
    feeder_thread_control_thread_create(&control_thread, codec_configs);


    feeder_threads_stop = 0;
    int _stop = 0;
    pthread_mutex_lock(&control_requests_mutex);
    while (1)
    {
        uint16_t key;
        struct control_request *control_request;
        while (linked_list_pop(control_requests, &key, (void **) &control_request))
        {
            if (pthread_cond_wait(&control_requests_cond, &control_requests_mutex))
            {
                // TODO: handle error
            }
        }
        if (control_request->type == CONTROL_REQUEST_TYPE_THREAD_START)
        {
            feeder_ui_log(2, 1, "DEBUG CONTROL: control request THREAD "
                    "START\n");
            if (!_stop)
            {
                _stop = control_request_handler_thread_start(
                        socket_handle,
                        control_request->request.thread_start);
            }
        }
        else if (control_request->type == CONTROL_REQUEST_TYPE_THREAD_END)
        {
            feeder_ui_log(2, 1, "DEBUG CONTROL: control request THREAD "
                    "END\n");
            if (!_stop)
            {
                _stop = control_request_handler_thread_end(
                        socket_handle,
                        control_request->request.thread_end);
            }
        }
        else if (control_request->type == CONTROL_REQUEST_TYPE_CTRL_MSG)
        {
            feeder_ui_log(2, 1, "DEBUG CONTROL: control request CTRL MSG\n");
            _stop = _stop | control_request_handler_ctrl_msg(
                    socket_handle,
                    control_request->request.ctrl_msg.ctrl_msg_type,
                    control_request->request.ctrl_msg.stream_id,
                    control_request->request.ctrl_msg.data);
        }
        else if (control_request->type == CONTROL_REQUEST_TYPE_KEY_PRESS)
        {
            feeder_ui_log(2, 1, "DEBUG CONTROL: control request KEY PRESS\n");
            if (!_stop)
            {
                _stop = control_request_handler_key_press(
                        control_request->request.key_press);
            }
            else if (control_request_handler_key_press(
                    control_request->request.key_press))
            {
                if (feeder_control_reading_thread_reapable)
                {
                    break ;
                }
                else
                {
                    feeder_ui_log(1, 2, "ERROR: Control thread is still "
                            "running\n");
                }
            }
        }
        else if (control_request->type == CONTROL_REQUEST_TYPE_PROGRAM_NOTIFICATION)
        {
            feeder_ui_log(2, 1, "DEBUG CONTROL: control request PROGRAM NOTIFICATION\n");
            if (!_stop)
            {
                _stop = control_request_handler_program_notification(
                        control_request->request.notification);
            }
        }
        free(control_request);

        if (_stop && (feeder_control_state != FEEDER_CONTROL_STATE_STOPPING))
        {
            feeder_ui_log(1, 3, "Feeder control state is STOPPING.\n");
            feeder_control_state = FEEDER_CONTROL_STATE_STOPPING;

            feeder_threads_stop = 1;
            feeder_control_reading_thread_stop = 1;
        }
    }
    pthread_mutex_unlock(&control_requests_mutex);


    pthread_join(sctp_thread, NULL);

    pthread_join(control_thread, NULL);

    feeder_ui_log(2, 1, "Feeder control state is FINISHED.\n");
    feeder_control_state = FEEDER_CONTROL_STATE_FINISHED;
}

void feeder_control_request_thread_start(struct codec_config *codec_config)
{
    struct control_request *new_request = (struct control_request *)
            malloc(sizeof(struct control_request));

    new_request->type = CONTROL_REQUEST_TYPE_THREAD_START;
    new_request->request.thread_start = codec_config;

    pthread_mutex_lock(&control_requests_mutex);
    linked_list_insert(control_requests, 0, new_request);
    pthread_cond_signal(&control_requests_cond);
    pthread_mutex_unlock(&control_requests_mutex);
}

void feeder_control_request_thread_end(int id)
{
    struct control_request *new_request = (struct control_request *)
            malloc(sizeof(struct control_request));

    new_request->type = CONTROL_REQUEST_TYPE_THREAD_END;
    new_request->request.thread_end = id;

    pthread_mutex_lock(&control_requests_mutex);
    linked_list_insert(control_requests, 0, new_request);
    pthread_cond_signal(&control_requests_cond);
    pthread_mutex_unlock(&control_requests_mutex);
}

void feeder_control_request_ctrl_msg(enum CTRL_MSG_TYPE ctrl_msg_type,
        uint16_t stream_id, char *data)
{
    struct control_request *new_request = (struct control_request *)
            malloc(sizeof(struct control_request));

    new_request->type = CONTROL_REQUEST_TYPE_CTRL_MSG;
    new_request->request.ctrl_msg.ctrl_msg_type = ctrl_msg_type;
    new_request->request.ctrl_msg.stream_id = stream_id;
    new_request->request.ctrl_msg.data = data;

    pthread_mutex_lock(&control_requests_mutex);
    linked_list_insert(control_requests, 0, new_request);
    pthread_cond_signal(&control_requests_cond);
    pthread_mutex_unlock(&control_requests_mutex);
}

void feeder_control_request_key_press(int ch)
{
    struct control_request *new_request = (struct control_request *)
            malloc(sizeof(struct control_request));

    new_request->type = CONTROL_REQUEST_TYPE_KEY_PRESS;
    new_request->request.key_press = ch;

    pthread_mutex_lock(&control_requests_mutex);
    linked_list_insert(control_requests, 0, new_request);
    pthread_cond_signal(&control_requests_cond);
    pthread_mutex_unlock(&control_requests_mutex);
}

void feeder_control_request_program_notification(
        enum FEEDER_CONTROL_PROGRAM_NOTIFICATION notification)
{
    struct control_request *new_request = (struct control_request *)
            malloc(sizeof(struct control_request));

    new_request->type = CONTROL_REQUEST_TYPE_PROGRAM_NOTIFICATION;
    new_request->request.notification = notification;

    pthread_mutex_lock(&control_requests_mutex);
    linked_list_insert(control_requests, 0, new_request);
    pthread_cond_signal(&control_requests_cond);
    pthread_mutex_unlock(&control_requests_mutex);
}
