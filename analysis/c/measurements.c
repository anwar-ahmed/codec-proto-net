
#define _GNU_SOURCE

#include "measurements.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#define TIMESNS "%" PRIu64 ".%.9" PRIu64 " "

// version 2:
//  long        version tag
//  char[20]    codec name
//  size_t      timing (ms)
#define TIMING_FILE_VERSION_TAG 4L
#define TIMING_FILE_HEADER_SIZE (sizeof(long) + CODEC_NAME_MAX_LENGTH + \
        sizeof(size_t))

#define FILENAME_MAX_LEN 256

struct measurement_set_node
{
    struct measurement_set *set;
    struct measurement_set_node *next;
};

void free_measurement_set(struct measurement_set *set)
{
    free(set->codec_name);
    free(set->directory);
    free(set->filename);
    free(set->list);
    free(set);
}
void free_measurement_set_list(struct measurement_set **set_list)
{
    struct measurement_set **_curr = set_list;
    while (*_curr)
    {
        free_measurement_set(*_curr);
        _curr++;
    }
    free(set_list);
}

void move_timing_s(struct timespec *timing, int s)
{
    timing->tv_sec += s;
}

void move_timing_ms(struct timespec *timing, int ms)
{
    int64_t _tv_nsec = timing->tv_nsec + ms * 1000000;

    if (_tv_nsec > 1000000000)
    {
        timing->tv_nsec = _tv_nsec - 1000000000;
        timing->tv_sec++;
    }
    else if (_tv_nsec < 0)
    {
        timing->tv_nsec = _tv_nsec + 1000000000;
        timing->tv_sec--;
    }
    else
        timing->tv_nsec = _tv_nsec;
}

// return 1  if a > b
// return 0  if a == b
// return -1 if a < b
int timing_compare(struct timespec *timing_a, struct timespec *timing_b)
{
    if (timing_a->tv_sec > timing_b->tv_sec)
        return 1;
    else if (timing_a->tv_sec < timing_b->tv_sec)
        return -1;

    if (timing_a->tv_nsec > timing_b->tv_nsec)
        return 1;
    else if (timing_a->tv_nsec < timing_b->tv_nsec)
        return -1;

    return 0;
}

uint64_t timing_get_diff_ns(struct timespec *later, struct timespec *earlier)
{
    uint64_t _result = later->tv_sec - earlier->tv_sec;
    _result *= 1000000000;

    _result += later->tv_nsec - earlier->tv_nsec;

    return _result;
}

static int invalid_filename(char *filename)
{
    char *_curr = filename;
    while (*_curr)
    {
        if (*_curr == '.')
            return -1;

        _curr++;
    }

    return 0;
}

int write_stats_to_file(char *directory, char **schedulers, struct stats *stats,
        size_t scheduler_amount)
{
    char _filename[FILENAME_MAX_LEN];
    snprintf(_filename, FILENAME_MAX_LEN, "%sstats.txt", directory);

    FILE *_file = fopen(_filename, "w");
    if (!_file)
    {
        printf("ERROR: Could not open %s for writing of stats "
                "(%d - %s)\n", _filename, errno, strerror(errno));
        return -1;
    }

    fprintf(_file, "--- START CODEC AMOUNTS ---\n");
    struct call_amount *_curr_call_amount = stats->call_amount_first;
    while (_curr_call_amount)
    {
        fprintf(_file, "%s: %zu\n", _curr_call_amount->codec_name,
                _curr_call_amount->call_amount);
        _curr_call_amount = _curr_call_amount->next;
    }
    fprintf(_file, "--- END CODEC AMOUNTS ---\n");

    for (size_t i = 0; i < scheduler_amount; ++i)
    {
        fprintf(_file, "--- START SCHEDULER %s STATS ---\n", schedulers[i]);
        fprintf(_file, "Packets lost total: %zu\n",
                stats->scheduler_stats[i].packets_lost_total);
        fprintf(_file, "Packets lost per call max: %zu\n",
                stats->scheduler_stats[i].packets_lost_per_call_max);
        fprintf(_file, "Consecutive packets lost max: %zu\n",
                stats->scheduler_stats[i].consecutive_packets_lost_max);
        fprintf(_file, "--- END SCHEDULER %s STATS ---\n", schedulers[i]);
    }

    fclose(_file);

    return 0;
}

int find_out_call_amounts(struct measurement_set **set_list,
        struct call_amount **first_ptr)
{
    struct call_amount *_call_amount_first = NULL;
    struct measurement_set **_curr = set_list;
    while (*_curr)
    {
        struct call_amount *_call_amount_ptr = _call_amount_first;
        while (_call_amount_ptr &&
                strcmp((*_curr)->codec_name, _call_amount_ptr->codec_name))
            _call_amount_ptr = _call_amount_ptr->next;

        if (!_call_amount_ptr)
        {
            if (!_call_amount_first)
            {
                _call_amount_first = malloc(sizeof(struct call_amount));
                _call_amount_ptr = _call_amount_first;
            }
            else
            {
                _call_amount_ptr = _call_amount_first;
                while (_call_amount_ptr->next)
                    _call_amount_ptr = _call_amount_ptr->next;
                _call_amount_ptr->next = malloc(sizeof(struct call_amount));
                _call_amount_ptr = _call_amount_ptr->next;
            }

            strncpy(_call_amount_ptr->codec_name, (*_curr)->codec_name,
                    strlen((*_curr)->codec_name) + 1);
            _call_amount_ptr->call_amount = 0;
            _call_amount_ptr->next = NULL;
        }

        _call_amount_ptr->call_amount++;

        _curr++;
    }

    *first_ptr = _call_amount_first;

    return 0;
}

static int __parse_invalid_measurements(struct measurement_set *set,
        struct timespec *earliest, struct timespec *latest)
{
    if (!set || !set->list || !earliest || !latest)
        return -1;

    if (timing_compare(&set->list[set->amount-1].timing, earliest) == -1)
    {
        printf("ERROR: earliest time invalid (%lld.%.9ld, %lld.%.9ld)\n",
                (long long) set->list[set->amount-1].timing.tv_sec,
                set->list[set->amount-1].timing.tv_nsec,
                (long long) earliest->tv_sec,
                earliest->tv_nsec);
        return -1;
    }
    if (timing_compare(&set->list[0].timing, latest) == 1)
    {
        printf("ERROR: latest time invalid (%lld.%.9ld, %lld.%.9ld)\n",
                (long long) set->list[0].timing.tv_sec,
                set->list[0].timing.tv_nsec,
                (long long) latest->tv_sec,
                latest->tv_nsec);
        return -1;
    }

    // amount := amount - <strip beginning> - <strip end>
    // <strip beginning> := first - last
    // <strip end> := amount - (last - list)
    // -> amount := amount - (first - list) - (amount - (last - list))
    size_t _amount = set->amount;

    // find out how many to strip from the beginning
    struct measurement *_first = set->list;
    while (1)
    {
        if (timing_compare(&_first->timing, earliest) > -1)
            break;

        _first++;
    }
    _amount -= _first - set->list;

    // find out how many to strip from the end
    struct measurement *_last = set->list + set->amount - 1;
    while (1)
    {
        if (timing_compare(&_last->timing, latest) < 1)
            break;

        _last--;
    }
    _amount -= (set->amount - (_last - set->list));

    struct measurement *_new_list = malloc(
            sizeof(struct measurement) * _amount);
    if (!_new_list)
    {
        printf("ERROR: memory allocation for _dest->list failed, "
                "tried to allocate %zu bytes\n",
                sizeof(struct measurement) * _amount);
        return -1;
    }

    memcpy(_new_list, _first, sizeof(struct measurement) * _amount);

    free(set->list);
    set->list = _new_list;
    set->amount = _amount;

    return 0;
}
int parse_invalid_measurements(struct measurement_set **set_list,
        struct timespec *earliest, struct timespec *latest)
{
    struct measurement_set **_curr = set_list;
    while (*_curr)
    {
        if (__parse_invalid_measurements(*_curr, earliest, latest))
            return -1;

        _curr++;
    }

    return 0;
}

static int read_timing_file_header(int fd, char **codec_name, size_t *timing)
{
    long _version_tag;
    size_t _timing;

    ssize_t bytes_read = read(fd, &_version_tag, sizeof(long));
    if (bytes_read < (ssize_t)sizeof(long) || _version_tag != TIMING_FILE_VERSION_TAG)
        return -1;

    char _codec_name[CODEC_NAME_MAX_LENGTH];
    bytes_read = read(fd, _codec_name, CODEC_NAME_MAX_LENGTH);
    _codec_name[CODEC_NAME_MAX_LENGTH-1] = '\0';
    if (bytes_read < CODEC_NAME_MAX_LENGTH)
        return -1;

    bytes_read = read(fd, &_timing, sizeof(size_t));
    if (bytes_read < (ssize_t)sizeof(size_t))
        return -1;

    size_t _codec_name_strlen = strlen(_codec_name) + 1;
    *codec_name = malloc(_codec_name_strlen);
    strncpy(*codec_name, _codec_name, _codec_name_strlen);
    *timing = _timing;

    return 0;
}

static int load_file(char *directory, char *file, struct measurement_set **set, int *save_raw_data)
{
    if (!set)
        return -1;

    char _filename[FILENAME_MAX_LEN];
    if (directory[strlen(directory)-1] == '/')
        snprintf(_filename, FILENAME_MAX_LEN, "%s%s", directory, file);
    else
        snprintf(_filename, FILENAME_MAX_LEN, "%s/%s", directory, file);

    int fd = open(_filename, O_RDONLY);
    if (fd == -1)
    {
        printf("ERROR: couldn't open file %s\n", _filename);
        return -1;
    }

    struct measurement_set *_set = malloc(sizeof(struct measurement_set));

    if (read_timing_file_header(fd, &_set->codec_name, &_set->timing)) {
        printf("ERROR: reading timing file header failed\n");
        return -1;
    }

    struct stat fd_stat;
    if (fstat(fd, &fd_stat)) {
        printf("ERROR: fstat() failed\n");
        free(_set);
        return -1;
    }

    asprintf(&_set->filename, "%s", file);
    asprintf(&_set->directory, "%s", directory);

    _set->amount = (fd_stat.st_size - TIMING_FILE_HEADER_SIZE)
            / sizeof(struct measurement);
    _set->list = malloc(sizeof(struct measurement) * _set->amount);
    if (!_set->list) {
        printf("ERROR: malloc() failed allocating %zu bytes\n",
                sizeof(struct measurement) * _set->amount);
        free(_set);
        return -1;
    }

    // SAVE FILE INFORMATION FOR RAW DATA SAVING
    char _raw_filename[FILENAME_MAX_LEN];
    if (directory[strlen(directory) - 1] == '/')
        snprintf(_raw_filename, FILENAME_MAX_LEN, "%s_rawdata_%s",
                directory, file);
    else {

        char *raw_directory;
        asprintf(&raw_directory, "%s_rawdata", directory);

        struct stat st = { 0 };

        if (stat(raw_directory, &st) == -1) {
            printf("Creating folder %s for raw files. \n", raw_directory);
            mkdir(raw_directory, 0700);
        }

        snprintf(_raw_filename, FILENAME_MAX_LEN, "%s/%s.raw",
                raw_directory, file);
    }

    FILE *fd_out = fopen(_raw_filename, "w");
    if (!fd_out) {
        printf("ERROR: Could not open %s for writing of raw data "
                "(%d - %s)\n", _filename, errno, strerror(errno));
        return -1;
    }


    uint16_t _prev_seq_num = 0;
    for (size_t i = 0; i < _set->amount; ++i)
    {
        ssize_t bytes_read = read(fd, _set->list + i,
                sizeof(struct measurement));

        if (save_raw_data){
            fprintf(fd_out, "%" PRIu16 " " TIMESNS " " TIMESNS " " TIMESNS "\n"
                    , (uint16_t)(_set->list+i)->seq_num
                    , (uint64_t)(_set->list+i)->udp_rx_hw_timestamp.tv_sec
                    , (uint64_t)(_set->list+i)->udp_rx_hw_timestamp.tv_nsec
                    , (uint64_t)(_set->list+i)->timing.tv_sec
                    , (uint64_t)(_set->list+i)->timing.tv_nsec
                    , (uint64_t)(_set->list+i)->timing_end.tv_sec
                    , (uint64_t)(_set->list+i)->timing_end.tv_nsec);
        }

        if (bytes_read < (ssize_t)sizeof(struct measurement))
        {
            printf("ERROR: read() returned %zi\n", bytes_read);
            free(_set->list);
            free(_set);
            return -1;
        }

        if (_set->list[i].seq_num < _prev_seq_num)
        {
            printf("ERROR: seq_num going backwards, seq_num %u and %u\n",
                    _prev_seq_num,
                    _set->list[i].seq_num);
            free(_set->list);
            free(_set);
            return -1;
        }

        _prev_seq_num = _set->list[i].seq_num;
    }

    fclose(fd_out);


    close(fd);

    *set = _set;

    return 0;
}

int load_directory(char *directory, char *scheduler,
        struct measurement_set ***set_list, struct timespec *latest_start,
        struct timespec *earliest_end, int *save_raw_data)
{
    if (!directory || !scheduler || !set_list || !latest_start ||
            !earliest_end)
        return -1;

    DIR *dirp;
    struct dirent *dp;

    char *_full_path = malloc(strlen(directory) + strlen(scheduler) + 1);
    strncpy(_full_path, directory, strlen(directory));
    strncpy(_full_path + strlen(directory), scheduler, strlen(scheduler) + 1);
    dirp = opendir(_full_path);
    if (!dirp)
    {
        printf("ERROR: opendir() failed on %s\n", _full_path);
        return -1;
    }

    struct measurement_set_node _first;
    struct measurement_set_node *_prev;
    _prev = &_first;
    _first.next = NULL;
    size_t _amount = 0;
    while (1)
    {
        errno = 0;
        dp = readdir(dirp);
        if (!dp)
            break ;

        if (invalid_filename(dp->d_name))
            continue ;

        _prev->next = malloc(sizeof(struct measurement_set_node));
        _prev = _prev->next;
        _prev->next = NULL;

        if (load_file(_full_path, dp->d_name, &_prev->set, save_raw_data))
        {
            _prev = _first.next;
            while (_prev)
            {
                free_measurement_set(_prev->set);
                _prev = _prev->next;
            }
            return -1;
        }
        _amount++;
    }

    free(_full_path);

    if (errno)
    {
        printf("ERROR: readdir() failed with %d - %s\n", errno, strerror(errno));
        _prev = _first.next;
        while (_prev)
        {
            free_measurement_set(_prev->set);
            _prev = _prev->next;
        }
        return -1;
    }
    closedir(dirp);

    // change from linked list to array and find out earliest / latest
    struct measurement_set **_set_list = malloc(
            sizeof(struct measurement_set *) * (_amount + 1));
    _prev = _first.next;
    struct timespec _latest_start = {.tv_sec = 0, .tv_nsec = 0};
    struct timespec _earliest_end =
            {
            .tv_sec = _prev->set->list[_prev->set->amount-1].timing.tv_sec,
            .tv_nsec = LONG_MAX
            };
    for (size_t i = 0; i < _amount; ++i)
    {
        if (!_prev)
        {
            printf("ERROR: weird bug; _prev is null :-( go fix the code!\n");
            return -1;
        }

        if (timing_compare(&_prev->set->list[0].timing, &_latest_start) == 1)
            _latest_start = _prev->set->list[0].timing;
        if (timing_compare(&_prev->set->list[_prev->set->amount-1].timing,
                &_earliest_end) == -1)
            _earliest_end = _prev->set->list[_prev->set->amount-1].timing;

        _set_list[i] = _prev->set;
        _prev = _prev->next;
    }
    _set_list[_amount] = NULL;

    *set_list = _set_list;
    *latest_start = _latest_start;
    *earliest_end = _earliest_end;

    return 0;
}
