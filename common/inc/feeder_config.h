
#pragma once

#include <pthread.h>

#include "usc.h"

#define CONFIG_ERROR_JSON_FILE_READ_FAILED 0x0001
#define CONFIG_ERROR_JSON_TYPE_ROOT 0x0010
#define CONFIG_ERROR_JSON_TYPE_START 0x0011
#define CONFIG_ERROR_JSON_TYPE_PARAMETERS 0x0012
#define CONFIG_ERROR_JSON_TYPE_PCMTYPE 0x0013
#define CONFIG_ERROR_JSON_TYPE_MODES 0x0014
#define CONFIG_ERROR_JSON_TYPE_EXPECT_STRING 0x0015
#define CONFIG_ERROR_JSON_TYPE_EXPECT_INTEGER 0x0016
#define CONFIG_ERROR_DIRECTION_VALUE 0x1000
#define CONFIG_ERROR_OUTMODE_VALUE 0x1001
#define CONFIG_ERROR_LAW_VALUE 0x1002

struct codec_config
{
    char *id;
    char *codec_name;
    char *in_filename;
    char *out_filename;
    char *campaign_filename;
    struct timespec start_time;
    size_t framesize;
    size_t timing;
    size_t weight;
    size_t buffer_size;
    size_t max_delay;
    USC_Option parameters;
};

int load_config_file(
        const char *filename,
        struct codec_config ***codec_config_list);
