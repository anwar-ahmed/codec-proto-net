/*
 * save_raw_data.h
 *
 *  Created on: Aug 24, 2015
 *      Author: eheihol
 */

#ifndef ANALYSIS_C_SAVE_RAW_DATA_H_
#define ANALYSIS_C_SAVE_RAW_DATA_H_

#include "measurements.h"

int save_raw_data(char *directory, char *scheduler, struct measurement_set **set_list);

#endif
