
#pragma once

#include "measurements.h"

int jitter_histogram(char *directory, char *scheduler,
        struct measurement_set **set_list, size_t jitter_min_ms,
        size_t jitter_max_ms, size_t bin_amount);

