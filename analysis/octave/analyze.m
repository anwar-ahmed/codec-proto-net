function [jitter_bins, jitter_outliers, packet_loss_bins, packet_loss_biggest, packet_loss_timing_bins] = analyze(directory, safe_interval, jitter_bin_amount, jitter_min_delay, jitter_max_delay, packet_loss_bin_amount, packet_loss_timing_accuracy)
    dir_name = strcat('FIXME', directory);
    dir_name = strcat(dir_name, '/');
    dir_contents = dir(dir_name);

    printf("Loading measurements\n");

    curr_call = 0;
    for i = 1:length(dir_contents)
        if (index(dir_contents(i).name, ".") == 1)
            continue;
        endif

        file_contents = load(strcat(dir_name, dir_contents(i).name));

        curr_call += 1;
        measurements(curr_call).timings = file_contents(:,2);
        measurements(curr_call).seq_nums = file_contents(:,1);
    endfor

    printf("Reading timings\n");

    [earliest, latest] = get_time_all_calls(measurements);

    if (earliest >= latest)
        error("Earliest time >= latest time");
    endif

    printf("Stripping invalid measurements out\n");

    measurements_valid = strip_measurements(measurements, earliest + safe_interval, latest - safe_interval);

    printf("Building up jitter histogram\n");

    [jitter_bins, jitter_outliers] = timings_to_histogram(measurements_valid, jitter_bin_amount, jitter_min_delay, jitter_max_delay);

    printf("Building up packet loss histogram\n");

    [packet_loss_bins, packet_loss_biggest] = packet_loss_histogram( measurements_valid, packet_loss_bin_amount);

    printf("Calculating packet loss timings\n");

    [packet_loss_timing_bins] = packet_loss_timings(measurements_valid, round((latest-earliest)/packet_loss_timing_accuracy), earliest, latest);
endfunction
