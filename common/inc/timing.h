
#pragma once

#include <time.h>

/**
 *  Count the time difference between 'start' and 'end'.
 *
 *  The time difference of 'start' and 'end' is put into 'difference'.
 *
 *  This function always succeeds, e.g. it doesn't perform any validation such as if 'start'
 *  indeed is before 'end' nor does it check for overflow.
 */
void time_difference(
        struct timespec *start,
        struct timespec *end,
        struct timespec *difference);
