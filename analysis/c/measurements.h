
#pragma once

#include <stdint.h>
#include <time.h>

#define CODEC_NAME_MAX_LENGTH 20

struct scheduler_stats
{
    size_t packets_lost_total;
    size_t packets_lost_per_call_max;
    size_t consecutive_packets_lost_max;
};

struct call_amount
{
    char codec_name[CODEC_NAME_MAX_LENGTH];
    size_t call_amount;
    struct call_amount *next;    
};

struct stats
{
    struct call_amount *call_amount_first;
    struct scheduler_stats *scheduler_stats;
};

struct measurement
{
    uint16_t seq_num;
    struct timespec udp_rx_hw_timestamp;
    struct timespec timing;
    struct timespec timing_end;
};

struct measurement_set
{
    char *codec_name;
    char *directory;
    char *filename;
    size_t timing;
    size_t amount;
    struct measurement *list;
};

void free_measurement_set(struct measurement_set *set);
void free_measurement_set_list(struct measurement_set **set_list);

void move_timing_s(struct timespec *timing, int s);
void move_timing_ms(struct timespec *timing, int ms);

// return 1  if a > b
// return 0  if a == b
// return -1 if a < b
int timing_compare(struct timespec *timing_a, struct timespec *timing_b);

// NOTE: this overflows in >400 years
uint64_t timing_get_diff_ns(struct timespec *later, struct timespec *earlier);

int write_stats_to_file(char *directory, char **schedulers, struct stats *stats,
        size_t scheduler_amount);

int find_out_call_amounts(struct measurement_set **set_list,
        struct call_amount **first_ptr);

int parse_invalid_measurements(struct measurement_set **set_list,
        struct timespec *earliest, struct timespec *latest);

int load_directory(char *directory, char *scheduler,
        struct measurement_set ***set_list, struct timespec *latest_start,
        struct timespec *earliest_end, int *save_raw_data);

