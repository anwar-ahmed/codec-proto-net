# Codec Proto Net

## Installation

Tested to work on Ubuntu 14.04.

* Install required packages:

	sudo apt-get install cmake libjson-c-dev libsctp-dev libncurses5-dev

* Download and install Litmus RT

	mkdir litmus
	cd litmus
	wget http://www.kernel.org/pub/linux/kernel/v3.x/linux-3.10.41.tar.gz
	tar xvzf linux-3.10.41.tar.gz
	wget http://www.litmus-rt.org/releases/2014.2/litmus-rt-2014.2.patch
	mv linux-3.10.41 litmus-rt
	cd litmus-rt
	patch -p1 < ../litmus-rt-2014.2.patch
	make menuconfig
	make -j $(nproc) bzImage
	make -j $(nproc) modules
	cd ..

* Download and install Litmus userspace library (current directory should be: path/to/codec-proto-net/litmus)

	wget http://www.litmus-rt.org/releases/2014.2/liblitmus-2014.2.tgz
	tar xvzf liblitmus-2014.2.tgz
	cd liblitmus
	wget http://www.litmus-rt.org/releases/2014.2/liblitmus-config
	mv liblitmus-config .config
	make -j $(nproc)
	cd ../../

* Download sample code (current directory should be: path/to/codec-proto-net/ )

	mkdir ipp
	cd ipp
	wget http://registrationcenter.intel.com/irc_nas/2855/l_ipp-samples_p_7.1.1.013.tgz
	tar xvzf l_ipp-samples_p_7.1.1.013.tgz

* Set `IPPROOT` environment variable to point to location of IPP library
(i.e. directory containing subdirectories `include` and `lib/intel64` where
files such as `ippcore.h` `libippcore_l.a` are located,
the default path for IPP installation is: /opt/intel/composer_xe_2013.1.117/ipp)

	export IPPROOT=path/to/ipp

* Compile IPP sample codecs (current directory should be: path/to/codec-proto-net/ipp ):

	cd ipp-samples.7.1.1.013/builder
	perl build.pl --cmake=speech-codecs,intel64,make,s,st,release --build

* Set environment variable `IPP_SAMPLES_DIR` to point to location of built
IPP samples

	export SAMPLES_LIB_DIR=path/to/codec-proto-net/ipp/ipp-samples.7.1.1.013/__cmake/speech-codecs.intel64.make.s.st.release/__lib/release

* Set environment variable `LIBLITMUS_DIR` to point to location of liblitmus

	export LIBLITMUS_DIR=path/to/codec-proto-net/litmus/liblitmus

* Compilation (current path is: path/to/codec-proto-net/)

	mkdir build
	cd build
	cmake ..
	make -j $(nproc)
