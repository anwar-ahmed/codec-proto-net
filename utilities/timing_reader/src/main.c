
#include <stdio.h>
#include <stdint.h>
#include <time.h>

struct seq_num_and_timings
{
    uint16_t seq_num;
    struct timespec kernel_timestamp;
    struct timespec enc_alogrithm_timestamp;
    struct timespec task_done_timestamp;
};



int64_t timespecDiff(struct timespec *timeEnd, struct timespec *timeStart)
{
  return (((timeEnd->tv_sec * 1000000) + (timeEnd->tv_nsec / 1000)) -
           ((timeStart->tv_sec * 1000000) + (timeStart->tv_nsec / 1000)));
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("Usage: %s <timing-file> [output file]\n", argv[0]);
        return -1;
    }

    FILE *in_file = fopen(argv[1], "r");
    FILE *out_file;

    if (argc > 2)
    {
        out_file = fopen(argv[2], "w");
    }
    else
    {
        out_file = stdout;
    }

    struct seq_num_and_timings buffer;
    size_t bytes_read;



    while (1)
    {
        bytes_read = fread(&buffer, 1, sizeof(buffer), in_file);

        if (bytes_read < sizeof(buffer))
        {
            break ;
        }

        long long unsigned int schedulingDelay =
                timespecDiff(&buffer.enc_alogrithm_timestamp,
                        &buffer.kernel_timestamp);
        long long unsigned int algorithmicDelay =
                timespecDiff(&buffer.task_done_timestamp,
                        &buffer.enc_alogrithm_timestamp);
        long long unsigned int processingDelay =
                timespecDiff(&buffer.task_done_timestamp,
                        &buffer.kernel_timestamp);
        fprintf(out_file,
                "%.4u, %lld.%.9ld, %lld.%.9ld, "
                " scheduling_delay:%llu us, "
                " algorithmic_delay:%llu us, "
                " processing_delay:%llu us\n",
                buffer.seq_num,
                (long long)buffer.kernel_timestamp.tv_sec,
                buffer.kernel_timestamp.tv_nsec,
                (long long)buffer.task_done_timestamp.tv_sec,
                buffer.task_done_timestamp.tv_nsec,
                schedulingDelay,
                algorithmicDelay,
                processingDelay);
    }

    return 0;
}
