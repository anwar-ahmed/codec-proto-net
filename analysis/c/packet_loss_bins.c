
#include "packet_loss_bins.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME_MAX_LEN 256

//   1: 0
//   2: 1 
//   3: 2-3
//   4: 4-7
//   5: 8-15
//   6: 16-31
//   7: 32-63
//   8: 64-127
//   9: 128-255
//  10: 256-511
//  11: 512-1023
//  12: 1024-2047
//  13: 2048-4095
//  14: 4096-8191
//  15: 8192->
#define PACKET_LOSS_BINS 15

int packet_loss_bins(char *directory, char *scheduler,
        struct measurement_set **set_list,
        struct scheduler_stats *scheduler_stats)
{
    if (!set_list)
        return -1;

    size_t *_bins = malloc(sizeof(size_t) * PACKET_LOSS_BINS);
    memset(_bins, 0, sizeof(size_t) * PACKET_LOSS_BINS);

    size_t _packet_loss_total = 0;
    size_t _packet_loss_max = 0;
    size_t _packet_loss_consecutive_max = 0;

    struct measurement_set **_set_curr = set_list;
    while (*_set_curr)
    {
        size_t _packet_loss_curr = 0;
        for (size_t i = 1; i < (*_set_curr)->amount; ++i)
        {
            size_t _seq_num_diff = ((*_set_curr)->list + i)->seq_num -
                    ((*_set_curr)->list + i - 1)->seq_num;

            if (_seq_num_diff > 1)
            {
                _packet_loss_curr += _seq_num_diff - 1;
                if ((_seq_num_diff - 1) > _packet_loss_consecutive_max)
                    _packet_loss_consecutive_max = _seq_num_diff - 1;
            }
        }

        _packet_loss_total += _packet_loss_curr;

        if (_packet_loss_curr > _packet_loss_max)
            _packet_loss_max = _packet_loss_curr;

        size_t _bin_to_increase = 0;
        while ((_packet_loss_curr >> _bin_to_increase) &&
                (_bin_to_increase < PACKET_LOSS_BINS))
            ++_bin_to_increase;
        _bins[_bin_to_increase]++;

        _set_curr++;
    }

    scheduler_stats->packets_lost_total = _packet_loss_total;
    scheduler_stats->packets_lost_per_call_max = _packet_loss_max;
    scheduler_stats->consecutive_packets_lost_max = _packet_loss_consecutive_max;

    char _filename[FILENAME_MAX_LEN];
    snprintf(_filename, FILENAME_MAX_LEN, "%s%s_packet_loss_bins.txt",
            directory, scheduler);

    FILE *_file = fopen(_filename, "w");
    if (!_file)
    {
        printf("ERROR: Could not open %s for writing of packet loss "
                "histogram (%d - %s)\n", _filename, errno,
                strerror(errno));
        free(_bins);
        return -1;
    }

    for (size_t i = 0; i < PACKET_LOSS_BINS; ++i)
        fprintf(_file, "%zu\n", _bins[i]);

    fclose(_file);

    free(_bins);

    return 0;
}

