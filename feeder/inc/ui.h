
#pragma once

#include <stdint.h>

#define VERBOSITY_LEVELS_DEFAULT_VALUES {2, 1, 0, 1, 0}
enum FEEDER_UI_LOG_TYPE
{
    FEEDER_UI_LOG_TYPE_REPORT = 0,
    FEEDER_UI_LOG_TYPE_INFO = 1,
    FEEDER_UI_LOG_TYPE_CONTROL = 2,
    FEEDER_UI_LOG_TYPE_THREAD = 3,
    FEEDER_UI_LOG_TYPE_SCTP = 4
};

enum FEEDER_UI_LOG_TYPE_REPORT
{
    FEEDER_UI_LOG_TYPE_REPORT_NONE = 0,
    FEEDER_UI_LOG_TYPE_REPORT_FATAL_ERROR = 1,
    FEEDER_UI_LOG_TYPE_REPORT_ERROR = 2,
    FEEDER_UI_LOG_TYPE_REPORT_WARNING = 3
};

enum FEEDER_UI_LOG_TYPE_INFO
{
    FEEDER_UI_LOG_TYPE_INFO_NONE = 0,
    FEEDER_UI_LOG_TYPE_INFO_BASIC = 1,
    FEEDER_UI_LOG_TYPE_INFO_ADVANCED = 2,
    FEEDER_UI_LOG_TYPE_INFO_EXTENDED = 3
};

enum FEEDER_UI_LOG_TYPE_CONTROL
{
    FEEDER_UI_LOG_TYPE_CONTROL_NONE = 0,
    FEEDER_UI_LOG_TYPE_CONTROL_DEBUG = 1,
};

enum FEEDER_UI_LOG_TYPE_THREAD
{
    FEEDER_UI_LOG_TYPE_THREAD_NONE = 0,
    FEEDER_UI_LOG_TYPE_THREAD_INFO = 1,
    FEEDER_UI_LOG_TYPE_THREAD_DEBUG = 2
};

enum FEEDER_UI_LOG_TYPE_SCTP
{
    FEEDER_UI_LOG_TYPE_SCTP_NONE = 0,
    FEEDER_UI_LOG_TYPE_SCTP_VALID = 1,
    FEEDER_UI_LOG_TYPE_SCTP_ALL = 2
};

struct verbosity_levels
{
    enum FEEDER_UI_LOG_TYPE_REPORT report;
    enum FEEDER_UI_LOG_TYPE_INFO info;
    enum FEEDER_UI_LOG_TYPE_CONTROL control;
    enum FEEDER_UI_LOG_TYPE_THREAD thread;
    enum FEEDER_UI_LOG_TYPE_SCTP sctp;
};
//struct verbosity_levels
//{
//    int report;
//    int info;
//    int control;
//    int thread;
//    int sctp;
//}

struct feeder_ui_thread_start
{
    int *end;
    int display_thread_id;
    struct verbosity_levels verbosity_levels;
};

void feeder_ui_log(int type, int level, char *format, ...)
        __attribute__ ((format (printf, 3, 4)));

size_t *feeder_ui_codec_new(uint16_t id, size_t filesize, char *call_id);
void feeder_ui_codec_remove(uint16_t id);

void *feeder_ui_thread(void *arg);

void feeder_ui_wait_init_completion();
