function plothistogram(directory, min_max, bin_amount, output_file)

	fh1 = figure();
	clf();

	directory_contents = dir(directory);
	 
	data = directory_contents(1).name;

	for i = 1:length(directory_contents)
		if (strcmp(directory_contents(i).name, ".") == 1)
			printf("Tiedosto: %s", directory_contents(i).name);
			continue;
		endif

		if (strcmp(directory_contents(i).name, "..") == 1)
			printf("Tiedosto: %s", directory_contents(i).name);
			continue;
		endif

		printf("Tiedosto: %s", directory_contents(i).name);

		data = vertcat(load(strcat(directory,  directory_contents(i).name)));	
	endfor

	
	sh1 = stairs(linspace(min_max(1), min_max(2), length(data(:,2))), data(:,2), 'color', 'blue');
        set(sh1(1), 'linewidth', 3);
        set(gca(), 'linewidth', 3);
        set(gca(), 'fontsize', 22);
	set(gca(), 'yscale', 'log');
	hist(((data(:,3) - data(:,2))*1000000), bin_amount);
	xlabel('ms');

	print(output_file, '-dpng');
    
endfunction

