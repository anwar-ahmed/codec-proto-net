#define _GNU_SOURCE

#include "feeder_config.h"

#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <usc.h>

#include "codec.h"
#include "json_object.h"
#include "json_util.h"

int get_string_value(
        struct json_object *jobj,
        const char *key,
        char **destination)
{
    struct json_object *value = json_object_object_get(jobj, key);
    if (json_object_get_type(value) != json_type_string)
    {
        return CONFIG_ERROR_JSON_TYPE_EXPECT_STRING;
    }
    const char *value_str = json_object_get_string(value);
    *destination = (char *) malloc(sizeof(char) * (strlen(value_str) + 1));
    strcpy(*destination, value_str);

    return 0;
}

int get_integer_value(
        struct json_object *jobj,
        const char *key,
        int32_t *destination)
{
    struct json_object *value = json_object_object_get(jobj, key);
    if (json_object_get_type(value) != json_type_int)
    {
        return CONFIG_ERROR_JSON_TYPE_EXPECT_INTEGER;
    }
    *destination = json_object_get_int(value);

    return 0;
}

int get_direction_value(
        struct json_object *jobj,
        const char *key,
        USC_Direction *destination)
{
    struct json_object *value = json_object_object_get(jobj, key);
    if (json_object_get_type(value) != json_type_string)
    {
        return CONFIG_ERROR_JSON_TYPE_EXPECT_STRING;
    }
    const char *value_str = json_object_get_string(value);

    if (!strcmp(value_str, "ENCODE"))
    {
        *destination = USC_ENCODE;
    }
    else if (!strcmp(value_str, "DECODE"))
    {
        *destination = USC_DECODE;
    }
    else if (!strcmp(value_str, "DUPLEX"))
    {
        *destination = USC_DUPLEX;
    }
    else
    {
        return CONFIG_ERROR_DIRECTION_VALUE;
    }

    return 0;
}

int get_law_value(
        struct json_object *jobj,
        const char *key,
        int32_t *destination)
{
    struct json_object *value = json_object_object_get(jobj, key);
    if (json_object_get_type(value) != json_type_string)
    {
        return CONFIG_ERROR_JSON_TYPE_EXPECT_STRING;
    }
    const char *value_str = json_object_get_string(value);

    if (!strcmp(value_str, "LINEAR"))
    {
        *destination = 0;
    }
    else if (!strcmp(value_str, "ALAW"))
    {
        *destination = 1;
    }
    else if (!strcmp(value_str, "ULAW"))
    {
        *destination = 2;
    }
    else
    {
        return CONFIG_ERROR_LAW_VALUE;
    }

    return 0;
}

int get_outmode_value(
        struct json_object *jobj,
        const char *key,
        USC_OutputMode *destination)
{
    struct json_object *value = json_object_object_get(jobj, key);
    if (json_object_get_type(value) != json_type_string)
    {
        return CONFIG_ERROR_JSON_TYPE_EXPECT_STRING;
    }
    const char *value_str = json_object_get_string(value);

    if (!strcmp(value_str, "NO_CONTROL"))
    {
        *destination = USC_OUT_NO_CONTROL;
    }
    else if (!strcmp(value_str, "MONO"))
    {
        *destination = USC_OUT_MONO;
    }
    else if (!strcmp(value_str, "STEREO"))
    {
        *destination = USC_OUT_STEREO;
    }
    else if (!strcmp(value_str, "COMPATIBLE"))
    {
        *destination = USC_OUT_COMPATIBLE;
    }
    else if (!strcmp(value_str, "DELAY"))
    {
        *destination = USC_OUT_DELAY;
    }
    else
    {
        return CONFIG_ERROR_OUTMODE_VALUE;
    }

    return 0;
}

int parse_pcmtype_object(
        struct json_object *jobj,
        USC_PCMType *pcmtype)
{
    int result;
    if ((result = get_integer_value(jobj, "sample-frequency", &pcmtype->sample_frequency)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "bit-per-sample", &pcmtype->bitPerSample)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "channels", &pcmtype->nChannels)))
    {
        return result;
    }

    return 0;
}

int parse_modes_object(
        struct json_object *jobj,
        USC_Modes *modes)
{
    int result;
    if ((result = get_integer_value(jobj, "bitrate", &modes->bitrate)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "truncate", &modes->truncate)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "vad", &modes->vad)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "hpf", &modes->hpf)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "pf", &modes->pf)))
    {
        return result;
    }
    if ((result = get_outmode_value(jobj, "out-mode", &modes->outMode)))
    {
        return result;
    }

    return 0;
}

int parse_start_object(
        struct json_object *jobj,
        struct timespec *start_time)
{
    int result;
    if ((result = get_integer_value(jobj, "sec", (int *) &start_time->tv_sec)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "nsec", (int *) &start_time->tv_nsec)))
    {
        return result;
    }

    return 0;
}

int parse_parameters_object(
        struct json_object *jobj,
        USC_Option *parameters)
{
    int result;

    if ((result = get_direction_value(jobj, "direction", &parameters->direction)))
    {
        return result;
    }
    if ((result = get_law_value(jobj, "law", &parameters->law)))
    {
        return result;
    }

    struct json_object *pcmtype = json_object_object_get(jobj, "pcm-type");
    if (json_object_get_type(pcmtype) != json_type_object)
    {
        return CONFIG_ERROR_JSON_TYPE_PCMTYPE;
    }
    if ((result = parse_pcmtype_object(pcmtype, &parameters->pcmType)))
    {
        return result;
    }

    parameters->nModes = sizeof(USC_Modes)/sizeof(int);
    struct json_object *modes = json_object_object_get(jobj, "modes");
    if (json_object_get_type(modes) != json_type_object)
    {
        return CONFIG_ERROR_JSON_TYPE_MODES;
    }
    if ((result = parse_modes_object(modes, &parameters->modes)))
    {
        return result;
    }

    return 0;
}

int parse_codec_object(
        struct json_object *jobj,
        struct codec_config **codec_config)
{
    int result;

    if ((result = get_string_value(jobj, "id", &(**codec_config).id)))
    {
        return result;
    }
    if ((result = get_string_value(jobj, "codec", &(**codec_config).codec_name)))
    {
        return result;
    }
    if ((result = get_string_value(jobj, "in-filename", &(**codec_config).in_filename)))
    {
        return result;
    }
    if ((result = get_string_value(jobj, "out-filename", &(**codec_config).out_filename)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "framesize", (int *) &(**codec_config).framesize)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "timing", (int *) &(**codec_config).timing)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "weight", (int *) &(**codec_config).weight)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "buffer_size", (int *) &(**codec_config).buffer_size)))
    {
        return result;
    }
    if ((result = get_integer_value(jobj, "max_delay", (int *) &(**codec_config).max_delay)))
    {
        return result;
    }


    struct json_object *start = json_object_object_get(jobj, "start");
    if (json_object_get_type(start) != json_type_object)
    {
        return CONFIG_ERROR_JSON_TYPE_START;
    }
    if ((result = parse_start_object(start, &(**codec_config).start_time)))
    {
        return result;
    }

    struct json_object *parameters = json_object_object_get(jobj, "parameters");
    if (json_object_get_type(parameters) != json_type_object)
    {
        return CONFIG_ERROR_JSON_TYPE_PARAMETERS;
    }
    if ((result = parse_parameters_object(parameters, &(**codec_config).parameters)))
    {
        return result;
    }

    return 0;
}

int parse_root_object(
        struct json_object *jobj,
        struct codec_config ***codec_config_list,
        const char *filename)
{
    int array_length = json_object_array_length(jobj);

    *codec_config_list = (struct codec_config **)
            malloc(sizeof(struct codec_config *) * (array_length + 1));
    (*codec_config_list)[array_length] = NULL;

    for (size_t i = 0; i < (size_t)array_length; ++i)
    {
        struct json_object *array_entry = json_object_array_get_idx(jobj, i);

        struct codec_config *codec_config = (struct codec_config *)
                malloc(sizeof(struct codec_config));

        // get_integer_value() will only set 32-bit parts of integer values
        // and therefore we need to zero the rest of the bits (negative values
        // obviously don't work since they should have 1's in the higher order bits)
        memset(codec_config, 0, sizeof(struct codec_config));

        int result;
        if ((result = parse_codec_object(array_entry, &codec_config)))
        {
            return result;
        }

        asprintf(&codec_config->campaign_filename, "%s", filename);

        (*codec_config_list)[i] = codec_config;
    }

    return 0;
}

int load_config_file(
        const char *filename,
        struct codec_config ***codec_config_list)
{
    struct json_object *jobj = json_object_from_file(filename);

    if (!jobj)
    {
        return CONFIG_ERROR_JSON_FILE_READ_FAILED;
    }

    if (json_object_get_type(jobj) != json_type_array)
    {
        return CONFIG_ERROR_JSON_TYPE_ROOT;
    }

    int result = parse_root_object(jobj, codec_config_list, filename);

    json_object_put(jobj);

    return result;
}

