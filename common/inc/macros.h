/*
 * macros.h
 *
 *  Created on: Jul 20, 2015
 *      Author: eheihol
 */

#ifndef COMMON_INC_MACROS_H_
#define COMMON_INC_MACROS_H_


// Helper macro to vectorize a function

#define Fn_apply(type, fn, ...) {                                      \
    void *apply_stopper = (int[]){0};                                  \
    type **fn_apply_list = (type*[]){__VA_ARGS__, apply_stopper};      \
    for (int i=0; fn_apply_list[i] != apply_stopper; i++)              \
        fn(fn_apply_list[i]);                                          \
}

// Macro to free memory
#define free_all(...) Fn_apply(void, free, __VA_ARGS__);

#endif /* COMMON_INC_MACROS_H_ */
