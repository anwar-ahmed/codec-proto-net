function sets = load_timing_histograms(directory, scheduler)
    dir_contents = dir(directory);

    set_amount = 0;
    for i = 1:length(dir_contents)
        if (index(dir_contents(i).name, ".txt") == 0)
            continue;
        endif

        if (index(dir_contents(i).name, strcat(scheduler, "_jitter_")) == 0)
            continue;
        endif

        % remove .txt from the end
        temp_name = strtrunc(dir_contents(i).name, rindex(dir_contents(i).name, ".") - 1);

        % remove <scheduler> from the beginning
        temp_name = substr(temp_name, length(scheduler) + 1);

        % remove _jitter_ from the beginning
        set_name = substr(temp_name, 9);

        set_amount += 1;

        sets(set_amount).name = set_name;
        sets(set_amount).bins = load(strcat(directory, dir_contents(i).name));

        %printf('set loaded: %s\n', set_name);
    endfor
endfunction
