# IPP
set(IPP_SAMPLE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/ipp)

if(NOT DEFINED ENV{IPPROOT})
    message(SEND_ERROR "You must set IPPROOT environment variable")
    return()
endif()

if(NOT DEFINED ENV{SAMPLES_LIB_DIR})
    message(SEND_ERROR "You must set SAMPLES_LIB_DIR environment variable")
    return()
endif()


set(IPP_INSTALL_DIR $ENV{IPPROOT})
set(IPP_LIB_DIR ${IPP_INSTALL_DIR}/lib/${ARCH})
set(SAMPLES_LIB_DIR $ENV{SAMPLES_LIB_DIR})

message("IPP_INSTALL_DIR: ${IPP_INSTALL_DIR}")
message("IPP_LIB_DIR: ${IPP_LIB_DIR}")
message("SAMPLES_LIB_DIR: ${SAMPLES_LIB_DIR}")

# Codecs
set(USCROOT ${IPP_SAMPLE_DIR}/ipp-samples.7.1.1.013/sources/speech-codecs)
