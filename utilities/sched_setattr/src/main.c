
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "sched_headers.h"

void print_usage(char *binary_name)
{
    printf("usage:\n");
    printf("  %s <pid> <runtime> <period>\n", binary_name);
    printf("  %s <pid> -f <priority>\n", binary_name);
    printf("\n");
    printf("The default is to set SCHED_DEADLINE parameters. Using the -f"
            " flag one can set FIFO scheduling\n");
}

void parse_cmd_line(int argc, char **argv, size_t *pid, size_t *runtime,
        size_t *period)
{
    *pid = atoi(argv[1]);

    if (*argv[2] == '-')
    {
        // FIFO mode
        *runtime = 0;
        *period = atoi(argv[3]);
        return ;
    }

    *runtime = atoi(argv[2]);
    *period = atoi(argv[3]);

    return ;
}

int main(int argc, char **argv)
{
    size_t pid;
    size_t runtime;
    size_t period;

    if (argc < 4)
    {
        print_usage(argv[0]);
        return -1;
    }

    parse_cmd_line(argc, argv, &pid, &runtime, &period);

    struct sched_attr _sched_attr;
    memset(&_sched_attr, 0, sizeof(struct sched_attr));
    _sched_attr.size = sizeof(struct sched_attr);

    if (runtime > 0)
    {
        _sched_attr.sched_policy = __SCHED_DEADLINE;
        _sched_attr.sched_period = period;
        _sched_attr.sched_deadline = period;
        _sched_attr.sched_runtime = runtime;
        _sched_attr.sched_flags = __SCHED_FLAG_SOFT_RSV;
    }
    else
    {
        _sched_attr.sched_policy = __SCHED_FIFO;
        _sched_attr.sched_priority = period;
    }

    // sched_setattr(pid_t pid, struct sched_attr *uattr, unsigned int flags)
    if (syscall(SYSCALL_SETATTR, pid, &_sched_attr, 0))
    {
        printf("ERROR: sched_setattr() failed (%d - %s)\n", errno,
                strerror(errno));
        return -1;
    }

    return 0;
}

