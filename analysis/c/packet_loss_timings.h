
#pragma once

#include <time.h>

#include "measurements.h"

int packet_loss_timings(char *directory, char *scheduler,
        struct measurement_set **set_list, struct timespec *start,
        struct timespec *end, size_t packet_loss_timing_accuracy_ms);

