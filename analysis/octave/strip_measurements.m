function measurements = strip_measurements(measurements_all, earliest, latest)
    for i = 1:length(measurements_all)
        smallest_idx = binary_search_find_smallest(measurements_all(i).timings, earliest);
        largest_idx = binary_search_find_largest(measurements_all(i).timings, latest);

        measurements(i).timings = measurements_all(i).timings(smallest_idx:largest_idx);
        measurements(i).seq_nums = measurements_all(i).seq_nums(smallest_idx:largest_idx);
    endfor
endfunction

function smallest_idx = binary_search_find_smallest(the_list, more_than)
    min_idx = 1;
    max_idx = length(the_list);
    while (max_idx > min_idx)
        curr_idx = round((max_idx + min_idx)/2);

        if (the_list(curr_idx) > more_than)
            if ((curr_idx == 1) || (the_list(curr_idx - 1) < more_than))
                smallest_idx = curr_idx;
                return ;
            endif

            max_idx = curr_idx; % correct??
        elseif (the_list(curr_idx) < more_than)
            min_idx = curr_idx; % correct??
        else
            smallest_idx = curr_idx;
            return ;
        endif
    endwhile
    smallest_idx = min_idx;
endfunction

function largest_idx = binary_search_find_largest(the_list, less_than)
    min_idx = 1;
    max_idx = length(the_list);
    while (max_idx > min_idx)
        curr_idx = round((max_idx + min_idx)/2);

        if (the_list(curr_idx) < less_than)
            if ((curr_idx == length(the_list)) || (the_list(curr_idx + 1) > less_than))
                largest_idx = curr_idx;
                return ;
            endif

            min_idx = curr_idx; % correct??
        elseif (the_list(curr_idx) > less_than)
            max_idx = curr_idx; % correct??
        else
            largest_idx = curr_idx;
            return ;
        endif
    endwhile
    largest_idx = min_idx;
endfunction
