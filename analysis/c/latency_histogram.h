/*
 * latency_histogram.h
 *
 *  Created on: Aug 24, 2015
 *      Author: eheihol
 */

#ifndef ANALYSIS_C_LATENCY_HISTOGRAM_H_
#define ANALYSIS_C_LATENCY_HISTOGRAM_H_

#include "measurements.h"

int latency_histogram(char *directory, char *scheduler,
        struct measurement_set **set_list, size_t latency_min_ms,
        size_t latency_max_ms, size_t bin_amount);

#endif /* ANALYSIS_C_LATENCY_HISTOGRAM_H_ */
