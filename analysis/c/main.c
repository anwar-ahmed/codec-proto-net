
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>


#include "latency_histogram.h"
#include "scheduling_latency_histogram.h"
#include "processing_latency_histogram.h"
#include "jitter_histogram.h"
#include "measurements.h"
#include "packet_loss_bins.h"
#include "packet_loss_timings.h"
#include "save_raw_data.h"


#define FILENAME_MAX_LEN 256

static void print_usage(char *prog_name)
{
    printf("usage: %s [options] <directory>\n", prog_name);
    printf("\n");
    printf("options:\n");
    printf("  -s BEFORE,AFTER\n");
    printf("        safe intervals (seconds)\n");
    printf("  -d MIN,MAX\n");
    printf("        jitter [min,max] amount (ms)\n");
    printf("  -b AMOUNT\n");
    printf("        jitter bin amount\n");
    printf("  -a INTERVAL\n");
    printf("        packet loss timing accuracy (ms)\n");
    printf("  -r SAVERAWDATA\n");
    printf("        save raw data file\n");
    printf("\n");
}

static void dual_value_parse(char *arg, size_t *first_value,
        size_t *second_value)
{
    size_t first_str_len = 0;
    size_t second_str_len = 0;

    char *first_str_ptr;
    char *second_str_ptr;

    size_t i = 0;
    if (arg[i] == '[')
        first_str_ptr = arg + 1;
    else
        first_str_ptr = arg;

    while (arg[i] != ',')
        ++i;

    first_str_len = i - (first_str_ptr - arg);
    second_str_ptr = arg + i + 1;

    second_str_len = strlen(arg) - first_str_len - 1;
    if (first_str_ptr != arg)
        second_str_len += 2;

    char *first_str = malloc(first_str_len + 1);
    strncpy(first_str, first_str_ptr, first_str_len);
    first_str[first_str_len] = '\0';
    *first_value = atoi(first_str);
    free(first_str);

    char *second_str = malloc(second_str_len + 1);
    strncpy(second_str, second_str_ptr, second_str_len);
    second_str[second_str_len] = '\0';
    *second_value = atoi(second_str);
    free(second_str);
}

static int parse_cmd_line(int argc, char **argv, char **directory,
        size_t *safe_before, size_t *safe_after, size_t *jitter_min_ms,
        size_t *jitter_max_ms, size_t *jitter_bin_amount,
        size_t *packet_loss_timing_accuracy_ms, int *save_raw_data)
{
    int opt;

    while ((opt = getopt(argc, argv, "hrs:d:b:a:")) != -1)
    {
        switch (opt)
        {
            case 's':
                dual_value_parse(optarg, safe_before, safe_after);
                break;
            case 'd':
                dual_value_parse(optarg, jitter_min_ms, jitter_max_ms);
                break;
            case 'b':
                *jitter_bin_amount = atoi(optarg);
                break;
            case 'a':
                *packet_loss_timing_accuracy_ms = atoi(optarg);
                break;
            case 'r':
                *save_raw_data = 1;
                break;
            default:
                printf("\n");
            case 'h':
                print_usage(argv[0]);
                return -1;
        }
    }

    if (optind >= argc)
    {
        printf("ERROR: no directory specified!\n");
        printf("\n");
        print_usage(argv[0]);
        return -1;
    }

    char *_tmp = argv[optind];
    size_t _strlen_tmp = strlen(_tmp);
    char *_directory;
    if (_tmp[_strlen_tmp-1] == '/')
    {
        _directory = malloc(_strlen_tmp + 1);
        strncpy(_directory, _tmp, _strlen_tmp + 1);
    }
    else
    {
        _directory = malloc(_strlen_tmp + 2);
        strncpy(_directory, _tmp, _strlen_tmp);
        _directory[_strlen_tmp-1] = '/';
        _directory[_strlen_tmp] = '\0';
    }
    *directory = _directory;

    return 0;
}

struct subdirectory_tmp_struct
{
    char *name;
    struct subdirectory_tmp_struct *next;
};
static int parse_subdirectories(char *directory, char ***subdirs,
        size_t *subdir_amount)
{
    DIR *dirp;
    struct dirent *dp;

    dirp = opendir(directory);
    if (!dirp)
    {
        printf("ERROR: opendir() failed on %s\n", directory);
        return -1;
    }

    struct subdirectory_tmp_struct _first;
    struct subdirectory_tmp_struct *_prev;
    _first.name = NULL;
    _first.next = NULL;
    _prev = NULL;
    size_t _amount = 0;
    while (1)
    {
        errno = 0;
        dp = readdir(dirp);
        if (!dp)
            break ;

        if (dp->d_name[0] == '.')
            continue ;



        if (strstr(dp->d_name, "rawdata"))
                continue;

        struct stat _stat;
        char _stat_filename[FILENAME_MAX_LEN];
        snprintf(_stat_filename, FILENAME_MAX_LEN, "%s%s", directory,
                dp->d_name);
        if (stat(_stat_filename, &_stat))
        {
            printf("ERROR: Reading file %s stat failed with %d - %s\n",
                    _stat_filename, errno, strerror(errno));
            closedir(dirp);
            return -1;
        }

        if (!S_ISDIR(_stat.st_mode))
            continue ;

        if (!_prev)
            _prev = &_first;
        else
        {
            _prev->next = malloc(sizeof(struct subdirectory_tmp_struct));
            _prev = _prev->next;
        }

        _prev->name = malloc(strlen(dp->d_name) + 1);
        strncpy(_prev->name, dp->d_name, strlen(dp->d_name) + 1);
        _prev->next = NULL;
        _amount++;
    }

    if (errno)
    {
        printf("ERROR: readdir() failed with %d - %s\n", errno, strerror(errno));
        return -1;
    }
    closedir(dirp);

    char **_subdirs = malloc(sizeof(char *) * _amount);
    _prev = &_first;
    for (size_t i = 0; i < _amount; ++i)
    {
        if (!_prev)
        {
            printf("ERROR: _prev is null :-( go fix your code!\n");
            return -1;
        }
        _subdirs[i] = _prev->name;
        _prev = _prev->next;
    }
    *subdir_amount = _amount;
    *subdirs = _subdirs;

    return 0;
}

int main(int argc, char **argv)
{
    char *directory = NULL;
    size_t safe_before = 10;  // s
    size_t safe_after = 2;
    size_t jitter_min_ms = 0; // ms
    size_t jitter_max_ms = 100; // ms
    size_t jitter_bin_amount = 200;

    // Adjust these
    size_t latency_min_ms = 0; //ms
    size_t latency_max_ms = 200; //ms
    size_t latency_bin_amount = 200;

    int save_raw_data = 0;


    size_t packet_loss_timing_accuracy_ms = 1000; // ms

    int _error = 0;

    if (parse_cmd_line(argc, argv, &directory, &safe_before, &safe_after,
            &jitter_min_ms, &jitter_max_ms, &jitter_bin_amount,
            &packet_loss_timing_accuracy_ms, &save_raw_data))
        return -1;

    char **schedulers = NULL;
    size_t schedulers_amount = 0;
    if (parse_subdirectories(directory, &schedulers, &schedulers_amount))
        return -1;

    printf("Starting analysis of directory %s:\n", directory);
    printf("  safe before: %zu s\n", safe_before);
    printf("  safe after: %zu s\n", safe_after);
    printf("  jitter range (ms): [%zu, %zu]\n", jitter_min_ms, jitter_max_ms);
    printf("  jitter bin amount: %zu\n", jitter_bin_amount);
    printf("  saving raw files: %d", save_raw_data);
    printf("  packet loss timing accuracy: %zu ms\n",
            packet_loss_timing_accuracy_ms);
    printf("  schedulers: {");
    for (size_t i = 0; i < (schedulers_amount - 1); ++i)
        printf("%s, ", schedulers[i]);
    printf("%s}\n", schedulers[schedulers_amount - 1]);
    printf("\n");

    printf("Loading measurements...\n");

    struct timespec *latest_start = malloc(sizeof(struct timespec) *
            schedulers_amount);
    struct timespec *earliest_end = malloc(sizeof(struct timespec) *
            schedulers_amount);

    struct measurement_set ***set_list = malloc(
            sizeof(struct measurement_set **) * schedulers_amount);

    struct stats stats;
    stats.call_amount_first = NULL;
    stats.scheduler_stats = malloc(sizeof(struct scheduler_stats) *
            schedulers_amount);
    for (size_t i = 0; i < schedulers_amount; ++i)
    {
        if (load_directory(directory, schedulers[i], set_list + i,
                latest_start + i, earliest_end + i, &save_raw_data))
            return -1;
        printf("Measurements for %s loaded.\n", schedulers[i]);
        move_timing_s(latest_start + i, safe_before);
        move_timing_s(earliest_end + i, -((int) safe_after));

        if (timing_compare(earliest_end + i, latest_start + i) != 1)
        {
            printf("ERROR: start later than end!\n");
            printf("Problematic scheduler is '%s'\n", schedulers[i]);
            printf("latest start == %lld\n", (long long) latest_start[i].tv_sec);
            printf("earliest end == %lld\n", (long long) earliest_end[i].tv_sec);
            return -1;
        }
    }

    find_out_call_amounts(*set_list, &stats.call_amount_first);

    printf("All measurements loaded!\n");
    printf("\n");

//
//    printf("Writing raw data!");
//    printf("\n");
//
//    for (size_t i = 0; i < schedulers_amount; i++){
//        if(save_raw_data(directory, schedulers[i], set_list[i])){
//            _error = -1;
//            goto label_exit;
//        }
//
//
//    }
//
//    printf("Raw data saved!\n");

    printf("Parsing invalid measurements...\n");

    for (size_t i = 0; i < schedulers_amount; ++i)
        if (parse_invalid_measurements(set_list[i], latest_start + i,
                earliest_end + i))
        {
            _error = -1;
            goto label_exit;
        }

    printf("Invalid measurements parsed!\n");
    printf("\n");
    printf("Building processing latency histograms...\n");

    for (size_t i = 0; i < schedulers_amount; ++i){
        if (processing_latency_histogram(directory, schedulers[i], set_list[i],
                latency_min_ms, latency_max_ms, latency_bin_amount))
        {
            _error = -1;
            goto label_exit;
        }
    }

    printf("Processing latency histograms built and saved!\n");
    printf("\n");
    printf("Building network latency histograms...\n");

    for (size_t i = 0; i < schedulers_amount; ++i){
        if (scheduling_latency_histogram(directory, schedulers[i], set_list[i],
                latency_min_ms, latency_max_ms, latency_bin_amount))
        {
            _error = -1;
            goto label_exit;
        }
    }

    printf("Scheduling latency histograms built and saved!\n");
    printf("\n");
    printf("Building latency histograms...\n");

    for (size_t i = 0; i < schedulers_amount; ++i){
        if (latency_histogram(directory, schedulers[i], set_list[i],
                latency_min_ms, latency_max_ms, latency_bin_amount))
        {
            _error = -1;
            goto label_exit;
        }
    }

    printf("Scheduling histograms built and saved!\n");
    printf("\n");
    printf("Building jitter histograms...\n");

    for (size_t i = 0; i < schedulers_amount; ++i)
        if (jitter_histogram(directory, schedulers[i], set_list[i],
                jitter_min_ms, jitter_max_ms, jitter_bin_amount))
        {
            _error = -1;
            goto label_exit;
        }

    printf("Jitter histograms built and saved!\n");
    printf("\n");
    printf("Building packet loss histogram & statistics...\n");

    for (size_t i = 0; i < schedulers_amount; ++i)
        if (packet_loss_bins(directory, schedulers[i], set_list[i],
                stats.scheduler_stats + i))
        {
            _error = -1;
            goto label_exit;
        }

    printf("Packet loss histogram & statistics built!\n");
    printf("\n");
    printf("Building packet loss timing diagram...\n");

    for (size_t i = 0; i < schedulers_amount; ++i)
        if (packet_loss_timings(directory, schedulers[i], set_list[i],
                latest_start + i, earliest_end + i,
                packet_loss_timing_accuracy_ms))
        {
            _error = -1;
            goto label_exit;
        }

    printf("Packet loss timing diagram built!\n");
    printf("\n");
    printf("Writing stats file...\n");

    if (write_stats_to_file(directory, schedulers, &stats, schedulers_amount))
    {
        _error = -1;
        goto label_exit;
    }

    printf("Stats file written!\n");
    printf("\n");
    printf("All analysis done!\n");

label_exit:
    free(directory);
    for (size_t i = 0; i < schedulers_amount; ++i)
        free(schedulers[i]);
    free(schedulers);
    free(stats.scheduler_stats);
    struct call_amount *_call_amounts = stats.call_amount_first;
    while (_call_amounts)
    {
        struct call_amount *_removable = _call_amounts;
        _call_amounts = _call_amounts->next;
        free(_removable);
    }
    for (size_t i = 0; i < schedulers_amount; ++i)
        free_measurement_set_list(set_list[i]);

    return _error;
}

