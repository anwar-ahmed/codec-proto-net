
#include "timing.h"

#include <time.h>

// http://www.gnu.org/software/libc/manual/html_node/Elapsed-Time.html

void time_difference(
        struct timespec *start,
        struct timespec *end,
        struct timespec *difference)
{
    if (end->tv_nsec >= start->tv_nsec)
    {
        difference->tv_sec = end->tv_sec - start->tv_sec;
        difference->tv_nsec = end->tv_nsec - start->tv_nsec;
    }
    else
    {
        difference->tv_sec = end->tv_sec - start->tv_sec - 1;
        difference->tv_nsec = end->tv_nsec - start->tv_nsec + 1000000000;
    }
}
