function load_and_save(directory, schedulers, jitter_min_max, latency_min_max)
    packet_loss_bins_max = 0;
    packet_loss_timings_max = 0;

    latency_min_max = jitter_min_max;

    for i=1:size(schedulers,1)
        [sched(i).jitter_sets, sched(i).latency_sets, sched(i).packet_loss_bins, sched(i).packet_loss_timings] = load_analysis_results(directory, deblank(schedulers(i,:)));

        temp_loss_bins_max = max(sched(i).packet_loss_bins(2:end));
        if (temp_loss_bins_max > packet_loss_bins_max)
            packet_loss_bins_max = temp_loss_bins_max;
        endif

        temp_loss_timings_max = max(sched(i).packet_loss_timings(:,2));
        if (temp_loss_timings_max > packet_loss_timings_max)
            packet_loss_timings_max = temp_loss_timings_max;
        endif
    endfor

    if (packet_loss_bins_max == 0)
        packet_loss_bins_max = 10;
    endif
    if (packet_loss_timings_max == 0)
        packet_loss_timings_max = 10;
    endif

%    for i=1:size(schedulers,1)
%        save_analysis_results(directory, deblank(schedulers(i,:)), sched(i).jitter_sets, jitter_min_max, sched(i).packet_loss_bins, packet_loss_bins_max, sched(i).packet_loss_timings, packet_loss_timings_max);
%    endfor

    half_amount = size(schedulers,1)/2;
    for i=1:half_amount
        save_analysis_results(directory, deblank(schedulers(i,:)), deblank(schedulers(i+half_amount,:)), sched(i).jitter_sets, sched(i+half_amount).jitter_sets, jitter_min_max, sched(i).latency_sets, sched(i+half_amount).latency_sets, latency_min_max, sched(i).packet_loss_bins +  sched(i+half_amount).packet_loss_bins, packet_loss_bins_max, sched(i).packet_loss_timings(1:50,:) + sched(i+half_amount).packet_loss_timings(1:50,:), packet_loss_timings_max);
    endfor
endfunction
