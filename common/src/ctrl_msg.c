
#include "ctrl_msg.h"

#include <errno.h>
#include <netinet/sctp.h>
#include <stdlib.h>
#include <string.h>

#include "codec.h"

#define CTRL_MSG_BUFFER_SIZE 1024

static int ctrl_msg_send(socket_handle_t handle,
        struct ctrl_msg_hdr *ctrl_msg_hdr, void *data, size_t data_len,
        uint32_t stream_no)
{
    char *_send_buffer;
    if (data == NULL || data_len == 0)
    {
        _send_buffer = (char *) ctrl_msg_hdr;
    }
    else
    {
        _send_buffer = malloc(sizeof(struct ctrl_msg_hdr) + data_len);
        memcpy(_send_buffer, ctrl_msg_hdr, sizeof(struct ctrl_msg_hdr));
        memcpy(_send_buffer + sizeof(struct ctrl_msg_hdr), data, data_len);
    }
    int result = sctp_sendmsg(
            handle->fd_sctp_connection,
            _send_buffer, 
            sizeof(struct ctrl_msg_hdr) + data_len,
            NULL,                       // to (need if connectionless)
            0,                          // tolen
            0,                          // ppid (didn't understand)
            0,                          // flags
            stream_no,
            0,                          // timetolive (ms)
            0);                         // context (dunno)

    if (_send_buffer != (char *) ctrl_msg_hdr)
    {
        free(_send_buffer);
    }

    return result;
}

int ctrl_msg_send_connection_udp_opened_inf(socket_handle_t handle)
{
    struct ctrl_msg_hdr ctrl_msg_hdr;
    ctrl_msg_hdr.type = CTRL_MSG_TYPE_CONNECTION_UDP_OPENED_INF;

    int result = ctrl_msg_send(handle, &ctrl_msg_hdr, NULL, 0, 0);

    return result == -1 ? -1 : 0;
}

int ctrl_msg_send_connection_udp_opened_ack(socket_handle_t handle)
{
    struct ctrl_msg_hdr ctrl_msg_hdr;
    ctrl_msg_hdr.type = CTRL_MSG_TYPE_CONNECTION_UDP_OPENED_ACK;

    int result = ctrl_msg_send(handle, &ctrl_msg_hdr, NULL, 0, 0);

    return result == -1 ? -1 : 0;
}

int ctrl_msg_send_connection_udp_close(socket_handle_t handle)
{
    struct ctrl_msg_hdr ctrl_msg_hdr;
    ctrl_msg_hdr.type = CTRL_MSG_TYPE_CONNECTION_UDP_CLOSED;

    int result = ctrl_msg_send(handle, &ctrl_msg_hdr, NULL, 0, 0);

    return result == -1 ? -1 : 0;
}

int ctrl_msg_send_codec_start_req(
        socket_handle_t handle,
        int id,
        const struct codec_config *codec_config,
        const char *timing_output_filename)
{
    struct ctrl_msg_hdr ctrl_msg_hdr;
    ctrl_msg_hdr.type = CTRL_MSG_TYPE_CODEC_START_REQ;

    struct ctrl_msg_codec_start_req ctrl_msg;

    memset(ctrl_msg.codec_name, 0, CODEC_NAME_MAX_LENGTH);
    strncpy(ctrl_msg.codec_name,
            codec_config->codec_name,
            CODEC_NAME_MAX_LENGTH);
    ctrl_msg.codec_name[CODEC_NAME_MAX_LENGTH-1] = '\0';

    strncpy(ctrl_msg.timing_output_filename,
            timing_output_filename,
            TIMING_OUTPUT_FILENAME_MAX_LENGTH);
    ctrl_msg.timing_output_filename[TIMING_OUTPUT_FILENAME_MAX_LENGTH-1] = '\0';

    ctrl_msg.timing = codec_config->timing;
    ctrl_msg.weight = codec_config->weight;
    ctrl_msg.framesize = codec_config->framesize;
    ctrl_msg.buffer_size = codec_config->buffer_size;
    ctrl_msg.max_delay = codec_config->max_delay;
    ctrl_msg.parameters = codec_config->parameters;

    int result = ctrl_msg_send(handle, &ctrl_msg_hdr, (char *) &ctrl_msg,
            sizeof(struct ctrl_msg_codec_start_req), id);

    return result == -1 ? -1 : 0;
}

int ctrl_msg_send_codec_start_acc(socket_handle_t handle, int id)
{
    struct ctrl_msg_hdr ctrl_msg_hdr;
    ctrl_msg_hdr.type = CTRL_MSG_TYPE_CODEC_START_ACC;

    int result = ctrl_msg_send(handle, &ctrl_msg_hdr, NULL, 0, id);

    return result == -1 ? -1 : 0;
}

int ctrl_msg_send_codec_start_dec(socket_handle_t handle, int id)
{
    struct ctrl_msg_hdr ctrl_msg_hdr;
    ctrl_msg_hdr.type = CTRL_MSG_TYPE_CODEC_START_DEC;

    int result = ctrl_msg_send(handle, &ctrl_msg_hdr, NULL, 0, id);

    return result == -1 ? -1 : 0;
}

int ctrl_msg_send_codec_end(socket_handle_t handle, int id)
{
    struct ctrl_msg_hdr ctrl_msg_hdr;
    ctrl_msg_hdr.type = CTRL_MSG_TYPE_CODEC_END;

    int result = ctrl_msg_send(handle, &ctrl_msg_hdr, NULL, 0, id);

    return result == -1 ? -1 : 0;
}

int ctrl_msg_send_codec_error(socket_handle_t handle, int id,
        uint16_t error_code)
{
    struct ctrl_msg_hdr ctrl_msg_hdr;
    ctrl_msg_hdr.type = CTRL_MSG_TYPE_CODEC_ERROR;

    struct ctrl_msg_codec_error ctrl_msg;
    ctrl_msg.error_code = error_code;

    int result = ctrl_msg_send(handle, &ctrl_msg_hdr, (char *) &ctrl_msg,
            sizeof(struct ctrl_msg_codec_error), id);

    return result == -1 ? -1 : 0;
}

int ctrl_msg_recv(socket_handle_t handle, enum CTRL_MSG_TYPE *msg_type,
        uint16_t *stream_id, char **data, uint16_t *data_len)
{
    if (!msg_type)
        return -CTRL_MSG_RECV_ERROR_INVAL;

    char recv_buffer[CTRL_MSG_BUFFER_SIZE];

    struct sctp_sndrcvinfo sinfo;
    memset(&sinfo, 0, sizeof(struct sctp_sndrcvinfo));
    int flags = 0;
    int recv_result = 0;

    recv_result = sctp_recvmsg(handle->fd_sctp_connection, recv_buffer,
            CTRL_MSG_BUFFER_SIZE, NULL, NULL, &sinfo, &flags);

    if (recv_result == 0)
        return -CTRL_MSG_RECV_ERROR_CLIENT_DISCONNECTED;

    if (recv_result == -1)
        return -1;      // caller should check errno

    uint16_t _stream_id = sinfo.sinfo_stream;
    struct ctrl_msg_hdr *ctrl_msg_hdr = (struct ctrl_msg_hdr *) recv_buffer;
    enum CTRL_MSG_TYPE _msg_type = ctrl_msg_hdr->type;

    ssize_t _expected_size;
    switch (_msg_type)
    {
        case CTRL_MSG_TYPE_CODEC_START_REQ:
            _expected_size = sizeof(struct ctrl_msg_codec_start_req);
            break ;

        case CTRL_MSG_TYPE_CODEC_ERROR:
            _expected_size = sizeof(struct ctrl_msg_codec_error);
            break ;

        case CTRL_MSG_TYPE_CONNECTION_UDP_OPENED_INF:
        case CTRL_MSG_TYPE_CONNECTION_UDP_OPENED_ACK:
        case CTRL_MSG_TYPE_CONNECTION_UDP_CLOSED:
        case CTRL_MSG_TYPE_CODEC_START_ACC:
        case CTRL_MSG_TYPE_CODEC_START_DEC:
        case CTRL_MSG_TYPE_CODEC_END:
        case CTRL_MSG_TYPE_CAMPAIGN_END:
            _expected_size = 0;
            break ;

        default:
            _expected_size = -1;
            break ;
    }

    if (_expected_size == -1)
        return -CTRL_MSG_RECV_ERROR_MSG_TYPE;

    if (_expected_size != (recv_result - (ssize_t)sizeof(struct ctrl_msg_hdr)))
        return -CTRL_MSG_RECV_ERROR_MSG_SIZE;

    if (!stream_id && _stream_id > 0)
        return -CTRL_MSG_RECV_ERROR_STREAM_ID_NULL;

    if (_expected_size > 0)
    {
        if (!data)
            return -CTRL_MSG_RECV_ERROR_DATA_NULL;
        if (!data_len)
            return -CTRL_MSG_RECV_ERROR_DATA_LEN_NULL;

        *data = recv_buffer + sizeof(struct ctrl_msg_hdr);
        *data = malloc(_expected_size);
        memcpy(*data, recv_buffer + sizeof(struct ctrl_msg_hdr),
                _expected_size);
        *data_len = _expected_size;
    }
    else
    {
        if (data)
            *data = NULL;

        if (data_len)
            *data_len = 0;
    }

    *msg_type = _msg_type;
    if (stream_id)
        *stream_id = _stream_id;

    return 0;
}

