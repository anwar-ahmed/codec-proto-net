
#pragma once

#include <netinet/in.h>
#include <stddef.h>
#include <stdint.h>
#include <usc.h>

#include "feeder_config.h"  // for struct codec_config

#define SERVER_LISTEN_PORT 43762

// 1 reserved for syscall error
#define CTRL_MSG_RECV_ERROR_INVAL 0x0002
#define CTRL_MSG_RECV_ERROR_CLIENT_DISCONNECTED 0x0003
#define CTRL_MSG_RECV_ERROR_MSG_TYPE 0x0004
#define CTRL_MSG_RECV_ERROR_MSG_SIZE 0x0005
#define CTRL_MSG_RECV_ERROR_STREAM_ID_NULL 0x0006
#define CTRL_MSG_RECV_ERROR_DATA_NULL 0x0007
#define CTRL_MSG_RECV_ERROR_DATA_LEN_NULL 0x0008

struct socket_handle_struct
{
    int fd_sctp_listen;
    int fd_sctp_connection;
    int fd_udp;
    struct sockaddr *addr;
    socklen_t addr_len;
};
typedef struct socket_handle_struct *socket_handle_t;

enum __attribute__ ((__packed__)) CTRL_MSG_TYPE
{
    CTRL_MSG_TYPE_CONNECTION_UDP_OPENED_INF = 0x0001,
    CTRL_MSG_TYPE_CONNECTION_UDP_OPENED_ACK = 0x0002,
    CTRL_MSG_TYPE_CONNECTION_UDP_CLOSED = 0x0010,
    CTRL_MSG_TYPE_CODEC_START_REQ = 0x8000,
    CTRL_MSG_TYPE_CODEC_START_ACC = 0x8001,
    CTRL_MSG_TYPE_CODEC_START_DEC = 0x8002,
    CTRL_MSG_TYPE_CODEC_END = 0x8003,
    CTRL_MSG_TYPE_CODEC_ERROR = 0x8100,
    CTRL_MSG_TYPE_CAMPAIGN_END = 0xC000,
    CTRL_MSG_TYPE_UNKNOWN = 0xFFFF
};

#define CODEC_NAME_MAX_LENGTH 20

#define TIMING_OUTPUT_FILENAME_MAX_LENGTH 40

struct __attribute__ ((__packed__)) ctrl_msg_hdr
{
    enum CTRL_MSG_TYPE type;
};

// packed doesn't nest! (consequence: parameters is not packed)
struct __attribute__ ((__packed__)) ctrl_msg_codec_start_req
{
    char codec_name[CODEC_NAME_MAX_LENGTH];
    char timing_output_filename[TIMING_OUTPUT_FILENAME_MAX_LENGTH];
    uint16_t timing;
    uint32_t weight;
    uint16_t framesize;
    uint16_t buffer_size;
    uint16_t max_delay;
    USC_Option parameters;
};

struct __attribute__ ((__packed__)) ctrl_msg_codec_error
{
    uint16_t error_code;
};

int ctrl_msg_send_connection_udp_opened_inf(socket_handle_t handle);
int ctrl_msg_send_connection_udp_opened_ack(socket_handle_t handle);
int ctrl_msg_send_connection_udp_closed(socket_handle_t handle);

int ctrl_msg_send_codec_start_req(
        socket_handle_t handle,
        int id,
        const struct codec_config *codec_config,
        const char *timing_output_filename);
int ctrl_msg_send_codec_start_acc(socket_handle_t handle, int id);
int ctrl_msg_send_codec_start_dec(socket_handle_t handle, int id);

int ctrl_msg_send_codec_end(socket_handle_t handle, int id);

int ctrl_msg_send_codec_error(socket_handle_t handle, int id,
        uint16_t error_code);

int ctrl_msg_recv(socket_handle_t handle, enum CTRL_MSG_TYPE *msg_type,
        uint16_t *stream_id, char **data, uint16_t *data_len);

