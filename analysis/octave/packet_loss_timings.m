function bins = packet_loss_timings(measurements, bin_amount, earliest, latest)
% packet_loss_timings(measuremets, bin_amount, earliest, latest)
%
% Creates a histogram of the timings where packets were lost
%
% In:   measurements    the measurements of the campaign
%       bin_amount      amount of bins to use in the histogram
%       earliest        start time of the histogram
%       latest          end time of the histogram
% Out:  bins            bins of the histogram
%
% Worst-case execution time is O(n*m*k), where n is the amount of calls, m is
% the amount of measurements per call and k is the amount of dropped packets
% for each dropped sequence. In practice, execution time is more like O(n*m),
% where m is either the amount of measurements or amount of dropped packets,
% since dropped packets and measurements are mutually exclusive when running
% the campaign.
    bins = zeros(bin_amount, 1);

    for i = 1:length(measurements)
        for j = 2:length(measurements(i).seq_nums)
            loss_amount = measurements(i).seq_nums(j) - measurements(i).seq_nums(j-1) - 1;

            if (loss_amount == 0)
                continue ;
            endif

            loss_time_start = measurements(i).timings(j-1);
            loss_time_end = measurements(i).timings(j);

            frametime_estimator = (loss_time_end - loss_time_start) / (loss_amount + 1);

            for k = 1:loss_amount
                bin_idx = get_bin(loss_time_start + frametime_estimator * k, bin_amount, earliest, latest);
                bins(bin_idx) = bins(bin_idx) + 1;
            endfor
        endfor
    endfor
endfunction

function bin_number = get_bin(time, bin_amount, earliest, latest)

    % note! this function runs in linear time

    delta = (latest - earliest) / bin_amount;

    bin_number = floor((time - earliest) / delta) + 1;
endfunction
