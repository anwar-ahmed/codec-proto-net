
#pragma once

#include "ctrl_msg.h"

int server_open(int port, char *server_listen_address, socket_handle_t *socket_handle,
        int *errno_code, char **errstr);

int server_accept(socket_handle_t socket_handle, int *errno_code,
        char **errstr, char* interface, char *server_listen_address);

int server_close(socket_handle_t socket_handle);
