
#pragma once

// 65536 / 1024 = 64
#define HASH_TABLE_SIZE 1024

struct hash_table_entry
{
    int key;
    void *value;
    struct hash_table_entry *next;
    struct hash_table_entry *prev;
};

struct hash_table
{
    struct hash_table_entry **table;
};

typedef struct hash_table *hash_table_t;

/**
 *  Create a new hash table instance.
 *
 *  This function handles all memory allocation and initialization required.
 *
 *  Remember to call hash_table_destroy() after usage is finished in order to free the
 *  memory allocated.
 */
hash_table_t hash_table_create();

/**
 *  Initialize the table.
 *
 *  Note: Calling hash_table_create() will also handle initialization.
 */
int hash_table_init(hash_table_t table);

/**
 *  Destroy the table.
 *
 *  Releases all memory occupied by the table and its nodes.
 */
int hash_table_destroy(hash_table_t table);

/**
 *  Search a node.
 *
 *  Searches the table for the node specified by 'key'. If the node is found, its value is
 *  put into 'value'. Check the return value to see if the node was found.
 *
 *  Returns 0 if the node was found and the value set into 'value', otherwise -1.
 */
int hash_table_search(const hash_table_t table, int key, void **value);

/**
 *  Insert a new node to the table.
 *
 *  If the node doesn't exist, it creates a new node. If the node exists, it doesn nothing
 *  and just returns -1.
 *
 *  Returns 0 if insertion was successful and -1 otherwise.
 */
int hash_table_insert(hash_table_t table, int key, void *value);

/**
 *  Remove a node from the table.
 *
 *  Removes a node with the matching key. If a node doesn't exist, it returns -1.
 *
 *  Note: It doesn't free the 'value' of the node. If it was allocated on the heap, it
 *  has to be free'd elsewhere.
 *
 *  Returns 0 if the removing was succesfull and -1 otherwise.
 */
int hash_table_remove(hash_table_t table, int key);
