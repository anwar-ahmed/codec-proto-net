#define _GNU_SOURCE

#include "codec.h"
#include "macros.h"

#include <fcntl.h>
#ifndef BENCHMARK_CODEC
#include <errno.h>
#include <math.h>
#endif
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <inttypes.h>

#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#ifndef BENCHMARK_CODEC
#include "ctrl_msg.h"
#endif

#include "sched_headers.h"
#include "timing.h"
#include "usc.h"
#include "usc_base.h"

struct name_map
{
    const char *name;
    USC_Fxns *func_ptrs;
};

extern USC_Fxns USC_AMRWB_Fxns;
extern USC_Fxns USC_AMRWBE_Fxns;
extern USC_Fxns USC_G711A_Fxns;
extern USC_Fxns USC_G711U_Fxns;
extern USC_Fxns USC_G722_Fxns;
extern USC_Fxns USC_G722SB_Fxns;
extern USC_Fxns USC_G723_Fxns;
extern USC_Fxns USC_G726_Fxns;
extern USC_Fxns USC_G728_Fxns;
extern USC_Fxns USC_G7291_Fxns;
extern USC_Fxns USC_G729AFP_Fxns;
extern USC_Fxns USC_G729IFP_Fxns;
extern USC_Fxns USC_G729A_Fxns;
extern USC_Fxns USC_G729I_Fxns;
extern USC_Fxns USC_GSMAMR_Fxns;
extern USC_Fxns USC_GSMFR_Fxns;

struct name_map codec_name_mappings[] =
{
        { "AMRWB", &USC_AMRWB_Fxns },
        { "AMRWBE", &USC_AMRWBE_Fxns },
        { "G711A", &USC_G711A_Fxns },
        { "G711U", &USC_G711U_Fxns },
        { "G722.1", &USC_G722_Fxns },
        { "G722", &USC_G722SB_Fxns },   // has stability issues, don't use
        { "G723.1", &USC_G723_Fxns },
        { "G726", &USC_G726_Fxns },
        { "G728", &USC_G728_Fxns },
        { "G729.1", &USC_G7291_Fxns },
        { "G729Afp", &USC_G729AFP_Fxns },
        { "G729Ifp", &USC_G729IFP_Fxns },
        { "G729Ai", &USC_G729A_Fxns },
        { "G729Ii", &USC_G729I_Fxns },
        { "GSMAMR", &USC_GSMAMR_Fxns },
        { "GSMFR", &USC_GSMFR_Fxns },
        { "END", NULL }
};

USC_Fxns *get_func_ptrs(const char *codec_name)
{
    struct name_map *curr = codec_name_mappings;

    while (curr->func_ptrs)
    {
        if (strcmp(codec_name, curr->name) == 0)
        {
            return curr->func_ptrs;
        }

        ++curr;
    }

    return NULL;
}

int *initStreamStruct(struct codec_struct *codec_ptr)
{
    struct codec_struct *codecPtr = (struct codec_struct *) codec_ptr;
    codecPtr->ippFuncsPtr = get_func_ptrs(codecPtr->codec_name);

    if (!codecPtr->ippFuncsPtr)
    {
        codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_NO_SUCH_CODEC;
        return error_code;
    }

    int info_size;
    if (codecPtr->ippFuncsPtr->std.GetInfoSize(&info_size) != USC_NoError)
    {
    	codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_USC_GET_INFO_SIZE;
        return error_code;
    }

    codecPtr->ippCodecinfoPtr = (USC_CodecInfo *) malloc(info_size);
    if (codecPtr->ippFuncsPtr->std.GetInfo((USC_Handle) NULL,
            codecPtr->ippCodecinfoPtr) != USC_NoError)
    {
    	codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_USC_GET_INFO;
        return error_code;
    }

    if (codecPtr->parameters.direction == USC_ENCODE)
    {
        codecPtr->ippCodecinfoPtr->params.direction = USC_ENCODE;
    } else
    {
        codecPtr->ippCodecinfoPtr->params.direction = USC_DECODE;
    }

    codecPtr->ippCodecinfoPtr->params.modes.bitrate =
            codecPtr->parameters.modes.bitrate;
    codecPtr->ippCodecinfoPtr->params.law = codecPtr->parameters.law;
    codecPtr->ippCodecinfoPtr->params.pcmType = codecPtr->parameters.pcmType;
    codecPtr->ippCodecinfoPtr->params.modes.vad =
            codecPtr->parameters.modes.vad;

    if (codecPtr->ippFuncsPtr->std.NumAlloc(
            &(codecPtr->ippCodecinfoPtr->params), &codecPtr->numMemBanks)
            != USC_NoError)
    {
    	codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_USC_NUM_ALLOC;
        return error_code;
    }

    codecPtr->codecMemBanksPtr = (USC_MemBank *) malloc(
            sizeof(USC_MemBank) * codecPtr->numMemBanks);
    if (codecPtr->ippFuncsPtr->std.MemAlloc(
            &(codecPtr->ippCodecinfoPtr->params), codecPtr->codecMemBanksPtr)
            != USC_NoError)
    {
    	codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_USC_MEM_ALLOC;
        return error_code;
    }

    for (int i = 0; i < codecPtr->numMemBanks; ++i)
    {
        codecPtr->codecMemBanksPtr[i].pMem = (char *) malloc(
                codecPtr->codecMemBanksPtr[i].nbytes);
    }

    if (codecPtr->ippFuncsPtr->std.Init(&(codecPtr->ippCodecinfoPtr->params),
            codecPtr->codecMemBanksPtr, &codecPtr->encoder) != USC_NoError)
    {
    	codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_USC_INIT;
        return error_code;
    }

    if (codecPtr->ippFuncsPtr->std.GetInfo(codecPtr->encoder,
            codecPtr->ippCodecinfoPtr) != USC_NoError)
    {
    	codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_USC_GET_INFO2;
        return error_code;
    }

    // setting up USC_PCMStream structure
    codecPtr->inStream.bitrate =
            codecPtr->ippCodecinfoPtr->params.modes.bitrate;
    codecPtr->inStream.pcmType = codecPtr->ippCodecinfoPtr->params.pcmType;
    codecPtr->inStream.nbytes = codecPtr->frameLength;
    codecPtr->inStream.pBuffer = malloc(codecPtr->frameLength);

    int out_buffer_size;
    if (codecPtr->ippFuncsPtr->GetOutStreamSize(&codecPtr->parameters,
            codecPtr->ippCodecinfoPtr->params.modes.bitrate,
            codecPtr->frameLength, &out_buffer_size) != USC_NoError)
    {
    	codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_USC_GETOUTSTREAMSIZE;
        return error_code;
    }

    // setting up USC_Bitstream structure
    codecPtr->outStream.pBuffer = (char *) malloc(out_buffer_size);
    codecPtr->outStream.nbytes = out_buffer_size;

    // file naming with stream_id appended
    char *timeStamps_fileName;
    if (asprintf(&timeStamps_fileName, "%s_%zu", "timeStamps_from_callId",
            codecPtr->call_id) < 0)
    {
    	codecPtr->state = CODEC_STATE_REAPABLE;
    	int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_TIMESTAMP_FILE_NAME;
        return error_code;
    }

    // opening file for storing timestamps
    codecPtr->timeStamps_fd = open(timeStamps_fileName,
            O_CREAT | O_WRONLY | O_TRUNC,
            S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (codecPtr->timeStamps_fd == -1)
    {
        free(timeStamps_fileName);
        codecPtr->state = CODEC_STATE_REAPABLE;
        int *error_code = (int *) malloc(sizeof(int));
        *error_code = CODEC_ERROR_TIMESTAMP_FILE_OPEN;
        return error_code;
    }

    return NULL;
}
